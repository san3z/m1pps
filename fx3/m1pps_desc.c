/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   

#include "m1pps_desc.h"
#include "m1pps_usb.h"

#include "cyu3usbconst.h"
#include "cyu3externcstart.h"
#include "cyu3utils.h"

#if M1PPS_USB_XFER_TYPE == M1PPS_USB_XFER_TYPE_BULK
  #define M1PPS_USB_EP_XFER_TYPE CY_U3P_USB_EP_BULK
  #define M1PPS_USB_EP_INTERVAL_SS  (0)
  #define M1PPS_USB_EP_INTERVAL_HS  (0)
  #define M1PPS_USB_EP_INTERVAL_FS  (0)
#endif
#if M1PPS_USB_XFER_TYPE == M1PPS_USB_XFER_TYPE_ISOC
  #define M1PPS_USB_EP_XFER_TYPE CY_U3P_USB_EP_ISO
  #define M1PPS_USB_EP_INTERVAL_SS  (1)
  #define M1PPS_USB_EP_INTERVAL_HS  (1)
  #define M1PPS_USB_EP_INTERVAL_FS  (1)
#endif

/* Maximum packet size for the endpoint. Can take any value from 1 to 1024. */
#define CY_FX_ISO_MAXPKT                (1024)
/* ISO Mult settings for SS and HS operation. */
#define CY_FX_ISO_PKTS                  (3)
/* Burst length in packets supported by the ISO endpoints, when operating in USB 3.0 mode. */
#define CY_FX_ISO_BURST                 (8)


/* Standard device descriptor for USB 3.0 */
const uint8_t CyFxUSB30DeviceDscr[] __attribute__ ((aligned (32))) =
{
    0x12,                           /* Descriptor size */
    CY_U3P_USB_DEVICE_DESCR,        /* Device descriptor type */
    0x00,0x03,                      /* USB 3.0 */
    0x00,                           /* Device class */
    0x00,                           /* Device sub-class */
    0x00,                           /* Device protocol */
    0x09,                           /* Maxpacket size for EP0 : 2^9 */
    M1PPS_USB_VENDORID & 0xff ,M1PPS_USB_VENDORID >> 8,  /* Vendor ID */
    M1PPS_USB_PRODUCTID & 0xff, M1PPS_USB_PRODUCTID >> 8, /* Product ID */
    0x00,0x00,                      /* Device release number */
    0x01,                           /* Manufacture string index */
    0x02,                           /* Product string index */
    0x00,                           /* Serial number string index */
    0x01                            /* Number of configurations */
};

/* Standard device descriptor for USB 2.0 */
const uint8_t CyFxUSB20DeviceDscr[] __attribute__ ((aligned (32))) =
{
    0x12,                           /* Descriptor size */
    CY_U3P_USB_DEVICE_DESCR,        /* Device descriptor type */
    0x10,0x02,                      /* USB 2.10 */
    0x00,                           /* Device class */
    0x00,                           /* Device sub-class */
    0x00,                           /* Device protocol */
    0x40,                           /* Maxpacket size for EP0 : 64 bytes */
    M1PPS_USB_VENDORID & 0xff ,M1PPS_USB_VENDORID >> 8,  /* Vendor ID */
    M1PPS_USB_PRODUCTID & 0xff, M1PPS_USB_PRODUCTID >> 8, /* Product ID */
    0x00,0x00,                      /* Device release number */
    0x01,                           /* Manufacture string index */
    0x02,                           /* Product string index */
    0x00,                           /* Serial number string index */
    0x01                            /* Number of configurations */
};

/* Binary device object store descriptor */
const uint8_t CyFxUSBBOSDscr[] __attribute__ ((aligned (32))) =
{
    0x05,                           /* Descriptor size */
    CY_U3P_BOS_DESCR,               /* Device descriptor type */
    0x16,0x00,                      /* Length of this descriptor and all sub descriptors */
    0x02,                           /* Number of device capability descriptors */

    /* USB 2.0 extension */
    0x07,                           /* Descriptor size */
    CY_U3P_DEVICE_CAPB_DESCR,       /* Device capability type descriptor */
    CY_U3P_USB2_EXTN_CAPB_TYPE,     /* USB 2.0 extension capability type */
    0x02,0x00,0x00,0x00,            /* Supported device level features: LPM support  */

    /* SuperSpeed device capability */
    0x0A,                           /* Descriptor size */
    CY_U3P_DEVICE_CAPB_DESCR,       /* Device capability type descriptor */
    CY_U3P_SS_USB_CAPB_TYPE,        /* SuperSpeed device capability type */
    0x00,                           /* Supported device level features  */
    0x0E,0x00,                      /* Speeds supported by the device : SS, HS and FS */
    0x03,                           /* Functionality support */
    0x00,                           /* U1 Device Exit latency */
    0x00,0x00                       /* U2 Device Exit latency */
};

/* Standard device qualifier descriptor */
const uint8_t CyFxUSBDeviceQualDscr[] __attribute__ ((aligned (32))) =
{
    0x0A,                           /* Descriptor size */
    CY_U3P_USB_DEVQUAL_DESCR,       /* Device qualifier descriptor type */
    0x00,0x02,                      /* USB 2.0 */
    0x00,                           /* Device class */
    0x00,                           /* Device sub-class */
    0x00,                           /* Device protocol */
    0x40,                           /* Maxpacket size for EP0 : 64 bytes */
    0x01,                           /* Number of configurations */
    0x00                            /* Reserved */
};

/* Standard super speed configuration descriptor */
const uint8_t CyFxUSBSSConfigDscr[] __attribute__ ((aligned (32))) =
{
    /* Configuration descriptor */
    0x09,                           /* Descriptor size */
    CY_U3P_USB_CONFIG_DESCR,        /* Configuration descriptor type */
    0x2C,0x00,                      /* Length of this descriptor and all sub descriptors */
    0x01,                           /* Number of interfaces */
    0x01,                           /* Configuration number */
    0x00,                           /* COnfiguration string index */
    0x80,                           /* Config characteristics - Bus powered */
    0x32,                           /* Max power consumption of device (in 8mA unit) : 400mA */

    /* Interface descriptor */
    0x09,                           /* Descriptor size */
    CY_U3P_USB_INTRFC_DESCR,        /* Interface Descriptor type */
    0x00,                           /* Interface number */
    0x00,                           /* Alternate setting number */
    0x02,                           /* Number of end points */
    0xFF,                           /* Interface class */
    0x00,                           /* Interface sub class */
    0x00,                           /* Interface protocol code */
    0x00,                           /* Interface descriptor string index */

#if M1PPS_USB_XFER_TYPE == M1PPS_USB_XFER_TYPE_ISOC

    /* Endpoint descriptor for producer EP */
    0x07,                               /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,            /* Endpoint descriptor type */
    M1PPS_USB_EP_OUT,                   /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,                  /* ISO endpoint type */
    (CY_U3P_GET_LSB(CY_FX_ISO_MAXPKT)), /* LS Byte of maximum packet size. */
    (CY_U3P_GET_MSB(CY_FX_ISO_MAXPKT)), /* MS Byte of maximum packet size. */
    M1PPS_USB_EP_INTERVAL_SS,           /* Servicing interval for data transfers: 1 uFrame */

    /* Super speed endpoint companion descriptor for producer EP */
    0x06,                                                                       /* Descriptor size */
    CY_U3P_SS_EP_COMPN_DESCR,                                                   /* SS EP companion descriptor type */
    (CY_FX_ISO_BURST - 1),                                                      /* Number of packets per burst. */
    (CY_FX_ISO_PKTS - 1),                                                       /* Number of bursts per interval. */
    (CY_U3P_GET_LSB(CY_FX_ISO_MAXPKT * CY_FX_ISO_PKTS * CY_FX_ISO_BURST)),      /* LS Byte of data per interval. */
    (CY_U3P_GET_MSB(CY_FX_ISO_MAXPKT * CY_FX_ISO_PKTS * CY_FX_ISO_BURST)),      /* MS Byte of data per interval. */

    /* Endpoint descriptor for consumer EP */
    0x07,                               /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,            /* Endpoint descriptor type */
    M1PPS_USB_EP_IN,                   /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,             /* ISO endpoint type */
    (CY_U3P_GET_LSB(CY_FX_ISO_MAXPKT)), /* LS Byte of maximum packet size. */
    (CY_U3P_GET_MSB(CY_FX_ISO_MAXPKT)), /* MS Byte of maximum packet size. */
    M1PPS_USB_EP_INTERVAL_SS,           /* Servicing interval for data transfers: 1 uFrame */

    /* Super speed endpoint companion descriptor for consumer EP */
    0x06,                                                                       /* Descriptor size */
    CY_U3P_SS_EP_COMPN_DESCR,                                                   /* SS EP companion descriptor type */
    (CY_FX_ISO_BURST - 1),                                                      /* Number of packets per burst. */
    (CY_FX_ISO_PKTS - 1),                                                       /* Number of bursts per interval. */
    (CY_U3P_GET_LSB(CY_FX_ISO_MAXPKT * CY_FX_ISO_PKTS * CY_FX_ISO_BURST)),      /* LS Byte of data per interval. */
    (CY_U3P_GET_MSB(CY_FX_ISO_MAXPKT * CY_FX_ISO_PKTS * CY_FX_ISO_BURST))       /* MS Byte of data per interval. */

#endif

#if M1PPS_USB_XFER_TYPE == M1PPS_USB_XFER_TYPE_BULK

    /* Endpoint descriptor for producer EP */
    0x07,                           /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,        /* Endpoint descriptor type */
    M1PPS_USB_EP_OUT,               /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,         /* Bulk endpoint type */
    0x00,0x04,                      /* Max packet size = 1024 bytes */
    M1PPS_USB_EP_INTERVAL_SS,          /* Servicing interval for data transfers : 0 for bulk */

    /* Super speed endpoint companion descriptor for producer EP */
    0x06,                           /* Descriptor size */
    CY_U3P_SS_EP_COMPN_DESCR,       /* SS endpoint companion descriptor type */
    0x00,                           /* Max no. of packets in a burst : 0: burst 1 packet at a time */
    0x00,                           /* Max streams for bulk EP = 0 (No streams) */
    0x00,0x00,                      /* Service interval for the EP : 0 for bulk */

    /* Endpoint descriptor for consumer EP */
    0x07,                           /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,        /* Endpoint descriptor type */
    M1PPS_USB_EP_IN,                /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,             /* Bulk endpoint type */
    0x00,0x04,                      /* Max packet size = 1024 bytes */
    M1PPS_USB_EP_INTERVAL_SS,       /* Servicing interval for data transfers : 0 for Bulk */

    /* Super speed endpoint companion descriptor for consumer EP */
    0x06,                           /* Descriptor size */
    CY_U3P_SS_EP_COMPN_DESCR,       /* SS endpoint companion descriptor type */
    0x00,                           /* Max no. of packets in a burst : 0: burst 1 packet at a time */
    0x00,                           /* Max streams for bulk EP = 0 (No streams) */
    0x00,0x00                       /* Service interval for the EP : 0 for bulk */

#endif
};

/* Standard high speed configuration descriptor */
const uint8_t CyFxUSBHSConfigDscr[] __attribute__ ((aligned (32))) =
{
    /* Configuration descriptor */
    0x09,                           /* Descriptor size */
    CY_U3P_USB_CONFIG_DESCR,        /* Configuration descriptor type */
    0x20,0x00,                      /* Length of this descriptor and all sub descriptors */
    0x01,                           /* Number of interfaces */
    0x01,                           /* Configuration number */
    0x00,                           /* COnfiguration string index */
    0x80,                           /* Config characteristics - bus powered */
    0x32,                           /* Max power consumption of device (in 2mA unit) : 100mA */

    /* Interface descriptor */
    0x09,                           /* Descriptor size */
    CY_U3P_USB_INTRFC_DESCR,        /* Interface Descriptor type */
    0x00,                           /* Interface number */
    0x00,                           /* Alternate setting number */
    0x02,                           /* Number of endpoints */
    0xFF,                           /* Interface class */
    0x00,                           /* Interface sub class */
    0x00,                           /* Interface protocol code */
    0x00,                           /* Interface descriptor string index */

#if M1PPS_USB_XFER_TYPE == M1PPS_USB_XFER_TYPE_ISOC

    /* Endpoint descriptor for producer EP */
    0x07,                                                               /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,                                            /* Endpoint descriptor type */
    M1PPS_USB_EP_OUT,                                                   /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,                                             /* Isochronous endpoint type */
    (CY_U3P_GET_LSB(CY_FX_ISO_MAXPKT)),                                 /* LS Byte of maximum packet size. */
    (CY_U3P_GET_MSB(CY_FX_ISO_MAXPKT) | ((CY_FX_ISO_PKTS - 1) << 3)),   /* MS Byte of max. packet size and MULT. */
    M1PPS_USB_EP_INTERVAL_HS,                                           /* Servicing interval for data transfers */

    /* Endpoint descriptor for consumer EP */
    0x07,                                                               /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,                                            /* Endpoint descriptor type */
    M1PPS_USB_EP_IN,                                                    /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,                                             /* Isochronous endpoint type */
    (CY_U3P_GET_LSB(CY_FX_ISO_MAXPKT)),                                 /* LS Byte of maximum packet size. */
    (CY_U3P_GET_MSB(CY_FX_ISO_MAXPKT) | ((CY_FX_ISO_PKTS - 1) << 3)),   /* MS Byte of max. packet size and MULT. */
    M1PPS_USB_EP_INTERVAL_HS                                            /* Servicing interval for data transfers */

#endif

#if M1PPS_USB_XFER_TYPE == M1PPS_USB_XFER_TYPE_BULK

    /* Endpoint descriptor for producer EP */
    0x07,                           /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,        /* Endpoint descriptor type */
    M1PPS_USB_EP_OUT,               /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,             /* Bulk endpoint type */
    0x00,0x02,                      /* Max packet size = 512 bytes */
    M1PPS_USB_EP_INTERVAL_HS,       /* Servicing interval for data transfers : 0 for bulk */

    /* Endpoint descriptor for consumer EP */
    0x07,                           /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,        /* Endpoint descriptor type */
    M1PPS_USB_EP_IN,                /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,             /* Bulk endpoint type */
    0x00,0x02,                      /* Max packet size = 512 bytes */
    M1PPS_USB_EP_INTERVAL_HS        /* Servicing interval for data transfers : 0 for bulk */

#endif

};

/* Standard full speed configuration descriptor */
const uint8_t CyFxUSBFSConfigDscr[] __attribute__ ((aligned (32))) =
{
    /* Configuration descriptor */
    0x09,                           /* Descriptor size */
    CY_U3P_USB_CONFIG_DESCR,        /* Configuration descriptor type */
    0x20,0x00,                      /* Length of this descriptor and all sub descriptors */
    0x01,                           /* Number of interfaces */
    0x01,                           /* Configuration number */
    0x00,                           /* COnfiguration string index */
    0x80,                           /* Config characteristics - bus powered */
    0x32,                           /* Max power consumption of device (in 2mA unit) : 100mA */

    /* Interface descriptor */
    0x09,                           /* Descriptor size */
    CY_U3P_USB_INTRFC_DESCR,        /* Interface descriptor type */
    0x00,                           /* Interface number */
    0x00,                           /* Alternate setting number */
    0x02,                           /* Number of endpoints */
    0xFF,                           /* Interface class */
    0x00,                           /* Interface sub class */
    0x00,                           /* Interface protocol code */
    0x00,                           /* Interface descriptor string index */

#if M1PPS_USB_XFER_TYPE == M1PPS_USB_XFER_TYPE_ISOC
    /* Endpoint descriptor for producer EP */
    0x07,                                                       /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,                                    /* Endpoint descriptor type */
    M1PPS_USB_EP_OUT,                                           /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,                                     /* ISO endpoint type */
    (CY_U3P_GET_LSB(CY_U3P_MIN(1023,CY_FX_ISO_MAXPKT))),        /* LS Byte of Max Packet size. Limit to 1023. */
    (CY_U3P_GET_MSB(CY_U3P_MIN(1023,CY_FX_ISO_MAXPKT))),        /* LS Byte of Max Packet size. Limit to 1023. */
    M1PPS_USB_EP_INTERVAL_FS,                                   /* Servicing interval for data transfers */

    /* Endpoint descriptor for consumer EP */
    0x07,                                                       /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,                                    /* Endpoint descriptor type */
    M1PPS_USB_EP_IN,                                            /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,                                     /* ISO endpoint type */
    (CY_U3P_GET_LSB(CY_U3P_MIN(1023,CY_FX_ISO_MAXPKT))),        /* LS Byte of Max Packet size. Limit to 1023. */
    (CY_U3P_GET_MSB(CY_U3P_MIN(1023,CY_FX_ISO_MAXPKT))),        /* LS Byte of Max Packet size. Limit to 1023. */
    M1PPS_USB_EP_INTERVAL_FS                                    /* Servicing interval for data transfers */
#endif

#if M1PPS_USB_XFER_TYPE == M1PPS_USB_XFER_TYPE_BULK
    /* Endpoint descriptor for producer EP */
    0x07,                           /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,        /* Endpoint descriptor type */
    M1PPS_USB_EP_OUT,               /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,         /* Bulk endpoint type */
    0x40,0x00,                      /* Max packet size = 64 bytes */
    M1PPS_USB_EP_INTERVAL_FS,       /* Servicing interval for data transfers : 0 for bulk */

    /* Endpoint descriptor for consumer EP */
    0x07,                           /* Descriptor size */
    CY_U3P_USB_ENDPNT_DESCR,        /* Endpoint descriptor type */
    M1PPS_USB_EP_IN,                /* Endpoint address and description */
    M1PPS_USB_EP_XFER_TYPE,         /* Bulk endpoint type */
    0x40,0x00,                      /* Max packet size = 64 bytes */
    M1PPS_USB_EP_INTERVAL_FS        /* Servicing interval for data transfers : 0 for bulk */
#endif

};

/* Standard language ID string descriptor */
const uint8_t CyFxUSBStringLangIDDscr[] __attribute__ ((aligned (32))) =
{
    0x04,                           /* Descriptor size */
    CY_U3P_USB_STRING_DESCR,        /* Device descriptor type */
    0x09,0x04                       /* Language ID supported */
};

/* Standard manufacturer string descriptor */
const uint8_t CyFxUSBManufactureDscr[] __attribute__ ((aligned (32))) =
{
    0x06,                           /* Descriptor size */
    CY_U3P_USB_STRING_DESCR,        /* Device descriptor type */
    'G',0x00,
    '2',0x00
};

/* Standard product string descriptor */
const uint8_t CyFxUSBProductDscr[] __attribute__ ((aligned (32))) =
{
    0x0C,                           /* Descriptor size */
    CY_U3P_USB_STRING_DESCR,        /* Device descriptor type */
    'm',0x00,
    '1',0x00,
    'p',0x00,
    'p',0x00,
    's',0x00,
};

const void * descs[] = {
  [M1PPS_USB_DESC_DEVICE_SS]    = CyFxUSB30DeviceDscr,
  [M1PPS_USB_DESC_DEVICE_HS]    = CyFxUSB20DeviceDscr,
  [M1PPS_USB_DESC_BOS_SS]       = CyFxUSBBOSDscr,
  [M1PPS_USB_DESC_DEVQUAL]      = CyFxUSBDeviceQualDscr,
  [M1PPS_USB_DESC_CONFIG_SS]    = CyFxUSBSSConfigDscr,
  [M1PPS_USB_DESC_CONFIG_HS]    = CyFxUSBHSConfigDscr,
  [M1PPS_USB_DESC_CONFIG_FS]    = CyFxUSBFSConfigDscr,
  [M1PPS_USB_DESC_LANG]         = CyFxUSBStringLangIDDscr,
  [M1PPS_USB_DESC_MANUFACTURE]  = CyFxUSBManufactureDscr,
  [M1PPS_USB_DESC_PRODUCT]      = CyFxUSBProductDscr
};

/* Place this buffer as the last buffer so that no other variable / code shares
 * the same cache line. Do not add any other variables / arrays in this file.
 * This will lead to variables sharing the same cache line. */
const uint8_t CyFxUsbDscrAlignBuffer[32] __attribute__ ((aligned (32)));



const void *m1pps_usb_desc_get( int desc_id )
{
  if ( desc_id >= sizeof(descs)/sizeof(descs[0]))
    return 0;
  return descs[desc_id];
}

/* [ ] */

