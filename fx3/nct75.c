/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "nct75.h"

#include "i2c.h"

#define NCT75_KOEF (62500)


int nct75_read_temp_reg( uint16_t i2c_addr, uint8_t reg, int32_t *temp)
{
  int r;
  uint16_t value;
  int32_t t;

  if ((r = i2c_reg8_read16( i2c_addr, reg, &value)))
    return r;

  if ( value & (1 << 16) )
    (*temp) = -( (value & 0x7FFF) >> 4);
  else
    (*temp) =  (value >> 4);

  // 0.0625 degrees per unit -> get 1E6
  (*temp)*=NCT75_KOEF;

  t = *temp - *temp/10000*10000;

  // round degrees to 0.1
  *temp = *temp/100000*100000;
  if ( t > 5000 )
    *temp += 1;

  // switch back to 1E3
  *temp /= 1000;

  return 0;
}

int nct75_write_temp( uint16_t i2c_addr, uint8_t reg, int32_t temp)
{
  int r;
  uint16_t value;

  value = (temp<0)?-temp:temp;
  value *= 1000/NCT75_KOEF;
  value = (value & 0x7FF) << 4;

  if (temp < 0)
    value |= (1 << 16);

  if ((r = i2c_reg8_write16( i2c_addr, reg , value)))
    return r;

  return 0;
}

int nct75_init( uint16_t i2c_addr, int32_t alarm_temp)
{
  int r=0;
  uint8_t config;
  if ((r=nct75_write_temp( i2c_addr, NCT75_REG_T_HYST, alarm_temp)))
    return r;
  config = (2 << 3);
  return i2c_reg8_write8( i2c_addr, NCT75_REG_CONFIG, config);
}
