/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once


#define ERR_FX3(err, fmt, ...) \
 { ERR(" (%d/0x%x) "fmt, err, err, ##__VA_ARGS__); }

#define ERR_FATAL(err, fmt, ...) \
 { ERR_FX3(err, fmt, ##__VA_ARGS__); for (;;) CyU3PThreadSleep(100); }

#define CHECK_FATAL_ERR(err, fmt, ...) \
 { if (err != CY_U3P_SUCCESS) ERR_FATAL(err, fmt,  ##__VA_ARGS__) }

