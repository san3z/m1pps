/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "ad9548.h"

#include "i2c.h"
#include <m1pps_sys.h>

#define i2c_reg_read(a,r,v) i2c_reg16_read8(a,r,v)
#define i2c_reg_write(a,r,v) i2c_reg16_write8(a,r,v)

uint16_t ad9548_reg_prf_base[8] = {
  AD9548_REG_PRF0_BASE,
  AD9548_REG_PRF1_BASE,
  AD9548_REG_PRF2_BASE,
  AD9548_REG_PRF3_BASE,
  AD9548_REG_PRF4_BASE,
  AD9548_REG_PRF5_BASE,
  AD9548_REG_PRF6_BASE
};

int ad9548_i2c_device_reset( uint16_t i2c_addr )
{
  int err;
  if ( (err = i2c_reg_write( i2c_addr, AD9548_REG_SERCONF, 0x30) ))
    return err;
  return 0;
}

int ad9548_i2c_read_buf_reg( uint16_t i2c_addr, uint16_t reg_addr,  uint8_t *value )
{
  int err;
  if ( (err = i2c_reg_write( i2c_addr, AD9548_REG_IOUPDATE, 1) ))
    return err;

  if ( (err = i2c_reg_read( i2c_addr, reg_addr, value) ))
    return err;

  return 0;
}
int ad9548_i2c_write_buf_reg( uint16_t i2c_addr, uint16_t reg_addr,  uint8_t value )
{
  int err;

  if ( (err = i2c_reg_write( i2c_addr, reg_addr, value) ))
    return err;

  if ( (err = i2c_reg_write( i2c_addr, AD9548_REG_IOUPDATE, 1) ))
    return err;

  return 0;
}

int ad9548_i2c_read_dev_id( uint16_t i2c_addr, uint8_t *dev_id )
{
  return i2c_reg_read( i2c_addr, AD9548_REG_DEVID, dev_id );
}

int ad9548_i2c_read_pll_sts( uint16_t i2c_addr, struct ad9548_pll_sts *pll_sts)
{
  int err;

  if ( (err = ad9548_i2c_read_buf_reg( i2c_addr, AD9548_REG_PLLSTS1, pll_sts->status ) ))
    return err;

  if ( (err = ad9548_i2c_read_buf_reg( i2c_addr, AD9548_REG_PLLSTS2, pll_sts->status + 1) ))
    return err;

  return 0;
}

uint8_t ad9548_pll_sts_get_status( const struct ad9548_pll_sts *pll_sts )
{
   return (pll_sts->status[0] >> 4) & 0x3;
}

uint8_t ad9548_pll_sts_get_active_ref( const struct ad9548_pll_sts *pll_sts )
{
   return pll_sts->status[1] & 0x3;
}

int ad9548_i2c_read_full_power_down( uint16_t i2c_addr, uint8_t *power_down )
{
  int err;
  uint8_t value;
  if ( (err = ad9548_i2c_read_buf_reg( i2c_addr, AD9548_REG_GENPWRDOWN, &value) ))
    return err;
  *power_down = (value & 1 );
  return 0;
}

int ad9548_i2c_write_full_power_down( uint16_t i2c_addr, uint8_t power_down )
{
  int err;
  uint8_t value;

  if ( (err = ad9548_i2c_read_buf_reg( i2c_addr, AD9548_REG_GENPWRDOWN, &value) ))
    return err;

  if (power_down) value |= 1;
  else value &= ~1;

  return ad9548_i2c_write_buf_reg( i2c_addr,  AD9548_REG_GENPWRDOWN, value );
}

int ad9548_i2c_read_ref_status( uint16_t i2c_addr, uint8_t ref_num, uint8_t *value)
{
  return ad9548_i2c_read_buf_reg( i2c_addr, AD9548_REG_REF0_STS+ref_num, value);
}


static
int _ad9548_i2c_read_regs_u64( uint16_t i2c_addr, uint8_t buf_reg,  uint16_t start_reg, uint8_t bits,  uint64_t *u64 )
{
  int err;
  uint8_t value, i, bytes;

  *u64 = 0;

  bytes = bits >> 3;
  bytes += (bits & 7 ) ? 1 : 0;

  for (i=0;i<bytes;++i) {

    if ( buf_reg ) {
      if ( (err = ad9548_i2c_read_buf_reg( i2c_addr, start_reg+i, &value) ))
        return err;
    } else {
      if ( (err = i2c_reg_read( i2c_addr, start_reg+i, &value) ))
        return err;
    }

    if ( (i>=(bytes-1)) && (bits & 7) )
      value &=  ( UINT8_MAX >> (8-(bits & 7)) );

    (*u64) |= ((uint64_t)value << (i<<3));
  }

  return 0;
}

static
int _ad9548_i2c_write_regs_u64( uint16_t i2c_addr, uint8_t buf_reg,  uint16_t start_reg, uint8_t bits,  uint64_t u64 )
{
  int err;
  uint8_t value, i, bytes;

  bytes = bits >> 3;
  bytes += (bits & 7 ) ? 1 : 0;

  for (i=0;i<bytes;++i) {

    value = (u64 << i);

    if ( (i>=(bytes-1)) && (bits & 7) )
      value &=  ( UINT8_MAX >> (8-(bits & 7)) );

    if ( buf_reg ) {
      if ( (err = ad9548_i2c_write_buf_reg( i2c_addr, start_reg+i, value) ))
        return err;
    } else {
      if ( (err = i2c_reg_write( i2c_addr, start_reg+i, value) ))
        return err;
    }
  }

  return 0;
}

int ad9548_i2c_read_refprf_period( uint16_t i2c_addr, uint8_t prf_num, uint64_t *period_fs )
{
  if (prf_num > 7)
    return -EINVAL;

  return _ad9548_i2c_read_regs_u64( i2c_addr, 0, ad9548_reg_prf_base[prf_num]+AD9548_REG_PRF_OFS_PERIOD, 50, period_fs);
}

int ad9548_i2c_write_refprf_period( uint16_t i2c_addr, uint8_t prf_num, uint64_t period_fs )
{
  if (prf_num > 7)
    return -EINVAL;

  return _ad9548_i2c_write_regs_u64( i2c_addr, 0, ad9548_reg_prf_base[prf_num]+AD9548_REG_PRF_OFS_PERIOD, 50, period_fs);
}

uint8_t ad9548_ref_sts_get_prf_num(const struct ad9548_ref_sts *ref_sts)
{
  return (ref_sts->status & AD9548_REFSTS_PRF_MASK ) >> AD9548_REFSTS_PRF_SHIFT;
}

int ad9548_i2c_read_ref_sts( uint16_t i2c_addr, uint8_t ref_num, struct ad9548_ref_sts *ref_sts )
{
  int err=0;
  uint8_t prf_num;

  if ( (err = ad9548_i2c_read_ref_status( i2c_addr, ref_num, &ref_sts->status) ))
    return err;

  if ( ref_sts->status & AD9548_REFSTS_PRFSELECTED ) {
     prf_num = ad9548_ref_sts_get_prf_num(ref_sts);
     if ((err=ad9548_i2c_read_refprf_period( i2c_addr, prf_num, &ref_sts->period_fs )))
        return err;
  } else {
     ref_sts->period_fs = 0;
  }

  return 0;
}

int ad9548_i2c_read_freerun_tw( uint16_t i2c_addr, uint64_t *tw )
{
  return _ad9548_i2c_read_regs_u64( i2c_addr, 1, AD9548_REG_FREERUN_TW, 48, tw);
}

int ad9548_i2c_read_holdover_tw( uint16_t i2c_addr, uint64_t *tw )
{
  return _ad9548_i2c_read_regs_u64( i2c_addr, 1, AD9548_REG_HOLDOVER_TW, 48, tw);
}

#define AD9548_K    (281474976710656)
#define AD9548_FREQ (10)

int ad9548_i2c_read_input_info(uint16_t i2c_addr, ad9548_input_info_t *info)
{
  int err = 0;
  uint64_t tw;
  struct ad9548_pll_sts pll_sts;
  struct ad9548_ref_sts ref_sts;

  if ((err = ad9548_i2c_read_pll_sts(i2c_addr, &pll_sts)))
    return err;

  info->active_ref = ad9548_pll_sts_get_active_ref(&pll_sts);

  if ((err = ad9548_i2c_read_ref_sts(i2c_addr, info->active_ref, &ref_sts)))
    return err;

  info->freq_uHz = ((uint64_t)1000000000000)/(ref_sts.period_fs/1000000000);
  info->profile_num = ad9548_ref_sts_get_prf_num(&ref_sts);
  info->ref_status = ref_sts.status;

  if ((err = ad9548_i2c_read_freerun_tw(i2c_addr, &tw)))
    return err;

  info->fr_freq_uHz = (uint32_t)round(1000000.0*AD9548_FREQ*tw/(float)AD9548_K);

  if ((err = ad9548_i2c_read_holdover_tw(i2c_addr, &tw)))
    return err;

  info->ho_freq_uHz = (uint32_t)round(1000000.0*AD9548_FREQ*tw/(float)AD9548_K);

  return 0;

}
