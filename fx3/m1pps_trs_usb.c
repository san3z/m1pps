/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_trs_usb.h"
#include "m1pps_desc.h"

#include "m1pps.h"

#include "cyu3system.h"
#include "cyu3dma.h"
#include "cyu3error.h"
#include "cyu3usb.h"

#define M1PPS_MAX_EVENT_CNT             (16)                      /* event queue size (in packets) */
#define CY_FX_BULKLP_DMA_BUF_COUNT      (8)                       /* Bulk loop channel buffer count */
#define CY_FX_BULKLP_DMA_TX_SIZE        (0)                       /* DMA transfer size is set to infinite */
#define M1PPS_THREAD_STACK_SZ           (0x1000)
#define M1PPS_THREAD_PRIORITY           (8)
#define M1PPS_EVENT_THREAD_WAKEUP_PERIOD_MS (CYU3P_WAIT_FOREVER)

#pragma pack(push, 1)
struct evhdr {
  uint8_t type;
  m1pps_time_t ts;
  size_t size;
  uint8_t reserved[3];
};
#pragma pack(pop)

#define EV_TX_COMPLETE (0)
#define EV_RX          (1)

m1pps_trs_usb_t trs_usb;  // die to fx3 API we must access to transport object as singletone

CyBool_t glDataTransStarted = CyFalse; /* Whether DMA transfer has been started after enumeration. */

static CyU3PReturnStatus_t	__m1pps_trs_put_event( struct m1pps_trs_usb *trs_usb, struct evhdr *event )
{
  return CyU3PQueueSend( &trs_usb->events, event, CYU3P_NO_WAIT );
}

/* Callback funtion for the DMA event notification. */
void CyFxBulkSrcSinkDmaCallback(CyU3PDmaChannel *chHandle, CyU3PDmaCbType_t type,
                                CyU3PDmaCBInput_t *input)
{
	CyU3PDmaBuffer_t buf;
	CyU3PReturnStatus_t err;
  m1pps_trs_usb_t *t = &trs_usb;

//  DBG("t:%d", type);

	glDataTransStarted = CyTrue;

//  DBG("channel: %s", ( chHandle == (t->dma_channel + IN) ) ? "rx":"tx");

  switch (type) {
	  case CY_U3P_DMA_CB_PROD_EVENT: {

      __trs(t)->stat.rx++;
   		/* Wait for receiving a buffer from the producer socket (OUT endpoint). The call
   		 * will fail if there was an error or if the USB connection was reset / disconnected.
   		 * In case of error invoke the error handler and in case of reset / disconnection,
   		 * glIsApplnActive will be CyFalse; continue to beginning of the loop. */
   		//err = CyU3PDmaChannelGetBuffer( t->dma_channel + IN , &buf, CYU3P_WAIT_FOREVER);
   		err = CyU3PDmaChannelGetBuffer( t->dma_channel + IN , &buf, CYU3P_NO_WAIT);

      if (err != CY_U3P_SUCCESS) {
        __trs(t)->stat.err_rx++;
        //ERR_FX3(err,"CyU3PDmaChannelGetBuffer");
        goto exit;
      }

//      DBG("rx sz:%u", buf.count);
      t->event[TX]->type = EV_RX;
      t->event[TX]->ts   = m1pps_trs_get_time(__trs(t));
      t->event[TX]->size = buf.count;

      memcpy( t->event[TX]+1, buf.buffer, buf.count);

   		err = CyU3PDmaChannelDiscardBuffer( t->dma_channel +IN );
      if (err != CY_U3P_SUCCESS) {
        __trs(t)->stat.err_rx++;
        //ERR_FX3(err,"CyU3PDmaChannelDiscardBuffer");
        goto exit;
      }
      //CHECK_FATAL_ERR(err, "CyU3PDmaChannelDiscardBuffer");

      if (__m1pps_trs_put_event( t, t->event[TX] ) != CY_U3P_SUCCESS ) {
        __trs(t)->stat.err_rx++;
      }

	  } break;
    case CY_U3P_DMA_CB_CONS_EVENT: {
//    DBG("tx complete");
//
      __trs(t)->stat.tx++;

      t->event[TX]->type = EV_TX_COMPLETE;
      t->event[TX]->ts   = t->txcomp_ts;
      t->event[TX]->size = 0;
      if (__m1pps_trs_put_event( t, t->event[TX] ) != CY_U3P_SUCCESS ) {
        __trs(t)->stat.err_tx++;
      }

    } break;
    default:;
  }

exit:

//  DBG("end");
  return;
}

char *usb_speed_name[] = {
  [CY_U3P_FULL_SPEED] =  "full (12Mbps)",
  [CY_U3P_HIGH_SPEED] =  "high (480Mbps)",
  [CY_U3P_SUPER_SPEED] = "super (5Gbps)"
};


/* This function starts the bulk loop application. This is called
 * when a SET_CONF event is received from the USB host. The endpoints
 * are configured and the DMA pipe is setup in this function. */
void CyFxBulkLpApplnStart(m1pps_trs_usb_t *t)
{
	uint16_t size = 0;
	CyU3PEpConfig_t epCfg;
	CyU3PDmaChannelConfig_t dmaCfg;
	CyU3PReturnStatus_t err = CY_U3P_SUCCESS;
	CyU3PUSBSpeed_t usbSpeed = CyU3PUsbGetSpeed();


	/* First identify the usb speed. Once that is identified,
	 * create a DMA channel and start the transfer on this. */

	/* Based on the Bus Speed configure the endpoint packet size */
	switch (usbSpeed) {
	  case CY_U3P_FULL_SPEED:	size = 64; break;
	  case CY_U3P_HIGH_SPEED: size = 512;	break;
	  case CY_U3P_SUPER_SPEED: size = 1024;	break;
	  default:
		  ERR_FATAL(CY_U3P_ERROR_FAILURE, "Error! Invalid USB speed %d", usbSpeed);
	}

  INF("USB speed: %s ", usb_speed_name[usbSpeed] );


	CyU3PMemSet((uint8_t *) &epCfg, 0, sizeof(epCfg));
	epCfg.enable = CyTrue;
	epCfg.epType = CY_U3P_USB_EP_BULK;
	epCfg.burstLen = 1;
	epCfg.streams = 0;
	epCfg.pcktSize = size;

	/* Producer endpoint configuration */
	err = CyU3PSetEpConfig(t->ep[TX], &epCfg);
  CHECK_FATAL_ERR(err, "CyU3PSetEpConfig");

	/* Consumer endpoint configuration */
	err = CyU3PSetEpConfig(t->ep[RX], &epCfg);
  CHECK_FATAL_ERR(err, "CyU3PSetEpConfig");

	/* Flush the endpoint memory */
	CyU3PUsbFlushEp(t->ep[TX]);
	CyU3PUsbFlushEp(t->ep[RX]);

	/* Create a DMA MANUAL_IN channel for the producer socket. */
	dmaCfg.size = size;
	dmaCfg.count = CY_FX_BULKLP_DMA_BUF_COUNT;
	dmaCfg.prodSckId = CY_U3P_UIB_SOCKET_PROD_1;
	dmaCfg.consSckId = CY_U3P_CPU_SOCKET_CONS;
	dmaCfg.dmaMode = CY_U3P_DMA_MODE_BYTE;
	/* No callback is required. */
	dmaCfg.notification = CY_U3P_DMA_CB_PROD_EVENT;
	dmaCfg.cb = CyFxBulkSrcSinkDmaCallback;
	dmaCfg.prodHeader = 0;
	dmaCfg.prodFooter = 0;
	dmaCfg.consHeader = 0;
	dmaCfg.prodAvailCount = 0;

	err = CyU3PDmaChannelCreate( t->dma_channel + IN ,
			CY_U3P_DMA_TYPE_MANUAL_IN, &dmaCfg);
	CHECK_FATAL_ERR(err,"CyU3PDmaChannelCreate");

	/* Create a DMA MANUAL_OUT channel for the consumer socket. */
	dmaCfg.notification = CY_U3P_DMA_CB_CONS_EVENT;
	dmaCfg.prodSckId = CY_U3P_CPU_SOCKET_PROD;
	dmaCfg.consSckId = CY_U3P_UIB_SOCKET_CONS_1;
	err = CyU3PDmaChannelCreate( t->dma_channel + OUT,
			CY_U3P_DMA_TYPE_MANUAL_OUT, &dmaCfg);
	CHECK_FATAL_ERR(err,"CyU3PDmaChannelCreate");

	/* Set DMA Channel transfer size */
	err = CyU3PDmaChannelSetXfer( t->dma_channel + IN,
			CY_FX_BULKLP_DMA_TX_SIZE);
	CHECK_FATAL_ERR(err,"CyU3PDmaChannelSetXfer");

	err = CyU3PDmaChannelSetXfer( t->dma_channel + OUT,
			CY_FX_BULKLP_DMA_TX_SIZE);
  CHECK_FATAL_ERR(err, "CyU3PDmaChannelSetXfer");

	/* Update the flag so that the application thread is notified of this. */
	t->active = 1;
}

/* This function stops the bulk loop application. This shall be called whenever
 * a RESET or DISCONNECT event is received from the USB host. The endpoints are
 * disabled and the DMA pipe is destroyed by this function. */
void CyFxBulkLpApplnStop(m1pps_trs_usb_t *t)
{
	CyU3PEpConfig_t epCfg;
	CyU3PReturnStatus_t err = CY_U3P_SUCCESS;
  DBG(" ");

	/* Update the flag so that the application thread is notified of this. */
	t->active = 0;

	/* Destroy the channels */
	CyU3PDmaChannelDestroy( t->dma_channel + IN);
	CyU3PDmaChannelDestroy( t->dma_channel + OUT);

	/* Flush the endpoint memory */
	CyU3PUsbFlushEp(t->ep[TX]);
	CyU3PUsbFlushEp(t->ep[RX]);

	/* Disable endpoints. */
	CyU3PMemSet((uint8_t *) &epCfg, 0, sizeof(epCfg));
	epCfg.enable = CyFalse;

	/* Producer endpoint configuration. */
	err = CyU3PSetEpConfig(t->ep[TX], &epCfg);
	CHECK_FATAL_ERR(err,"CyU3PSetEpConfig");

	/* Consumer endpoint configuration. */
	err = CyU3PSetEpConfig(t->ep[RX], &epCfg);
	CHECK_FATAL_ERR(err,"CyU3PSetEpConfig");
}

/* Callback to handle the USB setup requests. */
CyBool_t CyFxBulkLpApplnUSBSetupCB(uint32_t setupdat0, uint32_t setupdat1)
{
	/* Fast enumeration is used. Only requests addressed to the interface, class,
	 * vendor and unknown control requests are received by this function.
	 * This application does not support any class or vendor requests. */

	uint8_t bRequest, bReqType;
	uint8_t bType, bTarget;
	uint16_t wValue, wIndex;
	CyBool_t isHandled = CyFalse;
  m1pps_trs_usb_t *t = &trs_usb;

  DBG("d0:%x d1:%x", setupdat0, setupdat1);

	/* Decode the fields from the setup request. */
	bReqType = (setupdat0 & CY_U3P_USB_REQUEST_TYPE_MASK);
	bType = (bReqType & CY_U3P_USB_TYPE_MASK);
	bTarget = (bReqType & CY_U3P_USB_TARGET_MASK);
	bRequest =
			((setupdat0 & CY_U3P_USB_REQUEST_MASK) >> CY_U3P_USB_REQUEST_POS);
	wValue = ((setupdat0 & CY_U3P_USB_VALUE_MASK) >> CY_U3P_USB_VALUE_POS);
	wIndex = ((setupdat1 & CY_U3P_USB_INDEX_MASK) >> CY_U3P_USB_INDEX_POS);

	if (bType == CY_U3P_USB_STANDARD_RQT) {
		/* Handle SET_FEATURE(FUNCTION_SUSPEND) and CLEAR_FEATURE(FUNCTION_SUSPEND)
		 * requests here. It should be allowed to pass if the device is in configured
		 * state and failed otherwise. */
		if ((bTarget == CY_U3P_USB_TARGET_INTF)
				&& ((bRequest == CY_U3P_USB_SC_SET_FEATURE)
						|| (bRequest == CY_U3P_USB_SC_CLEAR_FEATURE))
				&& (wValue == 0)) {
			if (t->active)
				CyU3PUsbAckSetup();
			else
				CyU3PUsbStall(0, CyTrue, CyFalse);

			isHandled = CyTrue;
		}

		/* CLEAR_FEATURE request for endpoint is always passed to the setup callback
		 * regardless of the enumeration model used. When a clear feature is received,
		 * the previous transfer has to be flushed and cleaned up. This is done at the
		 * protocol level. Since this is just a loopback operation, there is no higher
		 * level protocol and there are two DMA channels associated with the function,
		 * it is easier to stop and restart the application. If there are more than one
		 * EP associated with the channel reset both the EPs. The endpoint stall and toggle
		 * / sequence number is also expected to be reset. Return CyFalse to make the
		 * library clear the stall and reset the endpoint toggle. Or invoke the
		 * CyU3PUsbStall (ep, CyFalse, CyTrue) and return CyTrue. Here we are clearing
		 * the stall. */
		if ((bTarget == CY_U3P_USB_TARGET_ENDPT)
				&& (bRequest == CY_U3P_USB_SC_CLEAR_FEATURE)
				&& (wValue == CY_U3P_USBX_FS_EP_HALT)) {
			if ((wIndex == t->ep[TX])
					|| (wIndex == t->ep[RX])) {
				if (t->active) {
					CyFxBulkLpApplnStop(t);
					/* Give a chance for the main thread loop to run. */
					CyU3PThreadSleep(1);
					CyFxBulkLpApplnStart(t);
					CyU3PUsbStall(wIndex, CyFalse, CyTrue);

					CyU3PUsbAckSetup();
					isHandled = CyTrue;
				}
			}
		}
	}

	return isHandled;
}

/* This is the callback function to handle the USB events. */
void m1pps_trs_usb_event_cb(CyU3PUsbEventType_t evtype, uint16_t evdata)
{
  m1pps_trs_usb_t *t = &trs_usb;
  DBG("event:%d", evtype);
	switch (evtype) {
	case CY_U3P_USB_EVENT_SETCONF:
		/* Stop the application before re-starting. */
		if (t->active) {
			CyFxBulkLpApplnStop(t);
		}
		/* Start the loop back function. */
		CyFxBulkLpApplnStart(t);
		break;

	case CY_U3P_USB_EVENT_RESET:
	case CY_U3P_USB_EVENT_DISCONNECT:
		/* Stop the loop back function. */
		if (t->active) {
			CyFxBulkLpApplnStop(t);
		}
		break;

	default:
		break;
	}
}

/* Callback function to handle LPM requests from the USB 3.0 host. This function is invoked by the API
 whenever a state change from U0 -> U1 or U0 -> U2 happens. If we return CyTrue from this function, the
 FX3 device is retained in the low power state. If we return CyFalse, the FX3 device immediately tries
 to trigger an exit back to U0.

 This application does not have any state in which we should not allow U1/U2 transitions; and therefore
 the function always return CyTrue.
 */
CyBool_t CyFxBulkLpApplnLPMRqtCB(CyU3PUsbLinkPowerMode link_mode)
{
	return CyTrue;
}

/* This function initializes the USB Module, sets the enumeration descriptors.
 * This function does not start the bulk streaming and this is done only when
 * SET_CONF event is received. */
void m1pps_usb_init(void)
{
	CyU3PReturnStatus_t err = CY_U3P_SUCCESS;

  INF("USB initialization...");

	/* Start the USB functionality. */
	err = CyU3PUsbStart();
	CHECK_FATAL_ERR(err, "CyU3PUsbStart");

	/* The fast enumeration is the easiest way to setup a USB connection,
	 * where all enumeration phase is handled by the library. Only the
	 * class / vendor requests need to be handled by the application. */
	CyU3PUsbRegisterSetupCallback(CyFxBulkLpApplnUSBSetupCB, CyTrue);

	/* Setup the callback to handle the USB events. */
	CyU3PUsbRegisterEventCallback(m1pps_trs_usb_event_cb);

	/* Register a callback to handle LPM requests from the USB 3.0 host. */
	CyU3PUsbRegisterLPMRequestCallback(CyFxBulkLpApplnLPMRqtCB);

	/* Set the USB Enumeration descriptors */

	/* Super speed device descriptor. */
	err = CyU3PUsbSetDesc(CY_U3P_USB_SET_SS_DEVICE_DESCR, 0,
			(uint8_t *) CyFxUSB30DeviceDscr);
	CHECK_FATAL_ERR(err,"CyU3PUsbSetDesc SSDEV");

	/* High speed device descriptor. */
	err = CyU3PUsbSetDesc(CY_U3P_USB_SET_HS_DEVICE_DESCR, 0,
			(uint8_t *) CyFxUSB20DeviceDscr);
	CHECK_FATAL_ERR(err,"CyU3PUsbSetDesc HSDEV");

	/* BOS descriptor */
	err = CyU3PUsbSetDesc(CY_U3P_USB_SET_SS_BOS_DESCR, 0,
			(uint8_t *) CyFxUSBBOSDscr);
	CHECK_FATAL_ERR(err,"CyU3PUsbSetDesc BOS");

	/* Device qualifier descriptor */
	err = CyU3PUsbSetDesc(CY_U3P_USB_SET_DEVQUAL_DESCR, 0,
			(uint8_t *) CyFxUSBDeviceQualDscr);
	CHECK_FATAL_ERR(err,"CyU3PUsbSetDesc DEVQUAL");

	/* Super speed configuration descriptor */
	err = CyU3PUsbSetDesc(CY_U3P_USB_SET_SS_CONFIG_DESCR, 0,
			(uint8_t *) CyFxUSBSSConfigDscr);
	CHECK_FATAL_ERR(err,"CyU3PUsbSetDesc SSCONF");

	/* High speed configuration descriptor */
	err = CyU3PUsbSetDesc(CY_U3P_USB_SET_HS_CONFIG_DESCR, 0,
			(uint8_t *) CyFxUSBHSConfigDscr);
	CHECK_FATAL_ERR(err,"CyU3PUsbSetDesc HSCONF");

	/* Full speed configuration descriptor */
	err = CyU3PUsbSetDesc(CY_U3P_USB_SET_FS_CONFIG_DESCR, 0,
			(uint8_t *) CyFxUSBFSConfigDscr);
	CHECK_FATAL_ERR(err,"CyU3PUsbSetDesc FSCONF");

	/* String descriptor 0 */
	err = CyU3PUsbSetDesc(CY_U3P_USB_SET_STRING_DESCR, 0,
			(uint8_t *) CyFxUSBStringLangIDDscr);
	CHECK_FATAL_ERR(err,"CyU3PUsbSetDesc STR0");

	/* String descriptor 1 */
	err = CyU3PUsbSetDesc(CY_U3P_USB_SET_STRING_DESCR, 1,
			(uint8_t *) CyFxUSBManufactureDscr);
	CHECK_FATAL_ERR(err,"CyU3PUsbSetDesc STR1");

	/* String descriptor 2 */
	err = CyU3PUsbSetDesc(CY_U3P_USB_SET_STRING_DESCR, 2,
			(uint8_t *) CyFxUSBProductDscr);
	CHECK_FATAL_ERR(err,"CyU3PUsbSetDesc STR2");

	/* Connect the USB Pins with super speed operation enabled. */
	err = CyU3PConnectState(CyTrue, CyTrue);
	CHECK_FATAL_ERR(err,"CyU3PConnectState");

}


int m1pps_trs_usb_send_pkt( struct m1pps_trs *trs, const void *data, size_t data_sz )
{
	CyU3PDmaBuffer_t buf;
  CyU3PReturnStatus_t err;
  m1pps_trs_usb_t *t = container_of(trs, struct m1pps_trs_usb, trs);

  DBG("dsz:%d ",data_sz);

  err = CyU3PDmaChannelGetBuffer(t->dma_channel + OUT, &buf, CYU3P_NO_WAIT);
  if (err != CY_U3P_SUCCESS) {
    ERR_FX3(err, "CyU3PDmaChannelGetBuffer on transmit");
    return -EIO;
  }

  if (data_sz > buf.size) {
    ERR("attempt to transmit %d > %d", data_sz,  buf.size );
    return -EIO;
  }

  memcpy( buf.buffer, data, data_sz);

  t->txcomp_ts = m1pps_trs_get_time(trs);

  err = CyU3PDmaChannelCommitBuffer(t->dma_channel + OUT, data_sz, 0);
  if (err != CY_U3P_SUCCESS) {
    ERR_FX3(err, "(%d) CyU3PDmaChannelCommitBuffer on transmit");
    return -EIO;
  }


  return 0;
}

int m1pps_trs_usb_thread_run( uint32_t input )
{
  m1pps_trs_usb_t *t = (m1pps_trs_usb_t *)input;
  m1pps_trs_t *trs = __trs(t);
  CyU3PReturnStatus_t err;

  while ( ! t->thread_stopped ) {

     err=CyU3PQueueReceive( &t->events, t->event[RX], M1PPS_EVENT_THREAD_WAKEUP_PERIOD_MS );

     if ( err != CY_U3P_SUCCESS ) {
       if ( err != CY_U3P_ERROR_QUEUE_EMPTY)
          ERR_FX3(err, "CyU3PQueueReceive");
       continue;
     }

     DBG("event: %s (%u) sz:%u",
         (t->event[RX]->type == EV_TX_COMPLETE)?"tx complete":"rx data",
         t->event[RX]->type,
         t->event[RX]->size );

     if (t->event[RX]->type == EV_TX_COMPLETE) {
       m1pps_trs_call_pkt_sended( trs, t->event[RX]->ts );
     } else {
       m1pps_trs_call_pkt_recieved( trs, t->event[RX] + 1,
                                      t->event[RX]->size, t->event[RX]->ts );
     }
  }
  return 0;
}

int m1pps_trs_usb_open( m1pps_trs_t *trs, void  *opendata )
{
  CyU3PReturnStatus_t err=0;
  m1pps_trs_usb_t *t = container_of(trs, struct m1pps_trs_usb, trs);
  m1pps_trs_usb_open_data_t *od = opendata;

  memcpy(t->ep, od->ep, sizeof(t->ep) );
  DBG("ep:%x/%x", t->ep[IN], t->ep[OUT]);

	err = CyU3PThreadCreate(&t->thread,
	                 	      "ev_thread",
			                    m1pps_trs_usb_thread_run,
			                    (uint32_t)t,
			                    t->thread_stack,
			                    M1PPS_THREAD_STACK_SZ,
			                    M1PPS_THREAD_PRIORITY,
			                    M1PPS_THREAD_PRIORITY,
			                    CYU3P_NO_TIME_SLICE,
			                    CYU3P_AUTO_START);

  if (err!=CY_U3P_SUCCESS) {
    ERR_FX3(err, "CyU3PThreadCreate event");
    return 1;
  }

  m1pps_usb_init();

  return 0;
}

int m1pps_trs_usb_close( m1pps_trs_t *trs)
{
  m1pps_trs_usb_t *t = container_of(trs, struct m1pps_trs_usb, trs);
  t->thread_stopped = 1;
  CyU3PThreadDestroy( &t->thread );
  return 0;
}

int m1pps_trs_usb_init( m1pps_trs_usb_t *t, m1pps_clk_t *clk, size_t max_pkt_sz )
{
   CyU3PReturnStatus_t err;
   m1pps_trs_ops_t ops={0};

   ops.open  = m1pps_trs_usb_open;
   ops.close = m1pps_trs_usb_close;
   ops.send_pkt = m1pps_trs_usb_send_pkt;
   t->max_pkt_sz = max_pkt_sz;

   m1pps_trs_init( __trs(t) , &ops, clk );

   CyU3PMutexCreate(&t->ev_mutex, CYU3P_INHERIT );


   t->max_ev_sz = t->max_pkt_sz+sizeof(struct evhdr);

   // max_ev_sz must be 4 byte alligned
   t->max_ev_sz = (t->max_ev_sz & ~3) +  ((t->max_ev_sz&3) ? 4 : 0);

   DBG("max_pkt_sz:%"PRIsz" max_ev_sz:%"PRIsz,  t->max_pkt_sz, t->max_ev_sz );

   t->event[RX]  = CyU3PMemAlloc( t->max_ev_sz );
   t->event[TX]  = CyU3PMemAlloc( t->max_ev_sz );
   t->events_buf = CyU3PMemAlloc( t->max_ev_sz*M1PPS_MAX_EVENT_CNT );

   // CyU3PQueueCreate accepted bufers size in words count (word == 4 bytes)

   err=CyU3PQueueCreate(&t->events, t->max_ev_sz>>2,
                         t->events_buf, t->max_ev_sz*M1PPS_MAX_EVENT_CNT );
   if ( err != CY_U3P_SUCCESS ) {
     ERR_FX3(err, "CyU3PQueueCreate");
     return 1;
   }

   t->thread_stack = CyU3PMemAlloc(M1PPS_THREAD_STACK_SZ);
   t->thread_stopped = 0;
   t->active = 0;

   return 0;
}

void m1pps_trs_usb_destroy( m1pps_trs_usb_t *t )
{
   if (t->events_buf) {
     CyU3PMemFree(t->events_buf);
     t->events_buf = 0;
   }
   if (t->event[RX]) {
     CyU3PMemFree(t->event[RX]);
     t->event[RX] = 0;
     t->event[TX] = 0;
   }
   if (t->thread_stack) {
     CyU3PMemFree( t->thread_stack );
     t->thread_stack = 0;
   }

   CyU3PQueueDestroy(&t->events);
   CyU3PMutexDestroy(&t->ev_mutex);
   m1pps_trs_destroy(__trs(t));
}


uint8_t m1pps_trs_usb_is_active( m1pps_trs_usb_t *t )
{
  return t->active;
}

