/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_sys.h"
#include "m1pps_trs.h"

#include "cyu3os.h"
#include "cyu3dma.h"

typedef struct m1pps_trs_usb {
  m1pps_trs_t trs;

  struct evhdr *event[2];         // temp buffer for one event
  void         *events_buf;       // events queue storage buffer
  CyU3PQueue    events;           // events queue
  CyU3PMutex    ev_mutex;         // events mutex
  CyU3PDmaChannel dma_channel[2];

  CyU3PThread   thread;
  uint8_t       thread_stopped;
  void         *thread_stack;

  uint8_t       active;
  size_t        max_ev_sz;

  uint8_t ep[2];
  size_t   max_pkt_sz;

  uint32_t err_cnt;

  m1pps_time_t txcomp_ts;

} m1pps_trs_usb_t;

typedef struct m1pps_trs_usb_open_data {
  uint32_t rx_period_us;
  uint8_t  ep[2];
} m1pps_trs_usb_open_data_t;

int m1pps_trs_usb_init( m1pps_trs_usb_t *t, m1pps_clk_t *clk, size_t max_pkt_sz );
void m1pps_trs_usb_destroy( m1pps_trs_usb_t *t );
uint8_t m1pps_trs_usb_is_active( m1pps_trs_usb_t *t );

extern  m1pps_trs_usb_t trs_usb;
