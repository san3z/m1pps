/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include <stdint.h>

#define AD9548_REG_SERCONF      (0x0000)
#define AD9548_REG_DEVID        (0x0003)
#define AD9548_REG_IOUPDATE     (0x0005)
#define AD9548_REG_PLLSTS1      (0x0d0a)
#define AD9548_REG_PLLSTS2      (0x0D0B)
#define AD9548_REG_GENPWRDOWN   (0x0A00)

#define AD9548_REG_FREERUN_TW    (0x0300)
#define AD9548_REG_HOLDOVER_TW   (0x0D14)

int ad9548_i2c_read_buf_reg(  uint16_t i2c_addr, uint16_t reg_addr,  uint8_t *value );
int ad9548_i2c_write_buf_reg(  uint16_t i2c_addr, uint16_t reg_addr,  uint8_t value );

int ad9548_i2c_device_reset(  uint16_t i2c_addr );
int ad9548_i2c_read_dev_id( uint16_t i2c_addr, uint8_t *dev_id );

#define AD9548_PLLSTS_NO_LOCK    (0)
#define AD9548_PLLSTS_HAS_PHASE  (1)
#define AD9548_PLLSTS_HAS_FREQ   (2)
#define AD9548_PLLSTS_HAS_BOTH   (3)

int ad9548_i2c_read_full_power_down( uint16_t i2c_addr, uint8_t *power_down );
int ad9548_i2c_write_full_power_down( uint16_t i2c_addr, uint8_t power_down );

struct ad9548_pll_sts {
  uint8_t status[2];
};

int ad9548_i2c_read_pll_sts( uint16_t i2c_addr, struct ad9548_pll_sts *pll_sts);
uint8_t ad9548_pll_sts_get_status( const struct ad9548_pll_sts *pll_sts );
uint8_t ad9548_pll_sts_get_active_ref( const struct ad9548_pll_sts *pll_sts );

#define AD9548_REG_REF0_STS (0x0D0C)

#define  AD9548_REFSTS_PRFSELECTED (1<<7)
#define  AD9548_REFSTS_VALID       (1<<3)
#define  AD9548_REFSTS_FAULT       (1<<2)
#define  AD9548_REFSTS_FAST        (1<<1)
#define  AD9548_REFSTS_SLOW        (1<<0)

#define  AD9548_REFSTS_PRF_SHIFT   (4)
#define  AD9548_REFSTS_PRF_MASK    (3<<AD9548_REFSTS_PRF_SHIFT)

int ad9548_i2c_read_ref_status( uint16_t i2c_addr, uint8_t ref_num, uint8_t *prf_num);

#define  AD9548_REG_PRF0_BASE (0x0600)
#define  AD9548_REG_PRF1_BASE (0x0632)
#define  AD9548_REG_PRF2_BASE (0x0680)
#define  AD9548_REG_PRF3_BASE (0x06B2)
#define  AD9548_REG_PRF4_BASE (0x0700)
#define  AD9548_REG_PRF5_BASE (0x0732)
#define  AD9548_REG_PRF6_BASE (0x0780)
#define  AD9548_REG_PRF7_BASE (0x07B2)

#define  AD9548_REG_PRF_OFS_PERIOD (1)

extern uint16_t ad9548_reg_prf_base[8];

int ad9548_i2c_read_refprf_period( uint16_t i2c_addr, uint8_t prf_num, uint64_t *period_fs );

struct ad9548_ref_sts {
   uint8_t status;
   uint64_t period_fs;  // femto seconds
};

int ad9548_i2c_read_ref_sts( uint16_t i2c_addr, uint8_t ref_num, struct ad9548_ref_sts *ref_sts );
uint8_t ad9548_ref_sts_get_prf_num( const struct ad9548_ref_sts *ref_sts );

int ad9548_i2c_read_freerun_tw( uint16_t i2c_addr, uint64_t *tw );
int ad9548_i2c_read_holdover_tw( uint16_t i2c_addr, uint64_t *tw );

typedef struct ad9548_input_info {
  uint8_t  active_ref;      // active freq
  uint8_t  profile_num;
  uint8_t  ref_status;
  uint32_t freq_uHz;        // nominal freq
  uint32_t fr_freq_uHz;     // free run freq
  uint32_t ho_freq_uHz;     // holdover freq
} ad9548_input_info_t;

int ad9548_i2c_read_input_info(uint16_t i2c_addr, ad9548_input_info_t *info);
