/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include <stdint.h>

#define I2CBUS(a) (a>>8)

int i2c_init( uint8_t bus_addr, uint32_t bitrate );

void i2c_lock(uint8_t bus_addr);
void i2c_unlock(uint8_t bus_addr);

int i2c_reg16_read8( uint16_t i2c_addr, uint16_t reg_addr,  uint8_t *value );
int i2c_reg16_write8( uint16_t i2c_addr, uint16_t reg_addr,  uint8_t value );

int i2c_reg8_read8( uint16_t i2c_addr, uint8_t reg_addr,  uint8_t *value );
int i2c_reg8_write8( uint16_t i2c_addr, uint8_t reg_addr,  uint8_t value );

int i2c_reg8_read16( uint16_t i2c_addr, uint8_t reg_addr,  uint16_t *value );
int i2c_reg8_write16( uint16_t i2c_addr, uint8_t reg_addr,  uint16_t value );

