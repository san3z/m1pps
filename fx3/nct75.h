/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include <stdint.h>

#define NCT75_REG_TEMP     (0)
#define NCT75_REG_CONFIG   (1)
#define NCT75_REG_T_HYST   (2)
#define NCT75_REG_T_OS     (3)
#define NCT75_REG_ONESHOT  (4)

int nct75_read_temp_reg( uint16_t i2c_addr, uint8_t reg, int32_t *temp);
int nct75_write_temp_reg( uint16_t i2c_addr, uint8_t reg, int32_t temp);


#define nct75_read_temp(a, t) nct75_read_temp_reg( a, NCT75_REG_TEMP, t)
#define nct75_read_t_hist(a, t) nct75_read_temp_reg( a, NCT75_REG_T_HYST, t)
#define nct75_read_t_os(a, t) nct75_read_temp_reg( a, NCT75_REG_T_OS, t)
#define nct75_write_t_hist(a, t) nct75_write_temp_reg( a, NCT75_REG_T_HYST, t)
#define nct75_write_t_os(a, t) nct75_write_temp_reg( a, NCT75_REG_T_OS, t)

int nct75_init( uint16_t i2c_addr, int32_t alarm_temp);
