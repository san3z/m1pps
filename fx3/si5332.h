/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include <stdint.h>

struct si5332_reg {
  uint8_t addr;
  uint8_t value;
};

struct si5332_info {
  uint8_t dev_rev;
  char pn[6];
};

#ifdef HW_VER_2
#define SI5332_INIT_REGS_CNT (78)
#endif

#ifdef HW_VER_2A
#define SI5332_INIT_REGS_CNT (78)
#endif

extern struct si5332_reg si5332_init_regs[];

#define SI5332_REG_DEVICE_REV      (0x0E)
#define SI5332_REG_FACTORY_OPN_ID0 (0x10)

int si5332_i2c_reg_read( uint16_t i2c_addr, uint8_t reg_addr, uint8_t *value);
int si5332_i2c_reg_write( uint16_t i2c_addr, uint8_t reg_addr, uint8_t value);
int si5332_i2c_regs_write( uint16_t i2c_addr, const struct si5332_reg *reg, uint8_t count);
int si5332_i2c_regs_check( uint16_t i2c_addr, const struct si5332_reg *reg, uint8_t count);

int si5332_i2c_reg_write_bits( uint16_t i2c_addr, uint16_t reg_bit_addr, uint8_t value);
int si5332_i2c_reg_read_bits( uint16_t i2c_addr, uint16_t reg_bit_addr, uint8_t *value);

int si5332_i2c_read_info( uint16_t i2c_addr, struct si5332_info *inf );
