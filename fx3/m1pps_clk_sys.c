/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_clk_sys.h"

#include <tx_api.h>

#define ACCURANCY_NS (1000000)

// fix-me: time_ge/set are 32 bit !!!!!

typedef struct m1pps_clk_sys {
  m1pps_clk_t clk;
  uint32_t high;
} m1pps_clk_sys_t;

struct m1pps_clk_sys  _m1pps_clk_sys;

#define __clk_sys(p) ((struct m1pps_clk_sys*)p)

m1pps_time_t m1pps_clk_sys_get_time( const m1pps_clk_t *c )
{
  return ((uint64_t)tx_time_get() + (uint64_t)__clk_sys(c)->high*UINT32_MAX )*1000000;
}

int m1pps_clk_sys_set_time( struct m1pps_clk *c, m1pps_time_t time)
{
  uint64_t t_ms, low;
  t_ms = time/1000000;

  if ( t_ms > UINT32_MAX ) {
    __clk_sys(c)->high= t_ms/UINT32_MAX;
    low = t_ms -  __clk_sys(c)->high*UINT32_MAX;
  } else
    low = t_ms;

  tx_time_set( low );
  return 0;
}

int m1pps_clk_sys_adj_time( m1pps_clk_t *c, m1pps_tofs_t offset )
{
  m1pps_clk_sys_set_time( c, m1pps_clk_sys_get_time(c) + offset);
  return 0;
}



int m1pps_clk_sys_init(void)
{
  m1pps_clk_ops_t m1pps_clk_sys_ops = {
    .set_time = m1pps_clk_sys_set_time,
    .get_time = m1pps_clk_sys_get_time,
    .adj_time = m1pps_clk_sys_adj_time
  };
  int err =  m1pps_clk_init( __clk(&_m1pps_clk_sys), &m1pps_clk_sys_ops, ACCURANCY_NS);
  if (err)
    return err;
  _m1pps_clk_sys.high = 0;
  m1pps_clk_sys = __clk(&_m1pps_clk_sys);
  return 0;
};

void m1pps_clk_sys_destroy( void )
{
  m1pps_clk_sys = 0;
  m1pps_clk_destroy( __clk(&_m1pps_clk_sys) );
}

struct m1pps_clk *m1pps_clk_sys = 0 ;
