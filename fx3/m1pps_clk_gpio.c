/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_clk_gpio.h"
#include "m1pps.h"

#include "cyu3system.h"
#include "cyu3os.h"
#include "cyu3error.h"
#include "cyu3gpio.h"
#include "gpio_regs.h"

#include <math.h>

//#define NO_TIME_ADJUST
//#define NO_PERIOD_ADJUST

#define ACCURANCY_NS (10)

#define CLK_THREAD_STACK_SZ          (0x500)
#define CLK_THREAD_PRIORITY          (10)

#define __clk_gpio(p) ((m1pps_clk_gpio_t*)p)


int __m1pps_clk_gpio_read_ts( m1pps_clk_gpio_t *clk_gpio,  uint32_t *ts )
{
  uint32_t regVal;
  uint8_t  index = clk_gpio->gpio_num % 8;
  regVal = (GPIO->lpp_gpio_pin[index].status & ~CY_U3P_LPP_GPIO_INTR);

  /* Set the mode to sample now. Wait for the sampling to
   * complete and read the value from the threshold register. */
  GPIO->lpp_gpio_pin[index].status = (regVal | (CY_U3P_GPIO_MODE_SAMPLE_NOW << CY_U3P_LPP_GPIO_MODE_POS));
  //while (GPIO->lpp_gpio_pin[index].status & CY_U3P_LPP_GPIO_MODE_MASK);

  *ts = GPIO->lpp_gpio_pin[index].threshold;
  return 0;
}

uint8_t done=0;

int __m1pps_clk_gpio_write_ts( m1pps_clk_gpio_t *clk_gpio, uint32_t timer_value )
{
   uint8_t  index = clk_gpio->gpio_num % 8;
   /* Disable the GPIO before configuration. */
   GPIO->lpp_gpio_pin[index].status &= ~CY_U3P_LPP_GPIO_ENABLE;

   /* Update the register configurations. */
   GPIO->lpp_gpio_pin[index].timer = timer_value;

   /* Enable the GPIO. */
   GPIO->lpp_gpio_pin[index].status |= CY_U3P_LPP_GPIO_ENABLE;
   return 0;
}


void __m1pps_clk_gpio_update_from_hw(  m1pps_clk_gpio_t *clk_gpio )
{
  //uint32_t dur_ts,cur_ts;
  uint32_t cur_ts;
  uint64_t period;

  __m1pps_clk_gpio_read_ts( clk_gpio, &cur_ts );

  if ( clk_gpio->ts > cur_ts ) {
    clk_gpio->period_cnt++;
  }

  clk_gpio->ts = cur_ts;
  period = (uint64_t)clk_gpio->period + (int64_t)clk_gpio->period_offset;
  clk_gpio->time_ns = (uint64_t)round((clk_gpio->period_cnt*period + (uint64_t)cur_ts)*clk_gpio->ns_per_tick);
}



uint32_t __m1pps_clk_gpio_calc_new_ts( m1pps_clk_gpio_t *clk_gpio, int32_t *rdt_ns )
{
   uint32_t new_ts;
   int32_t  dt_ts, dt_ns;
   uint64_t ns;
// uint64_t t;

   // calc real time spended from for last 'end of period'
   ns = clk_gpio->time_ns - clk_gpio->period_cnt*clk_gpio->period_ns;

   // calc new ts needed
   new_ts = (uint32_t)round((double)ns/clk_gpio->ns_per_tick);
/*
   t = (uint64_t)clk_gpio->time_ns/((uint64_t)UINT32_MAX+1);
   INF("timens: %u %u",  (uint32_t)t,  (uint32_t)((uint64_t)clk_gpio->time_ns - t*UINT32_MAX-t) );
   INF("oldspended:%u",  (uint32_t) round(clk_gpio->ts*clk_gpio->ns_per_tick));
   INF("periods:%u  spended %d ns %u ts", (uint32_t )clk_gpio->period_cnt, (uint32_t )ns, new_ts);
*/
   if (!rdt_ns)
     return new_ts;

   dt_ts = ( new_ts > clk_gpio->ts ) ? new_ts - clk_gpio->ts : clk_gpio->ts - new_ts;
   dt_ts = ( new_ts > clk_gpio->ts ) ? dt_ts : -dt_ts;

   dt_ns = round((double)dt_ts*clk_gpio->ns_per_tick);

/*
   INF("cts:%"PRIu32" nts:%"PRIu32" dt_ts:%"PRIu32,
       clk_gpio->ts, new_ts, dt_ns );
*/
   *rdt_ns = dt_ns;

   return new_ts;
}


void __m1pps_clk_gpio_adjust_ts_to_real( m1pps_clk_gpio_t *clk_gpio )
{
   int32_t dt_ns;
   uint32_t new_ts;

   new_ts =  __m1pps_clk_gpio_calc_new_ts( clk_gpio, &dt_ns );

   //INF("adj %d ns", dt_ns);

   if ( abs(dt_ns) < 500.0 )
     new_ts=new_ts;

   INF("adjusted old:%u new:%u ts",  clk_gpio->ts, new_ts);
   INF("adjusted dt:%d ns ", dt_ns );

   // assign new ts value
   __m1pps_clk_gpio_write_ts( clk_gpio, new_ts );


   clk_gpio->ts = new_ts;
}

m1pps_time_t m1pps_clk_gpio_get_time( const m1pps_clk_t *clk )
{
  uint64_t time_ns;

  CyU3PMutexGet( &__clk_gpio(clk)->mutex, CYU3P_WAIT_FOREVER );

  __m1pps_clk_gpio_update_from_hw( __clk_gpio(clk) );

  time_ns = __clk_gpio(clk)->time_ns;

  CyU3PMutexPut( &__clk_gpio(clk)->mutex );


  return time_ns;
}

int m1pps_clk_gpio_set_time( struct m1pps_clk *clk, m1pps_time_t time_ns)
{
  CyU3PMutexGet( &__clk_gpio(clk)->mutex, CYU3P_WAIT_FOREVER );
  __clk_gpio(clk)->time_ns = time_ns;
  __clk_gpio(clk)->period_cnt = (uint64_t)floor((double)__clk_gpio(clk)->time_ns/(double)__clk_gpio(clk)->period_ns);
  __m1pps_clk_gpio_adjust_ts_to_real(__clk_gpio(clk));
  CyU3PMutexPut( &__clk_gpio(clk)->mutex );
  return 0;
}

int m1pps_clk_gpio_adj_time( m1pps_clk_t *clk, m1pps_tofs_t offset )
{
  INF("adjust off: %d ns", (int32_t)offset );

  #ifndef NO_TIME_ADJUST

  CyU3PMutexGet( &__clk_gpio(clk)->mutex, CYU3P_WAIT_FOREVER );

  __m1pps_clk_gpio_update_from_hw( __clk_gpio(clk) );

  __clk_gpio(clk)->time_ns += offset;
  __clk_gpio(clk)->period_cnt = (uint64_t)floor((double)__clk_gpio(clk)->time_ns/(double)__clk_gpio(clk)->period_ns);

  __m1pps_clk_gpio_adjust_ts_to_real(__clk_gpio(clk));
  CyU3PMutexPut( &__clk_gpio(clk)->mutex );

  #endif

  return 0;
}

int m1pps_clk_gpio_adj_period( struct m1pps_clk *clk, m1pps_tofs_t period_k)
{
   m1pps_clk_gpio_t *clk_gpio = __clk_gpio(clk);
   uint8_t  index = clk_gpio->gpio_num % 8;
   m1pps_tofs_t period_offset_ns = ((m1pps_tofs_t)clk_gpio->period_ns)/period_k;
   uint32_t new_ts;

   INF("period_k:%d period_ofs_ns:%u", (int32_t)period_k,  (uint32_t)period_offset_ns);

   #ifndef NO_PERIOD_ADJUST
   CyU3PMutexGet( &__clk_gpio(clk)->mutex, CYU3P_WAIT_FOREVER );

   clk_gpio->period_offset = (int32_t)round((period_offset_ns/1E3)*clk_gpio->freq_mhz);
   clk_gpio->ns_per_tick = (double)clk_gpio->period_ns/(double)(clk_gpio->period + clk_gpio->period_offset);
   INF("GPIO %u clock adjusted: tick:%uns period:%uns = %u + %d ticks",
        clk_gpio->gpio_num,
       (uint32_t)round(clk_gpio->ns_per_tick),
       (uint32_t)round(clk_gpio->period_ns),
       clk_gpio->period,
       clk_gpio->period_offset);

   new_ts =  __m1pps_clk_gpio_calc_new_ts( clk_gpio, 0 );

   /* Disable the GPIO before configuration. */
   GPIO->lpp_gpio_pin[index].status &= ~CY_U3P_LPP_GPIO_ENABLE;
   /* Update the register configurations. */
   GPIO->lpp_gpio_pin[index].period = clk_gpio->period + clk_gpio->period_offset - 1;
   GPIO->lpp_gpio_pin[index].timer = new_ts;
   /* Enable the GPIO. */
   GPIO->lpp_gpio_pin[index].status |= CY_U3P_LPP_GPIO_ENABLE;

   CyU3PMutexPut( &__clk_gpio(clk)->mutex );
   #endif
   return 0;
}

void m1pps_clk_gpio_set_interrupt_mode( m1pps_clk_gpio_t *clk_gpio,  uint8_t mode )
{
    uint32_t value=0;
    uint8_t  index = clk_gpio->gpio_num % 8;

    /* Interrupt mode. */
    value |= (( ((mode)?CY_U3P_GPIO_INTR_TIMER_ZERO:0) << CY_U3P_LPP_GPIO_INTRMODE_POS) &
            CY_U3P_LPP_GPIO_INTRMODE_MASK);
    /* Clear any pending interrupt. */
    value |= CY_U3P_LPP_GPIO_INTR;

    value |= (( CY_U3P_GPIO_MODE_STATIC << CY_U3P_LPP_GPIO_MODE_POS) &
            CY_U3P_LPP_GPIO_MODE_MASK);

    /* Timer mode. */
    value |= ((CY_U3P_GPIO_TIMER_HIGH_FREQ << CY_U3P_LPP_GPIO_TIMER_MODE_POS) &
            CY_U3P_LPP_GPIO_TIMER_MODE_MASK);

    /* Disable the GPIO before configuration. */
    GPIO->lpp_gpio_pin[index].status &= ~CY_U3P_LPP_GPIO_ENABLE;

    /* Update the register configurations. */
//    GPIO->lpp_gpio_pin[gpioId % 8].timer = cfg_p->timer;
//    GPIO->lpp_gpio_pin[gpioId % 8].period = cfg_p->period;
//    GPIO->lpp_gpio_pin[gpioId % 8].threshold = cfg_p->threshold;
    GPIO->lpp_gpio_pin[index].status = value;

    /* Enable the GPIO. */
    GPIO->lpp_gpio_pin[index].status |= CY_U3P_LPP_GPIO_ENABLE;
}

int m1pps_clk_gpio_set_output( m1pps_clk_t *clk, uint8_t enabled )
{
  m1pps_clk_gpio_set_interrupt_mode( (m1pps_clk_gpio_t*)clk, enabled);
  return 0;
}

void m1pps_clk_gpio_timer_cb(uint32_t timerArg)
{
  //uint32_t dt_ns;
  //m1pps_clk_gpio_get_time( (m1pps_clk_t*)timerArg );
  INF("get ns");
}

void m1pps_clk_gpio_thread_run(uint32_t input)
{
  int32_t dt_ns;
  m1pps_clk_gpio_t *clk_gpio = (m1pps_clk_gpio_t*)input;
  while (1) {
    m1pps_clk_get_time(__clk(clk_gpio));
    __m1pps_clk_gpio_calc_new_ts( clk_gpio, &dt_ns );
    //DBG("get %d ns", dt_ns);
		CyU3PThreadSleep(250);
  }
}


int m1pps_clk_gpio_init( m1pps_clk_gpio_t *clk_gpio, uint8_t gpio_num, double freq_mhz )
{
  m1pps_clk_ops_t m1pps_clk_gpio_ops = {
    .set_time = m1pps_clk_gpio_set_time,
    .get_time = m1pps_clk_gpio_get_time,
    .adj_time = m1pps_clk_gpio_adj_time,
    .adj_period = m1pps_clk_gpio_adj_period,
    .set_output = m1pps_clk_gpio_set_output
  };
  CyU3PGpioComplexConfig_t cfg = {0};
  CyU3PReturnStatus_t err_fx3;
  int err;

  clk_gpio->gpio_num = gpio_num;
  clk_gpio->freq_mhz = freq_mhz;
  //clk_gpio->ns_per_tick = 1E3/freq_mhz;
  clk_gpio->period_ns = 1000000000;
  clk_gpio->period = (uint32_t)round((clk_gpio->period_ns/1E3)*clk_gpio->freq_mhz);
  clk_gpio->period_offset = 0;
  clk_gpio->ns_per_tick = (double)clk_gpio->period_ns/(double)(clk_gpio->period+clk_gpio->period_offset);

  INF("GPIO %u clock: tick:%uns period:%uns = %uticks", gpio_num,
       (uint32_t)round(clk_gpio->ns_per_tick),
       (uint32_t)round(clk_gpio->period_ns),
       clk_gpio->period );

  cfg.outValue = CyFalse;
  cfg.inputEn = CyFalse;
  cfg.driveLowEn = CyTrue;
  cfg.driveHighEn = CyTrue;
  cfg.pinMode = CY_U3P_GPIO_MODE_STATIC;
  //cfg.intrMode = CY_U3P_GPIO_INTR_TIMER_ZERO;
  cfg.intrMode = 0;
  cfg.timerMode = CY_U3P_GPIO_TIMER_HIGH_FREQ;
  cfg.timer = 0;
  cfg.period = clk_gpio->period+clk_gpio->period_offset-1;
  cfg.threshold = 0xffffffff;

  //cfg.intrMode = CY_U3P_GPIO_INTR_TIMER_THRES;
  //cfg.timerMode = CY_U3P_GPIO_TIMER_POS_EDGE;
  //cfg.threshold = clk_gpio->period;

  err_fx3 = CyU3PGpioSetComplexConfig(gpio_num, &cfg);
  if ( err_fx3 != CY_U3P_SUCCESS ) {
    ERR_FX3(err_fx3, "CyU3PGpioSetComplexConfig gpio %d", clk_gpio->gpio_num );
    err = -1;
    goto exit;
  }

  if ((err = m1pps_clk_init( __clk(clk_gpio), &m1pps_clk_gpio_ops, ACCURANCY_NS)))
    goto exit;

  CyU3PMutexCreate(&clk_gpio->mutex, CYU3P_INHERIT );

  clk_gpio->time_ns = 0;
  clk_gpio->ts = 0;
  clk_gpio->period_cnt = 0;

#if 0
  err_fx3 = CyU3PTimerCreate( &clk_gpio->timer, m1pps_clk_gpio_timer_cb,
                              (uint32_t)clk_gpio, 350, 350, 1 );

  if (err_fx3 != CY_U3P_SUCCESS) {
     ERR_FX3(err_fx3, "CyU3PTimerCreate gpio %d", clk_gpio->gpio_num );
     err= -1;
     goto exit;
  }
#endif

	clk_gpio->stack = CyU3PMemAlloc(CLK_THREAD_STACK_SZ);
  if ( ! clk_gpio->stack ) {
    ERR("No memory for thread stack");
    goto exit;
  }

	err = CyU3PThreadCreate(&clk_gpio->thread,
                          "clk thread",
                          m1pps_clk_gpio_thread_run,
                          (uint32_t)clk_gpio,
                          clk_gpio->stack,
                          CLK_THREAD_STACK_SZ,
                          CLK_THREAD_PRIORITY,
                          CLK_THREAD_PRIORITY,
                          CYU3P_NO_TIME_SLICE,
                          CYU3P_AUTO_START);

  if (err_fx3 != CY_U3P_SUCCESS) {
     ERR_FX3(err_fx3, "CyU3PThreadCreate gpio %d", clk_gpio->gpio_num );
     err= -1;
     goto exit;
  }

exit:

  return err;
};

void m1pps_clk_gpio_destroy( m1pps_clk_gpio_t *clk_gpio )
{
  CyU3PThreadDestroy( &clk_gpio->thread );
  //CyU3PTimerDestroy( &clk_gpio->timer );
  CyU3PMutexDestroy( &clk_gpio->mutex );
  m1pps_clk_destroy( __clk(clk_gpio) );
}



#if 0
1. Configure Fast GPIO Clock at approximately 10 MHz. Fast GPIO Clock is derived from SYS_CLK_PLL/4 with divider set as 10, so the frequency of Fast GPIO Clock is 10.08 MHz.
           CyU3PGpioClock_t gpioClock;
          /* Initialize the GPIO module */
         gpioClock.fastClkDiv = 10;
           gpioClock.slowClkDiv = 0;
           gpioClock.simpleDiv = CY_U3P_GPIO_SIMPLE_DIV_BY_2;
           gpioClock.clkSrc = CY_U3P_SYS_CLK_BY_4;
CY_U3P_SYS_CLK_BY_4;           gpioClock.halfDiv = 0;

          return CyU3PGpioInit(&gpioClock, NULL);

2. Configure DUMMY_COMPLEX_GPIO to be in static output mode.
          /* Configure DUMMY_COMPLEX_GPIO as static mode output*/
         gpioComplexConfig.outValue = CyFalse;
         gpioComplexConfig.inputEn = CyFalse;
         gpioComplexConfig.driveLowEn = CyTrue;
         gpioComplexConfig.driveHighEn = CyTrue;
         gpioComplexConfig.pinMode = CY_U3P_GPIO_MODE_STATIC;
         gpioComplexConfig.intrMode = CY_U3P_GPIO_INTR_TIMER_ZERO;
         gpioComplexConfig.timerMode = CY_U3P_GPIO_TIMER_HIGH_FREQ;
         gpioComplexConfig.timer = 0;
         gpioComplexConfig.period = 0xffffffff;
         gpioComplexConfig.threshold = 0xffffffff;
         status = CyU3PGpioSetComplexConfig(DUMMY_COMPLEX_GPIO, &gpioComplexConfig);
         if (status != CY_U3P_SUCCESS)
         {
            CyU3PDebugPrint (4, "CyU3PGpioSetComplexConfig failed, error code= %d\n" ,status);
            CyCx3UvcAppErrorHandler(status);
         }

3. Use the CyU3PGpioComplexSampleNow API to get the timestamp.
      CyU3PGpioComplexSampleNow(DUMMY_COMPLEX_GPIO, ×tamp);
#endif
