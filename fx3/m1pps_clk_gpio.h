/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_clk.h"

typedef struct m1pps_clk_gpio {
  m1pps_clk_t clk;
  uint8_t gpio_num;
  uint64_t time_ns;       // real time calced  according to ts & period_cnt;
  double freq_mhz;        // input freq to gpio module
  double ns_per_tick;     // nanoseconds per tick
  uint32_t period;        // number of ticks intervals in one period
  int32_t  period_offset;
  uint32_t ts;            // last readed hw ts count
  uint64_t period_cnt;    // spended number of periods
  uint64_t period_ns;
  CyU3PMutex mutex;
  CyU3PTimer timer;
  void *stack;
  CyU3PThread thread;
} m1pps_clk_gpio_t;

int m1pps_clk_gpio_init( m1pps_clk_gpio_t *clk_gpio, uint8_t gpio_num, double gpio_clock_mhz );
void m1pps_clk_gpio_destroy( m1pps_clk_gpio_t *clk_gpio );

void m1pps_clk_gpio_set_interrupt_mode( m1pps_clk_gpio_t *clk_gpio,  uint8_t mode );
