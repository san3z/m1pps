/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "i2c.h"

//#include "cyu3system.h"
#include <cyu3os.h>
#include <cyu3i2c.h>
#include <cyu3error.h>
#include <m1pps_sys.h>
#include "m1pps.h"


CyU3PMutex m1pps_i2c_mutex[1];

inline uint8_t i2c_bus(uint16_t i2c_addr)
{
  return i2c_addr >> 8;
}

int i2c_init( uint8_t bus_addr, uint32_t bitrate )
{
  CyU3PReturnStatus_t err =  CY_U3P_SUCCESS;
  CyU3PI2cConfig_t i2cConfig;

	INF("I2C initialization for bitrate:%u ...", bitrate);

	/* Initialize and configure the I2C master module. */
	err = CyU3PI2cInit();
	if (err != CY_U3P_SUCCESS) {
		ERR_FX3(err, "CyU3PI2cInit");
    goto exit;
	}

	CyU3PMemSet((uint8_t *) &i2cConfig, 0, sizeof(i2cConfig));
	i2cConfig.bitRate = bitrate;
	i2cConfig.busTimeout = 0xFFFFFFFF;
	i2cConfig.dmaTimeout = 0xFFFF;
	i2cConfig.isDma = CyFalse;

	err = CyU3PI2cSetConfig(&i2cConfig, NULL);
	if (err != CY_U3P_SUCCESS) {
    ERR_FX3(err, "CyU3PI2cSetConfig")
    goto exit;
  }

  CyU3PMutexCreate(m1pps_i2c_mutex, CYU3P_INHERIT);

exit:

  return err;
}

void i2c_lock(uint8_t bus_addr)
{
  CyU3PMutexGet(m1pps_i2c_mutex, CYU3P_WAIT_FOREVER);
}

void i2c_unlock(uint8_t bus_addr)
{
  CyU3PMutexPut(m1pps_i2c_mutex);
}

static uint32_t _i2c_addr( const CyU3PI2cPreamble_t *preamble )
{
  uint8_t i;
  uint32_t reg_addr=0;
  if (preamble->length<2)
    return reg_addr;
  for (i=0;i<preamble->length-2;++i)
    reg_addr |= (preamble->buffer[1+i] << (preamble->length-3-i));
  return reg_addr;
}

static int _i2c_read8( CyU3PI2cPreamble_t *preamble, uint8_t *value )
{
  CyU3PReturnStatus_t err;
	err = CyU3PI2cReceiveBytes(preamble, value, 1, 0);
	if (err != CY_U3P_SUCCESS) {
    uint32_t reg_addr=_i2c_addr(preamble);
    ERR_FX3(err, "CyU3PI2cReceiveBytes 0x%x:0x%x", preamble->buffer[0]>>1, reg_addr);
    return -EIO;
  }

  return 0;
}

static int _i2c_write8( CyU3PI2cPreamble_t *preamble, uint8_t value )
{
	CyU3PReturnStatus_t err = CyU3PI2cTransmitBytes(preamble, &value, 1, 0);
	if (err != CY_U3P_SUCCESS) {
    uint32_t reg_addr=_i2c_addr(preamble);
    ERR_FX3(err, "CyU3PI2cTransmitBytes 0x%x:0x%x:0x%x",
                 preamble->buffer[0]>>1, reg_addr, value);
    return -EIO;
	}
  return 0;
}

static int _i2c_read16( CyU3PI2cPreamble_t *preamble, uint16_t *value )
{
  CyU3PReturnStatus_t err;
  uint8_t bytes[2];
	err = CyU3PI2cReceiveBytes(preamble, bytes, 2, 0);
	if (err != CY_U3P_SUCCESS) {
    uint32_t reg_addr=_i2c_addr(preamble);
    ERR_FX3(err, "CyU3PI2cReceiveBytes 0x%x:0x%x", preamble->buffer[0]>>1, reg_addr);
    return -EIO;
  }

  (*value) = ((uint16_t)bytes[0] << 8) | bytes[1];

  return 0;
}

static int _i2c_write16( CyU3PI2cPreamble_t *preamble, uint16_t value )
{
	CyU3PReturnStatus_t err;
  uint8_t bytes[2];
  bytes[0] = (value >> 8) & 0xFF;
  bytes[1] = value & 0xFF;
  err = CyU3PI2cTransmitBytes(preamble, bytes, 2, 0);
	if (err != CY_U3P_SUCCESS) {
    uint32_t reg_addr=_i2c_addr(preamble);
    ERR_FX3(err, "CyU3PI2cTransmitBytes 0x%x:0x%x:0x%x",
                 preamble->buffer[0]>>1, reg_addr, value);
    return -EIO;
	}
  return 0;
}

int i2c_reg16_read8( uint16_t i2c_addr, uint16_t reg_addr,  uint8_t *value )
{
	CyU3PI2cPreamble_t preamble;

  i2c_addr &= 0xFF;
	preamble.length = 4;
	preamble.buffer[0] = i2c_addr << 1;
	preamble.buffer[1] = (uint8_t) (( reg_addr >> 8) & 0xFF);
	preamble.buffer[2] = (uint8_t) ( reg_addr & 0xFF);
	preamble.buffer[3] = ((i2c_addr << 1) | 0x01);
	preamble.ctrlMask = 0x0004;

  return _i2c_read8( &preamble, value );
}

int i2c_reg16_write8( uint16_t i2c_addr, uint16_t reg_addr,  uint8_t value )
{
	CyU3PI2cPreamble_t preamble;

  i2c_addr &= 0xFF;
	preamble.length = 3;
	preamble.buffer[0] = i2c_addr << 1;
	preamble.buffer[1] = (uint8_t) (( reg_addr >> 8) & 0xFF);
	preamble.buffer[2] = (uint8_t) ( reg_addr & 0xFF);
	preamble.ctrlMask = 0x0000;

  return _i2c_write8( &preamble, value );
}

int i2c_reg8_read8( uint16_t i2c_addr, uint8_t reg_addr,  uint8_t *value )
{
	CyU3PI2cPreamble_t preamble;

  i2c_addr &= 0xFF;
	preamble.length = 3;
	preamble.buffer[0] = i2c_addr << 1;
	preamble.buffer[1] = reg_addr;
	preamble.buffer[2] = ((i2c_addr << 1) | 0x01);
	preamble.ctrlMask = 0x0002;

  return _i2c_read8( &preamble, value );
}

int i2c_reg8_write8( uint16_t i2c_addr, uint8_t reg_addr,  uint8_t value )
{
	CyU3PI2cPreamble_t preamble;

  i2c_addr &= 0xFF;
	preamble.length = 2;
	preamble.buffer[0] = i2c_addr << 1;
	preamble.buffer[1] = reg_addr;
	preamble.ctrlMask = 0x0000;

  return _i2c_write8( &preamble, value );
}


int i2c_reg8_read16( uint16_t i2c_addr, uint8_t reg_addr,  uint16_t *value )
{
	CyU3PI2cPreamble_t preamble;

  i2c_addr &= 0xFF;
	preamble.length = 3;
	preamble.buffer[0] = i2c_addr << 1;
	preamble.buffer[1] = reg_addr;
	preamble.buffer[2] = ((i2c_addr << 1) | 0x01);
	preamble.ctrlMask = 0x0002;

  return _i2c_read16( &preamble, value );
}

int i2c_reg8_write16( uint16_t i2c_addr, uint8_t reg_addr,  uint16_t value )
{
	CyU3PI2cPreamble_t preamble;

  i2c_addr &= 0xFF;
	preamble.length = 2;
	preamble.buffer[0] = i2c_addr << 1;
	preamble.buffer[1] = reg_addr;
	preamble.ctrlMask = 0x0000;

  return _i2c_write16( &preamble, value );
}
