/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "cyu3system.h"
#include "cyu3os.h"
#include "cyu3dma.h"
#include "cyu3error.h"
#include "cyu3usb.h"
#include "cyu3uart.h"
#include "cyu3gpio.h"
#include "cyu3i2c.h"

#include "m1pps.h"
#include "m1pps_clk.h"
#include "m1pps_inst.h"
#include "m1pps_usb.h"
#include "m1pps_trs_usb.h"
#include "m1pps_clk_gpio.h"
#include "i2c.h"
#include "ad9548.h"
#include "si5332.h"
#include "nct75.h"

#define M1PPS_PROTOCOL_ROLE M1PPS_INST_ROLE_SLAVE
//#define M1PPS_PROTOCOL_ROLE M1PPS_INST_ROLE_MASTER

#define M1PPS_I2C_BUS_ADDR                  (0x00)
#define M1PPS_AD9548_I2C_ADDR               (0x0049)
#define M1PPS_AD9548_REFNUM                 (0)
#define M1PPS_M24M02_BASE_I2C_ADDR          (0x0050)

#define M1PPS_I2C_BITRATE_HZ                (100000)
#define M1PPS_TIMER_PERIOD_US               (10000)

#define M1PPS_TIMERS_THREAD_STACK_SZ        (0x500)
#define M1PPS_TIMERS_THREAD_PRIORITY        (10)

#define M1PPS_STAT_THREAD_STACK_SZ          (0x500)
#define M1PPS_STAT_THREAD_PRIORITY          (10)

#define M1PPS_PULSE_THREAD_STACK_SZ         (0x100)
#define M1PPS_PULSE_THREAD_PRIORITY         (8)

#define M1PPS_GPIO_FAST_CLOCK_DIV (2) // this is minimal availble
#define PULSE_DUR_MS              (100)

struct gpio_desc {
  uint8_t number;
  uint8_t output;
  uint8_t value;
};

#ifdef HW_VER_1

//#define FREQ_MHZ (384.0)
#define FREQ_MHZ (403.2)

#define APP_LED_GPIO      (54)
#define APP_SWITCH_GPIO   (45)
#define TRANSMIT_GPIO     (24)
#define FEEDBACK_GPIO     (22)
#define PLL_LED_GPIO      (25)
#define PLL_RESET_GPIO    (17)
#define CLOCK_GPIO        (40)
#define FPGA_CFGDONE_GPIO (26)

struct gpio_desc gpio[] = {
  { APP_SWITCH_GPIO, 0, 0 },
  { APP_LED_GPIO,    1, 1 },
  { TRANSMIT_GPIO,   1, 0 },
  { PLL_LED_GPIO,    1, 1 },
  { PLL_RESET_GPIO,  1, 1 },
  { FEEDBACK_GPIO,   0, 0 },
  { FPGA_CFGDONE_GPIO, 1, 1 }
};

#define M1PPS_LEDS_THREAD_STACK_SZ          (0x100)
#define M1PPS_LEDS_THREAD_PRIORITY          (10)

#endif

#if defined(HW_VER_2) || defined(HW_VER_2A)

#define FREQ_MHZ (416.0)

#define TRANSMIT_GPIO     (53)
#define PLL_RESET_GPIO    (54)
#define CLOCK_GPIO        (40)
#define FPGA_CFGDONE_GPIO (26)
#define FPGA_51_GPIO      (51)
#define FPGA_52_GPIO      (52)
#define FPGA_27_GPIO      (27)
#define FAN_ENABLE_GPIO   (57)

struct gpio_desc gpio[] = {
  { TRANSMIT_GPIO,   1, 0 },
  { PLL_RESET_GPIO,  1, 0 },
  { FPGA_CFGDONE_GPIO, 0, 0 },
  { FPGA_51_GPIO, 0, 0 },
  { FPGA_52_GPIO, 0, 0 },
  { FPGA_27_GPIO, 1, 0 },
  { FAN_ENABLE_GPIO, 1, 1 }
};

#define M1PPS_SI5332_I2C_ADDR (0x006a)
#define M1PPS_NCT75_I2C_ADDR  (0x004c)

#include "si5332.h"

#endif


m1pps_inst_t inst;               /* main protocol instance */

CyU3PThread m1pps_timers_thread; /* fsm timers thread */
CyU3PThread m1pps_stat_thread;   /* statistics calculate & output thread */
CyU3PThread m1pps_leds_thread;   /* Light status indication thread */
CyU3PThread m1pps_pulse_thread;  /* Pulse output thread */
CyU3PQueue  pulse_queue;
uint32_t    pulse_events[1];

m1pps_clk_gpio_t m1pps_clk_gpio;
m1pps_clk_t *m1pps_clk;



uint8_t  pll_cur_sts = AD9548_PLLSTS_NO_LOCK;
uint32_t pll_phlock_start_ms = 0;
uint32_t pll_frlock_start_ms = 0;

/* Application Error Handler */
void CyFxAppErrorHandler(CyU3PReturnStatus_t apiRetStatus)
{
  ERR("Stopped...")
	for (;;) {
		CyU3PThreadSleep(100);
	}
}

void m1pps_dbg_console_init(void)
{
	CyU3PUartConfig_t uartConfig;
	CyU3PReturnStatus_t err;

	/* Initialize the UART for printing debug messages */
	err = CyU3PUartInit();
	if (err != CY_U3P_SUCCESS) {
		/* Error handling */
		CyFxAppErrorHandler(err);
	}

	/* Set UART configuration */
	CyU3PMemSet((uint8_t *) &uartConfig, 0, sizeof(uartConfig));
	uartConfig.baudRate = CY_U3P_UART_BAUDRATE_115200;
	uartConfig.stopBit = CY_U3P_UART_ONE_STOP_BIT;
	uartConfig.parity = CY_U3P_UART_NO_PARITY;
	uartConfig.txEnable = CyTrue;
	uartConfig.rxEnable = CyFalse;
	uartConfig.flowCtrl = CyFalse;
	uartConfig.isDma = CyTrue;
	//uartConfig.isDma = CyFalse;

	err = CyU3PUartSetConfig(&uartConfig, NULL);
	if (err != CY_U3P_SUCCESS) {
		CyFxAppErrorHandler(err);
	}

	/* Set the UART transfer to a really large value. */
	err = CyU3PUartTxSetBlockXfer(0xFFFFFFFF);
	if (err != CY_U3P_SUCCESS) {
		CyFxAppErrorHandler(err);
	}

	/* Initialize the debug module. */
	err = CyU3PDebugInit(CY_U3P_LPP_SOCKET_UART_CONS, 5);
	if (err != CY_U3P_SUCCESS) {
		CyFxAppErrorHandler(err);
	}
  CyU3PDebugPreamble(CyFalse);
  CyU3PDebugPrint (4,"abcdef");
  //CyU3PDebugEnable(0XFFFF);
  DBG("Debug console initialized.");
}

void m1pps_timers_thread_run(uint32_t input)
{
  while (1) {
    CyU3PThreadSleep(M1PPS_TIMER_PERIOD_US/1000);
    m1pps_inst_tick(&inst);
  }
}

void *m1pps_inst_mutexes_create( m1pps_inst_t *inst, uint8_t mutex_cnt, size_t *mutex_sz )
{
   CyU3PMutex *mutexes = sys_zalloc( sizeof(CyU3PMutex)*mutex_cnt );
   if ( mutexes ) {
     (*mutex_sz) = sizeof(CyU3PMutex);
     do {
       mutex_cnt--;
       CyU3PMutexCreate(mutexes + mutex_cnt, CYU3P_INHERIT );
     } while (mutex_cnt);
   }
   return mutexes;
}

void m1pps_inst_mutexes_free( m1pps_inst_t *inst, void *mutexes )
{
   sys_free( mutexes );
}

void m1pps_inst_mutex_lock( m1pps_inst_t *inst, void *mutex )
{
  CyU3PMutexGet( (CyU3PMutex*)mutex, CYU3P_WAIT_FOREVER );
}

void m1pps_inst_mutex_unlock( m1pps_inst_t *inst, void *mutex )
{
  CyU3PMutexPut( (CyU3PMutex *)mutex );
}

#define CY_U3P_LPP_GPIO_ADDR(n) (uint32_t*)(0xe0001100 + ((n)*(0x0004)))
#define CY_U3P_LPP_GPIO_INTR (1<<27) /* Do not touch the interrupt bit */
#define OUT_VALUE 0x01 /* The 0th bit is the value that is output on the pins */

void m1pps_gpio_fast_toggle( uint8_t gpionum )
{
  /* Read the register and toggle the OUT_VALUE bit */
  uint32_t * gpioClockPtr = CY_U3P_LPP_GPIO_ADDR(gpionum);
  *gpioClockPtr = (*gpioClockPtr & ~CY_U3P_LPP_GPIO_INTR) ^ OUT_VALUE;
}

void m1pps_gpio_fast_set( uint8_t gpionum, uint8_t value )
{
  /* Read the register and toggle the OUT_VALUE bit */
  uint32_t * gpioClockPtr = CY_U3P_LPP_GPIO_ADDR(gpionum);

  if ( value )
    *gpioClockPtr = (*gpioClockPtr & ~CY_U3P_LPP_GPIO_INTR ) | OUT_VALUE;
  else
    *gpioClockPtr = (*gpioClockPtr & ~CY_U3P_LPP_GPIO_INTR ) & ~OUT_VALUE;

}

void m1pps_pulse_thread_run(uint32_t input)
{
  uint32_t ev;
  CyU3PReturnStatus_t err;
  while ( 1 ) {
     err=CyU3PQueueReceive( &pulse_queue, &ev, CYU3P_WAIT_FOREVER );
     if ( err != CY_U3P_SUCCESS ) {
       if ( err != CY_U3P_ERROR_QUEUE_EMPTY)
          ERR_FX3(err, "CyU3PQueueReceive");
       continue;
     }
	   CyU3PThreadSleep(ev);
     m1pps_gpio_fast_set( TRANSMIT_GPIO, 0 );
     #ifdef HW_VER_1
		 //CyU3PGpioSetValue(APP_LED_GPIO, CyTrue);
     #endif
  }
}

void m1pps_send_pulse_width( uint8_t value, uint32_t width_ms )
{
  uint32_t ev = width_ms;
  CyU3PReturnStatus_t err;

  m1pps_gpio_fast_set( TRANSMIT_GPIO, 1 );

  #ifdef HW_VER_1
  //CyU3PGpioSetValue(APP_LED_GPIO, CyFalse);
  #endif

  err = CyU3PQueueSend( &pulse_queue, &ev, CYU3P_NO_WAIT );
  if ( err != CY_U3P_SUCCESS ) {
     ERR_FX3(err, "CyU3PQueueSend");
  }
}

void m1pps_send_pulse( uint8_t value )
{
  m1pps_gpio_fast_set( TRANSMIT_GPIO, (value)?1:0 );
  #ifdef HW_VER_1
  //m1pps_gpio_fast_set( APP_LED_GPIO, (value)?0:1);
  #endif
}

void m1pps_gpio_intr_cb( uint8_t gpio_num )
{
  if ( gpio_num != CLOCK_GPIO )
    return;

  if ( m1pps_inst_get_sync_mode( &inst ) != M1PPS_SLAVE_MODE_NORMAL )
    return;

  m1pps_send_pulse_width(1, PULSE_DUR_MS);
}

int m1pps_send_pulse_cb( struct m1pps_inst *inst, uint8_t value )
{
  m1pps_send_pulse( value );
  return 0;
}

int m1pps_set_mode_cb( struct m1pps_inst *inst, uint8_t mode, uint8_t start_to_change )
{
  uint8_t old_mode = m1pps_inst_get_sync_mode(inst);
  if (start_to_change) {
    //INF("Mode changing started %u -> %u", old_mode, mode);
    if (old_mode != M1PPS_SLAVE_MODE_UNKNOWN ) {
      #ifdef HW_VER_1
      m1pps_gpio_fast_set( PLL_RESET_GPIO, 1 );
      #else
      ad9548_i2c_write_full_power_down(M1PPS_AD9548_I2C_ADDR, 0);
      #endif
      ad9548_i2c_write_full_power_down(M1PPS_AD9548_I2C_ADDR, 1);
    }
  } else {
    //INF("Mode changing ended %u", mode);
    #ifdef HW_VER_1
    m1pps_gpio_fast_set( PLL_RESET_GPIO, 0 );
    #else
    ad9548_i2c_write_full_power_down(M1PPS_AD9548_I2C_ADDR, 0);
    #endif
  }

  return 0;
}

int m1pps_inst_read_pll_info_cb(m1pps_inst_t *inst, m1pps_sync_slave_pll_info_t *pi)
{
  struct ad9548_pll_sts pll_sts;
  uint8_t pll_last_sts;
  uint32_t t;
  int r = 0;

  i2c_lock(I2CBUS(M1PPS_AD9548_I2C_ADDR));

  if (ad9548_i2c_read_pll_sts(M1PPS_AD9548_I2C_ADDR, &pll_sts)) {
    r = -EIO;
    goto exit;
  }

  pll_last_sts = pll_cur_sts;

  pll_cur_sts = ad9548_pll_sts_get_status( &pll_sts );

  if ( pll_last_sts != pll_cur_sts ) {
    t = CyU3PGetTime();
    if ( pll_cur_sts & 1 ) {
      if (!(pll_last_sts & 1))
        pi->phlock_dur_ms = t - pll_phlock_start_ms;
      pi->flags |= 1;
    } else {
      pi->flags &= ~1;
      if ( pll_last_sts & 1 )
         pll_phlock_start_ms = t;
    }
    if ( pll_cur_sts & 2 ) {
      if (!(pll_last_sts & 2))
        pi->frlock_dur_ms = t - pll_frlock_start_ms;
      pi->flags |= 2;
    } else {
      pi->flags &= ~2;
      if ( pll_last_sts & 2 )
         pll_frlock_start_ms = t;
    }
  }

exit:

  i2c_unlock(I2CBUS(M1PPS_AD9548_I2C_ADDR));

  return r;
}

void m1pps_stat_thread_run(uint32_t input)
{
	CyBool_t value = CyFalse;
	long long unsigned iter = 1;
  m1pps_time_t cur_ts;
  m1pps_sync_cmn_info_t info, *i=&info;
  m1pps_sync_cmn_stat_t stat, *s=&stat;
  m1pps_trs_stat_t trs_stat, *ts=&trs_stat;
  char time_s[T_SZ], tofs_s[T_SZ] ;
  char avg_s[T_SZ], tofs2_s[T_SZ];
  uint8_t sync_mode;

	for (;;) {

    cur_ts = m1pps_clk_get_time(m1pps_clk);
    //INF("\tClock ts: %"PRIu32"/%"PRIu32, (uint32_t)(cur_ts>>32), (uint32_t)(cur_ts&UINT32_MAX) );
    time2s(cur_ts, time_s, sizeof(time_s));
    INF("\tClock ts: %s", time_s);
		if (m1pps_trs_usb_is_active(&trs_usb)) {

      m1pps_inst_get_sync_info( &inst, i );
      m1pps_inst_get_sync_stat( &inst, s );
      m1pps_inst_get_trs_stat( &inst, ts );

      tofs2s(i->delay, avg_s, sizeof(avg_s));
      tofs2s(i->offset, tofs_s, sizeof(tofs_s));

      sync_mode = m1pps_inst_get_sync_mode(&inst);

      INF("\tSync mode: %s",
          (sync_mode==M1PPS_SLAVE_MODE_NORMAL)?"Protocol":(sync_mode==M1PPS_SLAVE_MODE_DIRECTPULSE)?"Direct pulse":"None");

      if ( sync_mode == M1PPS_SLAVE_MODE_NORMAL ) {
        INF("\tSync:%u Offset:%s Delay:%s", i->proto_sync, tofs_s, avg_s);

        {
          m1pps_sync_slave_info_t sinfo, *si=&sinfo;
          m1pps_sync_slave_stat_t sstat, *ss=&sstat;

          m1pps_slave_get_info(&inst.slave, si);
          m1pps_slave_get_stat(&inst.slave, ss);

          tofs2s(si->offset_filtered, tofs_s, sizeof(tofs_s));
          tofs2s(si->delay_filtered, avg_s, sizeof(avg_s));
          INF("\tFiltered: Offset:%s Delay:%s", tofs_s, avg_s);

          tofs2s(si->offset_diff_filtered, tofs_s, sizeof(tofs_s));
          tofs2s(si->t_diff_filtered, tofs2_s, sizeof(tofs2_s));
          INF("\tDifference: Offset:%s Duration:%s", tofs_s, tofs2_s);
          tofs2s(si->period_k_adjusted, tofs_s, sizeof(tofs_s));
          INF("\tPeriodK: adjusted:%s", tofs_s);
          INF("\tAdjusted:%"PRIu32"/%"PRIu32, ss->time_adjust_cnt, ss->period_adjust_cnt);
        }

        INF("\tCycles: total:%"PRIu32" good:%"PRIu32,
            s->cyrcles, s->good_cyrcles);
        INF("\tErrors: touts:%"PRIu32" delay:%"PRIu32" sync:%"PRIu32,
            s->err_timeout, s->err_delay, s->err_sync_lost );
      }

      INF("\tUSB%d: rx:%"PRIu32" tx:%"PRIu32" errors: rx:%"PRIu32" tx:%"PRIu32,
            CyU3PUsbGetSpeed(), ts->rx, ts->tx , ts->err_rx, ts->err_tx );

      #if HW_VER_1
			CyU3PGpioGetValue(FEEDBACK_GPIO, &value);
      #endif
      #if defined(HW_VER_2) || defined(HW_VER_2A)
      value = 1;
      #endif

      #if defined(HW_VER_1) || defined(HW_VER_2)
      {
        m1pps_sync_slave_pll_info_t pllinfo, *pi=&pllinfo;
        ad9548_input_info_t  adi;

        m1pps_slave_get_pll_info(&inst.slave, pi);
        i2c_lock(I2CBUS(M1PPS_AD9548_I2C_ADDR));
			  INF("\tPLL: lock:%u duration:%u/%u", pi->flags, pi->frlock_dur_ms, pi->phlock_dur_ms );

        if (!ad9548_i2c_read_input_info(M1PPS_AD9548_I2C_ADDR, &adi)) {
	  		  INF("\tPLL: inp:%u %x prf:%u freq:%u frfreq:%u hofreq:%u",
                adi.active_ref, adi.ref_status, adi.profile_num,
                adi.freq_uHz, adi.fr_freq_uHz, adi.ho_freq_uHz);
        }

        i2c_unlock(I2CBUS(M1PPS_AD9548_I2C_ADDR));
      }
      #endif

      #if defined(HW_VER_2) || defined(HW_VER_2A)
      {
        int32_t temp;
        i2c_lock(I2CBUS(M1PPS_NCT75_I2C_ADDR));
        nct75_read_temp_reg(M1PPS_NCT75_I2C_ADDR, NCT75_REG_TEMP, &temp);
        i2c_unlock(I2CBUS(M1PPS_NCT75_I2C_ADDR));
        INF("\tTemperature:%d.%d", temp/1000, temp-temp/1000*1000);
      }
      #endif
		} else {
			/* No active data transfer. Sleep for a small amount of time. */
			CyU3PThreadSleep(100);
		}
		iter++;
		CyU3PThreadSleep(2000); //Polling delay.
	}
}

#if HW_VER_1
void m1pps_leds_thread_run(uint32_t input)
{
	CyBool_t onValue = CyFalse, skipBlink = CyFalse;
	uint32_t onPeriod = 1000, offPeriod = 1000;

	for(;;) {

		switch(pll_cur_sts) {
		default:
		case AD9548_PLLSTS_NO_LOCK:
			CyU3PGpioSetValue(PLL_LED_GPIO, !onValue);
			skipBlink = CyTrue;
			break;
		case AD9548_PLLSTS_HAS_PHASE:
			onPeriod = 900;
			offPeriod = 100;
			skipBlink = CyFalse;
			break;
		case AD9548_PLLSTS_HAS_FREQ:
			onPeriod = 100;
			offPeriod = 900;
			skipBlink = CyFalse;
			break;
		case AD9548_PLLSTS_HAS_BOTH:
			CyU3PGpioSetValue(PLL_LED_GPIO, onValue);
			skipBlink = CyTrue;
			break;
		}

		if (!skipBlink) {
			CyU3PGpioSetValue(PLL_LED_GPIO, onValue);
			CyU3PThreadSleep(onPeriod);
			CyU3PGpioSetValue(PLL_LED_GPIO, !onValue);
			CyU3PThreadSleep(offPeriod);
		}
		else {
			CyU3PThreadSleep(1000);
		}
	}
}
#endif

int m1pps_gpio_configure(uint8_t gpio_num, uint8_t output, uint8_t value )
{
  CyU3PReturnStatus_t err;
  CyU3PGpioSimpleConfig_t gpioConf;

	err = CyU3PDeviceGpioOverride(gpio_num, CyTrue);
	if (err != CY_U3P_SUCCESS) {
     ERR_FX3(err, "CyU3PDeviceGpioOverride for GPIO %u", gpio_num);
     return -EINVAL;
	}

  if (output) {
	  gpioConf.inputEn = CyFalse;
	  gpioConf.outValue = value;
	  gpioConf.driveLowEn = CyTrue;
	  gpioConf.driveHighEn = CyTrue;
  } else {
	  gpioConf.inputEn = CyTrue;
	  gpioConf.outValue = CyFalse;
	  gpioConf.driveLowEn = CyFalse;
	  gpioConf.driveHighEn = CyFalse;
  }

	gpioConf.intrMode = CY_U3P_GPIO_NO_INTR;
	err = CyU3PGpioSetSimpleConfig(gpio_num, &gpioConf); /* Set GPIO54 as an output pin to drive the LED */
	if ( err != CY_U3P_SUCCESS) {
    ERR_FX3(err, "CyU3PGpioSetSimpleConfig for GPIO %u", gpio_num);
    return -EINVAL;
	}

  return 0;
}


int m1pps_gpio_init(void)
{
  CyU3PReturnStatus_t err=CY_U3P_SUCCESS;
  uint8_t i;
	INF("GPIOs initialization (%u)...", sizeof(gpio)/sizeof(gpio[0]));

  for (i=0; i<sizeof(gpio)/sizeof(gpio[0]); ++i) {
    err = m1pps_gpio_configure( gpio[i].number, gpio[i].output, gpio[i].value );
    if ( err != CY_U3P_SUCCESS)
      break;
  }

  CyU3PRegisterGpioCallBack(m1pps_gpio_intr_cb);

  return err;
}


/* Application define function which creates the threads. */
void CyFxApplicationDefine(void)
{
	void *stat_stack, *timers_stack, *pulse_stack;
  #if HW_VER_1
	void *leds_stack;
  #endif
	CyU3PReturnStatus_t err = CY_U3P_SUCCESS;
  m1pps_inst_prm_t inst_prm = {0};
  m1pps_inst_cbs_t inst_cbs = {0};
  m1pps_trs_usb_open_data_t trs_data;
  m1pps_time_t cur_ts;
  size_t max_pkt_sz = 0;
  uint8_t dev_id;
  struct si5332_info si5332_info;

	m1pps_dbg_console_init();

  INF(" ");
	INF("* m1pps firmware application started.");
  INF(" ");

  INF("vcs: "VCSVERSION" compiled at: "__DATE__);
  INF("CPU part number:%d", CyU3PDeviceGetPartNumber());

  if ((err = m1pps_gpio_init()))
    goto error;

  if ((err = i2c_init(M1PPS_I2C_BUS_ADDR, M1PPS_I2C_BITRATE_HZ)))
    goto error;

	CyU3PThreadSleep(100);
  #ifdef HW_VER_1
  INF("Bring up PLL device...");
  CyU3PGpioSetValue( PLL_RESET_GPIO, 0 );
  #endif
  #if defined(HW_VER_2) || defined(HW_VER_2A)

  if ((err=si5332_i2c_read_info( M1PPS_SI5332_I2C_ADDR, &si5332_info )))
    goto error;

  INF("Generator rev:%x pn:%s", si5332_info.dev_rev, si5332_info.pn );

  INF("Loading generator configuration (%u regs)...", SI5332_INIT_REGS_CNT);
  if ((err=si5332_i2c_regs_write( M1PPS_SI5332_I2C_ADDR, si5332_init_regs, SI5332_INIT_REGS_CNT ) ))
    goto error;

  //INF("Bring up FPGA device...");
	//CyU3PGpioSetValue( FPGA_CFGDONE_GPIO, 0 );
  #endif

  #if !defined(HW_VER_2A)
	CyU3PThreadSleep(100);

  if ((err= ad9548_i2c_read_dev_id(M1PPS_AD9548_I2C_ADDR,  &dev_id ))) {
    INF("No PLL found don't wory show must go on", dev_id);
    //goto error;
  } else
    INF("PLL device id:0x%x ", dev_id);
  #endif

  m1pps_sys_init();

	INF("Clock initialization...");
  /* initialize system clock */
  if ((err=m1pps_clk_sys_init( )))
    goto error;

  /* initialize gpio high freq clock */
  if ((err = m1pps_clk_gpio_init(&m1pps_clk_gpio, CLOCK_GPIO, (double)FREQ_MHZ/(double)M1PPS_GPIO_FAST_CLOCK_DIV )))
    goto error;

  //m1pps_clk = m1pps_clk_sys;
  m1pps_clk = __clk(&m1pps_clk_gpio);

  cur_ts = m1pps_clk_get_time(m1pps_clk);
  INF("Clock ts: %"PRIu32"/%"PRIu32, (uint32_t)(cur_ts>>32), (uint32_t)(cur_ts&UINT32_MAX) );


  /* initialize protocol instance */
  inst_prm.role = M1PPS_PROTOCOL_ROLE;
  inst_prm.max_pkt_sz = 0;
  inst_prm.timer_period_us = M1PPS_TIMER_PERIOD_US;
  inst_prm.ctx_cnt = 0;

  /* set needed cb */
  inst_cbs.mutexes_create = m1pps_inst_mutexes_create;
  inst_cbs.mutexes_free = m1pps_inst_mutexes_free;
  inst_cbs.mutex_lock = m1pps_inst_mutex_lock;
  inst_cbs.mutex_unlock = m1pps_inst_mutex_unlock;
  inst_cbs.send_pulse = m1pps_send_pulse_cb;
  inst_cbs.set_mode = m1pps_set_mode_cb;
  inst_cbs.read_pll_info = m1pps_inst_read_pll_info_cb;

	INF("Protocol initialization as %s...",
       (inst_prm.role==M1PPS_INST_ROLE_MASTER)?"master":"slave" );
  m1pps_inst_init(&inst, &inst_prm, &inst_cbs, m1pps_clk );
  max_pkt_sz = m1pps_inst_get_max_pkt_sz(&inst);

	INF("USB transport initialization with packet size %u ...", max_pkt_sz );
  /* initialize transport */
  if ((err=m1pps_trs_usb_init( &trs_usb, m1pps_clk, max_pkt_sz )))
    goto error;

  m1pps_inst_set_trs(&inst,  __trs(&trs_usb));

  /* open transport */
  trs_data.ep[RX] = M1PPS_USB_EP_IN;
  trs_data.ep[TX] = M1PPS_USB_EP_OUT;
	INF("USB transport starting with ep: 0x%x/0x%x...",
      trs_data.ep[RX], trs_data.ep[TX]);
  if ((err=m1pps_trs_open( __trs(&trs_usb), &trs_data )))
    goto error;

	/* Allocate the memory for the threads */
	stat_stack = CyU3PMemAlloc(M1PPS_STAT_THREAD_STACK_SZ);
  timers_stack = CyU3PMemAlloc(M1PPS_TIMERS_THREAD_STACK_SZ);
  pulse_stack = CyU3PMemAlloc(M1PPS_PULSE_THREAD_STACK_SZ);

  if ( ! ( stat_stack && timers_stack && pulse_stack ) ) {
    ERR("No memory for threads stack");
    goto error;
  }

  #if HW_VER_1
	leds_stack = CyU3PMemAlloc(M1PPS_LEDS_THREAD_STACK_SZ);
  if ( ! leds_stack ) {
    ERR("No memory for threads stack");
    goto error;
  }
  #endif



	err = CyU3PThreadCreate(&m1pps_stat_thread,    /* */
                         	"stat thread",         /* Thread ID and Thread name */
                         	m1pps_stat_thread_run, /* Bulk loop App Thread Entry function */
                          0,                     /* No input parameter to thread */
                          stat_stack,            /* Pointer to the allocated thread stack */
                          M1PPS_STAT_THREAD_STACK_SZ,
                          M1PPS_STAT_THREAD_PRIORITY,
                          M1PPS_STAT_THREAD_PRIORITY,
                          CYU3P_NO_TIME_SLICE,   /* No time slice for the application thread */
                          CYU3P_AUTO_START       /* Start the Thread immediately */
                         );

	if (err != 0) {
	  goto error;
	}

  err=CyU3PQueueCreate(&pulse_queue, sizeof(pulse_events)/sizeof(pulse_events[0]),
                       pulse_events, sizeof(pulse_events));

  if ( err != CY_U3P_SUCCESS ) {
    ERR_FX3(err, "CyU3PQueueCreate");
	  goto error;
  }


	err = CyU3PThreadCreate(&m1pps_pulse_thread,
                    			"pulse thread",
                          &m1pps_pulse_thread_run,
                          0,
                          pulse_stack,
                          M1PPS_PULSE_THREAD_STACK_SZ,
                          M1PPS_PULSE_THREAD_PRIORITY,
                          M1PPS_PULSE_THREAD_PRIORITY,
                          CYU3P_NO_TIME_SLICE,
                          CYU3P_AUTO_START);

	if (err != 0) {
	  goto error;
	}

  #ifdef HW_VER_1
	err = CyU3PThreadCreate(&m1pps_leds_thread,
                    			"leds thread",
                          &m1pps_leds_thread_run,
                          0,
                          leds_stack,
                          M1PPS_LEDS_THREAD_STACK_SZ,
                          M1PPS_LEDS_THREAD_PRIORITY,
                          M1PPS_LEDS_THREAD_PRIORITY,
                          CYU3P_NO_TIME_SLICE,
                          CYU3P_AUTO_START);

	if (err != 0) {
	  goto error;
	}
  #endif

	INF("Timer ticks thread starting...");
	err = CyU3PThreadCreate(&m1pps_timers_thread,
                          "timers thread",
                          m1pps_timers_thread_run,
                          0,
                          timers_stack,
                          M1PPS_TIMERS_THREAD_STACK_SZ,
                          M1PPS_TIMERS_THREAD_PRIORITY,
                          M1PPS_TIMERS_THREAD_PRIORITY,
                          CYU3P_NO_TIME_SLICE,
                          CYU3P_AUTO_START);
	if (err != 0) {
	  goto error;
	}


	INF("Sending START message to protocol instance...");
  m1pps_inst_process_msg(&inst, M1PPS_SUBSYS_SYNC, M1PPS_MSG_START, 0, 0);


error:

	if (err != 0) {
		CyFxAppErrorHandler( err);
	}

  INF(" ");
	INF("* m1pps firmware startup complete.");
  INF(" ");

}


/*
 * Main function
 */
int main(void)
{
  uint64_t gpio_mask;
	CyU3PIoMatrixConfig_t io_cfg;
	CyU3PReturnStatus_t status = CY_U3P_SUCCESS;
	CyU3PGpioClock_t gpioClock;

	/* Initialize the device */
	CyU3PSysClockConfig_t clockConfig;
	clockConfig.setSysClk400 = CyTrue;
	//clockConfig.setSysClk400 = 0;
	clockConfig.cpuClkDiv = 2;
	clockConfig.dmaClkDiv = 2;
	clockConfig.mmioClkDiv = 2;
	clockConfig.useStandbyClk = CyFalse;
	clockConfig.clkSrc = CY_U3P_SYS_CLK;
	status = CyU3PDeviceInit(&clockConfig);
	if (status != CY_U3P_SUCCESS) {
		goto handle_fatal_error;
	}

	/* Initialize the caches. Enable both Instruction and Data Caches. */
	status = CyU3PDeviceCacheControl(CyTrue, CyTrue, CyTrue);
	if (status != CY_U3P_SUCCESS) {
		goto handle_fatal_error;
	}

	///////////////////////////////
	/* Initialize the GPIO clock. */

/*
  gpioClock.fastClkDiv = 10;
  gpioClock.slowClkDiv = 0;
  gpioClock.simpleDiv = CY_U3P_GPIO_SIMPLE_DIV_BY_2;
  gpioClock.clkSrc = CY_U3P_SYS_CLK_BY_4;
  gpioClock.halfDiv = 0;
*/


	gpioClock.fastClkDiv = M1PPS_GPIO_FAST_CLOCK_DIV;   // this is minimal availble
	gpioClock.slowClkDiv = 0;
	gpioClock.simpleDiv = CY_U3P_GPIO_SIMPLE_DIV_BY_16;
	//gpioClock.clkSrc = CY_U3P_SYS_CLK_BY_2;
	gpioClock.clkSrc = CY_U3P_SYS_CLK;
	gpioClock.halfDiv = 0;

	CyU3PGpioInit(&gpioClock, NULL);

	/* Configure the IO matrix for the device. On the FX3 DVK board, the COM port
	 * is connected to the IO(53:56). This means that either DQ32 mode should be
	 * selected or lppMode should be set to UART_ONLY. Here we are choosing
	 * UART_ONLY configuration. */
	io_cfg.isDQ32Bit = CyFalse;
	io_cfg.s0Mode = CY_U3P_SPORT_INACTIVE;
	io_cfg.s1Mode = CY_U3P_SPORT_INACTIVE;
	io_cfg.useUart = CyTrue;
	io_cfg.useI2C = CyTrue;
	io_cfg.useI2S = CyFalse;
	io_cfg.useSpi = CyFalse;
	//io_cfg.lppMode   = CY_U3P_IO_MATRIX_LPP_DEFAULT;
	io_cfg.lppMode = CY_U3P_IO_MATRIX_LPP_UART_ONLY;

	/* Enable the GPIO which would have been setup by 2-stage booter. */

  //gpio_mask = ((uint64_t)1 << APP_LED_GPIO) | ((uint64_t)1 << APP_SWITCH_GPIO);
  //

  gpio_mask = 0;
	io_cfg.gpioSimpleEn[0] = gpio_mask & UINT32_MAX;
	io_cfg.gpioSimpleEn[1] = gpio_mask >> 32;


  gpio_mask = ((uint64_t)1 << CLOCK_GPIO);
	io_cfg.gpioComplexEn[0] = gpio_mask & UINT32_MAX;
	io_cfg.gpioComplexEn[1] = gpio_mask >> 32 ;

	status = CyU3PDeviceConfigureIOMatrix(&io_cfg);
	if (status != CY_U3P_SUCCESS) {
		goto handle_fatal_error;
	}

	/* This is a non returnable call for initializing the RTOS kernel */
	CyU3PKernelEntry();

	/* Dummy return to make the compiler happy */
	return 0;

handle_fatal_error:

	/* Cannot recover from this error. */
	CyFxAppErrorHandler(status);

  return 0;
}


