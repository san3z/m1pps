/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "cyu3types.h"

/* Extern definitions for the USB Descriptors */
extern const uint8_t CyFxUSB20DeviceDscr[];
extern const uint8_t CyFxUSB30DeviceDscr[];
extern const uint8_t CyFxUSBDeviceQualDscr[];
extern const uint8_t CyFxUSBFSConfigDscr[];
extern const uint8_t CyFxUSBHSConfigDscr[];
extern const uint8_t CyFxUSBBOSDscr[];
extern const uint8_t CyFxUSBSSConfigDscr[];
extern const uint8_t CyFxUSBStringLangIDDscr[];
extern const uint8_t CyFxUSBManufactureDscr[];
extern const uint8_t CyFxUSBProductDscr[];

#define M1PPS_USB_DESC_DEVICE_SS   (0)
#define M1PPS_USB_DESC_DEVICE_HS   (1)
#define M1PPS_USB_DESC_BOS_SS      (2)
#define M1PPS_USB_DESC_DEVQUAL     (3)
#define M1PPS_USB_DESC_CONFIG_SS   (4)
#define M1PPS_USB_DESC_CONFIG_HS   (5)
#define M1PPS_USB_DESC_CONFIG_FS   (6)
#define M1PPS_USB_DESC_LANG        (7)
#define M1PPS_USB_DESC_MANUFACTURE (8)
#define M1PPS_USB_DESC_PRODUCT     (9)


const void *m1pps_usb_desc_get( int decsc_id );
