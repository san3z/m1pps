
SUBDIRS := lin fx3 

SUBDIRSCLEAN=$(addsuffix _clean,$(SUBDIRS))

.PHONY: $(SUBDIRS) clean

lin%: 
	make -C lin $@ 

fx3%:
	make -C fx3 $@
	
default: lin-mod

.ONESHELL:
tar: clean 
	NAME="$$(basename $$(dirname $$(realpath Makefile)))"
	tar --exclude tags --exclude "../$${NAME}/.git*" -cvf  ../$${NAME}.tgz ../$${NAME}   

clean: $(SUBDIRSCLEAN)

%_clean: % 
	$(MAKE) -C $< clean 

