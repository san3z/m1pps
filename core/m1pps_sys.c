/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_sys.h"

#ifdef __KERNEL__

void m1pps_sys_init(void)
{
}

void m1pps_sys_fi(void)
{
}

#endif

#ifdef __FX3__

#if BYTE_ORDER == LITTLE_ENDIAN

uint64_t sys_swap_64(uint64_t v)
{
  uint8_t d[8];
  (*(uint64_t*)d) = v;

  return ((uint64_t) d[7] << 0) |
         ((uint64_t) d[6] << 8) |
         ((uint64_t) d[5] << 16) |
         ((uint64_t) d[4] << 24) |
         ((uint64_t) d[3] << 32) |
         ((uint64_t) d[2] << 40) |
         ((uint64_t) d[1] << 48) |
         ((uint64_t) d[0] << 56);
}

uint32_t sys_swap_32(uint32_t v)
{
  uint8_t d[8];
  (*(uint32_t*)d) = v;

  return ((uint64_t) d[3] << 0) |
         ((uint64_t) d[2] << 8) |
         ((uint64_t) d[1] << 16) |
         ((uint64_t) d[0] << 24) ;
}

uint16_t sys_swap_16(uint16_t v)
{
  uint8_t d[2];
  (*(uint16_t*)d) = v;

  return ((uint16_t) d[1] << 0) |
         ((uint16_t) d[0] << 8);
}


#endif

void m1pps_sys_init(void)
{
}

void m1pps_sys_fi(void)
{
}

#include <stdarg.h>
//#define snprintf(s,sz,fmt,...) { CyU3PDebugStringPrint( (uint8_t*)s, sz, fmt, ##__VA_ARGS__ ); s[sz-1]=0; }

/*
int __snprintf(char *s,size_t sz, const char *fmt,...)
{
  va_list args;
  va_start( args, fmt );
  CyU3PDebugStringPrint( (uint8_t*)s, sz, (char*)fmt, args );
  va_end( args);
  s[sz-1]=0;
  return strlen(s);
}
*/

#endif // __FX3

#define _1E9 (1000000000)

// ---- common functions
void u32_to_s(uint32_t v, char *str, size_t str_sz)
{
   size_t len;
   snprintf(str, str_sz, "%"PRIu32, v);
   len = strlen(str);
   if ( len < (str_sz - 1)) {
     size_t l = str_sz - len - 1, i;
     for (i = 0; i<=len; ++i) str[str_sz-1-i] = str[len-i];
     memset(str,'0', l);
   }
}

size_t _time2s(m1pps_time_t time_ns, char *str, size_t str_sz, char sign)
{
  m1pps_time_t sc;
  uint8_t h, m, s;
  uint32_t dc, hc, mc;
  char ns_str[10];

  sc = (time_ns/_1E9);
  dc = sc/(3600*24);
  hc = sc/3600;
  mc = sc/60;
  h = hc-dc*24;
  m = mc-hc*60;
  s = sc-mc*60;

  time_ns = time_ns - sc*_1E9;
  u32_to_s(time_ns, ns_str, sizeof(ns_str) );

  if (sign) {
    str[0]=sign;
    str++;
    str_sz--;
    return snprintf(str, str_sz, "%"PRIu32"/%u:%u:%u.%s",
                    dc, h, m, s, ns_str);
  } else {

    uint8_t mn, fdc;
    uint32_t y;

    y=dc/1461*4;
    y+=1970;
    fdc = (y>1972) ? ( ( (y-1972) & 0x3 ) ? 28 : 29) : 28;
    dc%=1461;
    if (dc>=730) {dc-=730; y+=2;}     // 2 years
    if (dc>=365) {dc-=365; y++;};     // 1 year
    mn = 1;
    if (dc>31) {dc-=31; mn+=1;};
    if (dc>fdc) {dc-=fdc; mn+=1;};
    if (dc>153) {dc-=153; mn+=5;};

    if (dc>122) {dc-=122; mn+=4;}     // here, 122 days mean 4 months
    else if (dc>=92) {dc-=92; mn+=3;}     // here, 122 days mean 4 months
      else if (dc>=61) {dc-=61; mn+=2;}  // 61 days mean 2 months
        else if (dc>=31) {dc-=31; mn++;}        // 31 days means exactly 1 month

    return snprintf(str, str_sz, "%"PRIu32".%u.%u %u:%u:%u.%s",
                    y, mn, dc, h, m, s, ns_str);
  }

}

size_t time2s(m1pps_time_t time_ns, char *str, size_t str_sz)
{
  return _time2s(time_ns, str, str_sz, 0);
}

size_t tofs2s(m1pps_tofs_t tofs_ns, char *str, size_t str_sz)
{
  char sign;
  if ( tofs_ns < 0 ) {
    sign = '-';
    tofs_ns*=-1;
  } else {
    sign = ' ';
  }
  return _time2s(tofs_ns, str, str_sz, sign);
}

char *__strncpy(char *d, const char *s, size_t sz)
{
  strncpy(d,s,sz); d[sz-1]=0; return d;
}
