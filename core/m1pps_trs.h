/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_sys.h"
#include "m1pps_clk.h"

struct m1pps_trs;

typedef struct m1pps_trs_ops {
  int (*open)( struct m1pps_trs *t, void *opendata );
  int (*close)( struct m1pps_trs *t );
  int (*send_pkt)( struct m1pps_trs *t, const void *data, size_t data_sz );
} m1pps_trs_ops_t;

typedef struct m1pps_trs_cbs {
  int (*pkt_sended)( struct m1pps_trs *t, m1pps_time_t ts );
  int (*pkt_recieved)( struct m1pps_trs *t, const void *data, size_t data_sz, m1pps_time_t ts );
} m1pps_trs_cbs_t;

typedef struct m1pps_trs_stat {
  uint32_t rx;
  uint32_t tx;
  uint32_t err_rx;
  uint32_t err_tx;
} m1pps_trs_stat_t;

typedef struct m1pps_trs {
  const char *name;
  m1pps_clk_t *clk;
  m1pps_trs_stat_t stat;
  m1pps_trs_ops_t ops;
  m1pps_trs_cbs_t cbs;
  void *ctx;
} m1pps_trs_t;

/* public */
int m1pps_trs_open( m1pps_trs_t *t, void *open_data );
int m1pps_trs_close( m1pps_trs_t *t);
int m1pps_trs_send_pkt( m1pps_trs_t *t, const void *data, size_t data_sz );
void m1pps_trs_set_cbs( m1pps_trs_t *t, const m1pps_trs_cbs_t *cbs );
void m1pps_trs_set_ctx( m1pps_trs_t *t, void *ctx );
void *m1pps_trs_get_ctx( const m1pps_trs_t *t );
void m1pps_trs_get_stat( const m1pps_trs_t *t, m1pps_trs_stat_t *stat );

/* private */
int m1pps_trs_init( m1pps_trs_t *t, const m1pps_trs_ops_t *ops, m1pps_clk_t *clk );
void m1pps_trs_destroy( m1pps_trs_t *t);

m1pps_time_t m1pps_trs_get_time( const m1pps_trs_t *t );
int m1pps_trs_call_pkt_sended( struct m1pps_trs *t, m1pps_time_t ts );
int m1pps_trs_call_pkt_recieved( struct m1pps_trs *t, const void *data, size_t data_sz, m1pps_time_t ts );


#define __trs(c) (&((c)->trs))
