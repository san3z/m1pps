/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_pkt_fpga.h"
#include "m1pps_msg_fpga.h"

// fpga subsys

#define M1PPS_PKTSUBSYS_FPGA        (2)

#define M1PPS_PKTID_FPGA_INFREQ      (0)
#define M1PPS_PKTID_FPGA_INFRSP      (1)
#define M1PPS_PKTID_FPGA_STSREQ      (2)
#define M1PPS_PKTID_FPGA_STSRSP      (3)
#define M1PPS_PKTID_FPGA_LINITREQ    (4)
#define M1PPS_PKTID_FPGA_LINITRSP    (5)
#define M1PPS_PKTID_FPGA_LDATAREQ    (6)
#define M1PPS_PKTID_FPGA_LDATARSP    (7)
#define M1PPS_PKTID_FPGA_LOADREQ     (8)
#define M1PPS_PKTID_FPGA_LOADRSP     (9)
#define M1PPS_PKTID_FPGA_RXREQ      (10)
#define M1PPS_PKTID_FPGA_RXRSP      (11)
#define M1PPS_PKTID_FPGA_TXREQ      (12)
#define M1PPS_PKTID_FPGA_TXRSP      (13)

#pragma pack(push, 1)

#define __M1PPS_PKT_FPGA_RSP \
  int16_t ret_code;

#define __M1PPS_PKT_FPGA_CHIPRSP \
  int16_t ret_code; \
  uint8_t chip_id

#define __M1PPS_PKT_FPGA_CHIPREQ \
  uint8_t chip_id

typedef struct m1pps_pkt_fpga_chiprsp {
  __M1PPS_PKT_FPGA_CHIPRSP;
} m1pps_pkt_fpga_chiprsp_t;

typedef struct m1pps_pkt_fpga_chipreq {
  __M1PPS_PKT_FPGA_CHIPREQ;
} m1pps_pkt_fpga_chipreq_t;

typedef struct m1pps_pkt_fpga_chip_sts_rsp {
  __M1PPS_PKT_FPGA_CHIPRSP;
  uint8_t status;
} m1pps_pkt_fpga_chip_sts_rsp_t;

typedef struct m1pps_pkt_fpgachip_desc {
  char name[16];
  uint8_t chip_id;
} m1pps_pkt_fpgachip_desc_t;

typedef struct m1pps_pkt_fpga_infreq {
} m1pps_pkt_fpga_infreq_t;

typedef struct m1pps_pkt_fpga_infrsp {
  __M1PPS_PKT_FPGA_RSP;
  uint32_t max_load_sz;
  uint8_t  chip_cnt;
  m1pps_pkt_fpgachip_desc_t chip[0];
} m1pps_pkt_fpga_infrsp_t;

typedef m1pps_pkt_fpga_chipreq_t m1pps_pkt_fpga_stsreq_t;
typedef m1pps_pkt_fpga_chip_sts_rsp_t m1pps_pkt_fpga_stsrsp_t;

typedef m1pps_pkt_fpga_chipreq_t m1pps_pkt_fpga_linitreq_t;
typedef m1pps_pkt_fpga_chip_sts_rsp_t m1pps_pkt_fpga_linitrsp_t;

typedef struct m1pps_pkt_fpga_ldatareq {
  __M1PPS_PKT_FPGA_CHIPREQ;
  uint32_t offset;
  uint8_t  data[0];
} m1pps_pkt_fpga_ldatareq_t;

typedef m1pps_pkt_fpga_chip_sts_rsp_t m1pps_pkt_fpga_ldatarsp_t;

typedef struct m1pps_pkt_fpga_loadreq {
  __M1PPS_PKT_FPGA_CHIPREQ;
  uint32_t offset;
  uint32_t size;
} m1pps_pkt_fpga_loadreq_t;

typedef m1pps_pkt_fpga_chip_sts_rsp_t m1pps_pkt_fpga_loadrsp_t;

typedef m1pps_pkt_fpga_chiprsp_t m1pps_pkt_fpga_loadcmpl_t;

typedef m1pps_pkt_fpga_chipreq_t m1pps_pkt_fpga_startload_t;

typedef struct m1pps_pkt_fpga_rxreq {
  __M1PPS_PKT_FPGA_CHIPREQ;
} m1pps_pkt_fpga_rxreq_t;

typedef struct m1pps_pkt_fpga_rxrsp {
  __M1PPS_PKT_FPGA_CHIPRSP;
  uint16_t size;
  uint8_t data[0];
} m1pps_pkt_fpga_rxrsp_t;

typedef struct m1pps_pkt_fpga_txreq {
  __M1PPS_PKT_FPGA_CHIPREQ;
  uint16_t size;
  uint8_t data[0];
} m1pps_pkt_fpga_txreq_t;

typedef m1pps_pkt_fpga_chiprsp_t m1pps_pkt_fpga_txrsp_t;

#pragma pack(pop)

static
void __copy_sts_rsp_to_pkt(void *pkt, const void *msg)
{
   m1pps_pkt_fpga_chip_sts_rsp_t *p = pkt;
   const m1pps_msg_fpga_chip_sts_rsp_t *m = msg;
   p->ret_code = hton_16(m->ret_code);
   p->chip_id = m->chip_id;
   p->status = m->status;
}

size_t _m1pps_pkt_subsys_fpga_pack_msg( m1pps_pkt_t *pkt, size_t offset, int msg_id,
                                        const void *msg_data, size_t msg_data_sz, uint8_t *pkt_id)
{
   size_t sz=0, max_sz = pkt->buf_sz - offset;
   void * pkt_body = pkt->buf + offset;

   switch ( msg_id ) {
     case M1PPS_MSG_FPGA_INFREQ: {
       m1pps_pkt_fpga_infreq_t *p = pkt_body;
       //const m1pps_msg_fpga_infreq_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_FPGA_INFREQ;
     } break;
     case M1PPS_MSG_FPGA_INFRSP: {
       uint8_t i;
       m1pps_pkt_fpga_infrsp_t *p = pkt_body;
       const m1pps_msg_fpga_infrsp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->ret_code = hton_16(m->ret_code);
       p->chip_cnt = m->chip_cnt;
       if ( (max_sz-sz) < p->chip_cnt*sizeof(p->chip[0]) )
         return 0;
       for (i=0; i<p->chip_cnt;++i) {
         __strncpy(p->chip[i].name, m->chip[i].name, sizeof(p->chip[i].name));
         p->chip[i].chip_id = m->chip[i].chip_id;
       }
       sz += p->chip_cnt*sizeof(p->chip[0]);
       *pkt_id = M1PPS_PKTID_FPGA_INFRSP;
     } break;
     case M1PPS_MSG_FPGA_STSREQ: {
       m1pps_pkt_fpga_stsreq_t *p = pkt_body;
       const m1pps_msg_fpga_stsreq_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->chip_id = m->chip_id;
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_FPGA_STSREQ;
     } break;
     case M1PPS_MSG_FPGA_STSRSP: {
       m1pps_pkt_fpga_stsrsp_t *p = pkt_body;
       const m1pps_msg_fpga_stsrsp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       __copy_sts_rsp_to_pkt(p, m);
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_FPGA_STSRSP;
     } break;
     case M1PPS_MSG_FPGA_LINITREQ: {
       m1pps_pkt_fpga_linitreq_t *p = pkt_body;
       const m1pps_msg_fpga_linitreq_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->chip_id = m->chip_id;
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_FPGA_LINITREQ;
     } break;
     case M1PPS_MSG_FPGA_LINITRSP: {
       m1pps_pkt_fpga_linitrsp_t *p = pkt_body;
       const m1pps_msg_fpga_linitrsp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       __copy_sts_rsp_to_pkt(p, m);
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_FPGA_LINITRSP;
     } break;
     case M1PPS_MSG_FPGA_LDATAREQ: {
       m1pps_pkt_fpga_ldatareq_t *p = pkt_body;
       const m1pps_msg_fpga_ldatareq_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->chip_id = m->chip_id;
       p->offset = hton_32(m->offset);
       sz += sizeof(*p);
       if ((max_sz-sz) < (msg_data_sz-sizeof(*m)))
         return 0;
       memmove(p->data, m->data, msg_data_sz-sizeof(*m));
       sz += msg_data_sz-sizeof(*m);
       *pkt_id = M1PPS_PKTID_FPGA_LDATAREQ;
     } break;
     case M1PPS_MSG_FPGA_LDATARSP: {
       m1pps_pkt_fpga_ldatarsp_t *p = pkt_body;
       const m1pps_msg_fpga_ldatarsp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       __copy_sts_rsp_to_pkt(p, m);
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_FPGA_LDATARSP;
     } break;
     case M1PPS_MSG_FPGA_LOADREQ: {
       m1pps_pkt_fpga_loadreq_t *p = pkt_body;
       const m1pps_msg_fpga_loadreq_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->chip_id = m->chip_id;
       p->offset = hton_32(m->offset);
       p->size = hton_32(m->size);
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_FPGA_LOADREQ;
     } break;
     case M1PPS_MSG_FPGA_LOADRSP: {
       m1pps_pkt_fpga_loadrsp_t *p = pkt_body;
       const m1pps_msg_fpga_loadrsp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       __copy_sts_rsp_to_pkt(p, m);
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_FPGA_LOADRSP;
     } break;
     case M1PPS_MSG_FPGA_RXREQ: {
       m1pps_pkt_fpga_rxreq_t *p = pkt_body;
       const m1pps_msg_fpga_rxreq_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->chip_id = m->chip_id;
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_FPGA_RXREQ;
     } break;
     case M1PPS_MSG_FPGA_RXRSP: {
       m1pps_pkt_fpga_rxrsp_t *p = pkt_body;
       const m1pps_msg_fpga_rxrsp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->ret_code = hton_16(m->ret_code);
       p->size = hton_16(m->size);
       sz += sizeof(*p);
       if ( (max_sz-sz) < m->size )
         return 0;
       memcpy(p->data, m->data, m->size);
       sz += m->size;
       *pkt_id = M1PPS_PKTID_FPGA_RXRSP;
     } break;
     case M1PPS_MSG_FPGA_TXREQ: {
       m1pps_pkt_fpga_txreq_t *p = pkt_body;
       const m1pps_msg_fpga_txreq_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->size = hton_16(m->size);
       p->chip_id = m->chip_id;
       sz += sizeof(*p);
       if ( (max_sz-sz) < m->size )
         return 0;
       memcpy( p->data, m->data, m->size);
       sz += sizeof(m->size);
       *pkt_id = M1PPS_PKTID_FPGA_TXREQ;
     } break;
     case M1PPS_MSG_FPGA_TXRSP: {
       m1pps_pkt_fpga_txrsp_t *p = pkt_body;
       const m1pps_msg_fpga_txrsp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->ret_code = hton_16(m->ret_code);
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_FPGA_TXRSP;
     } break;
     default: return 0;
   }

   return sz;
}

static
void __copy_sts_rsp_to_msg(void *msg, const void *pkt)
{
   const m1pps_pkt_fpga_chip_sts_rsp_t *p = pkt;
   m1pps_msg_fpga_chip_sts_rsp_t *m = msg;
   m->ret_code = ntoh_16(p->ret_code);
   m->chip_id = p->chip_id;
   m->status = p->status;
}

int _m1pps_pkt_subsys_fpga_unpack_msg( const m1pps_pkt_t *pkt, const m1pps_pkt_up_args_t *args,
                                       int *msg_id, void *msg_data, size_t *msg_data_sz)
{
   size_t sz=0;
   void *pkt_body =  pkt->buf + args->body_ofs;
   switch ( args->id ) {
     case M1PPS_PKTID_FPGA_INFREQ: {
       //const m1pps_pkt_fpga_infreq_t *p = pkt_body;
       m1pps_msg_fpga_infreq_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_FPGA_INFREQ;
     } break;
     case M1PPS_PKTID_FPGA_INFRSP: {
       uint8_t i;
       const m1pps_pkt_fpga_infrsp_t *p = pkt_body;
       m1pps_msg_fpga_infrsp_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->ret_code = ntoh_16(p->ret_code);
       m->chip_cnt = p->chip_cnt;
       sz += sizeof(*m);
       if ( (*msg_data_sz-sz) < m->chip_cnt*sizeof(m->chip[0]) )
         return -EINVAL;
       for (i=0; i<p->chip_cnt;++i) {
         __strncpy(m->chip[i].name, p->chip[i].name, sizeof(m->chip[i].name));
         m->chip[i].chip_id = p->chip[i].chip_id;
       }
       sz += m->chip_cnt*sizeof(m->chip[0]);
       *msg_id = M1PPS_MSG_FPGA_INFRSP;
     } break;
     case M1PPS_PKTID_FPGA_STSREQ: {
       const m1pps_pkt_fpga_stsreq_t *p = pkt_body;
       m1pps_msg_fpga_stsreq_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->chip_id = p->chip_id;
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_FPGA_STSREQ;
     } break;
     case M1PPS_PKTID_FPGA_STSRSP: {
       const m1pps_pkt_fpga_stsrsp_t *p = pkt_body;
       m1pps_msg_fpga_stsrsp_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       __copy_sts_rsp_to_msg(m, p);
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_FPGA_STSRSP;
     } break;
     case M1PPS_PKTID_FPGA_LINITREQ: {
       const m1pps_pkt_fpga_linitreq_t *p = pkt_body;
       m1pps_msg_fpga_linitreq_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->chip_id = p->chip_id;
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_FPGA_LINITREQ;
     } break;
     case M1PPS_PKTID_FPGA_LINITRSP: {
       const m1pps_pkt_fpga_linitrsp_t *p = pkt_body;
       m1pps_msg_fpga_linitrsp_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       __copy_sts_rsp_to_msg(m, p);
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_FPGA_LINITRSP;
     } break;
     case M1PPS_PKTID_FPGA_LDATAREQ: {
       const m1pps_pkt_fpga_ldatareq_t *p = pkt_body;
       m1pps_msg_fpga_ldatareq_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->chip_id = p->chip_id;
       m->offset = hton_32(m->offset);
       sz += sizeof(*m);
       if ( (*msg_data_sz - sz ) < (args->body_sz - sizeof(*p)) )
         return -EINVAL;
       memmove(m->data, p->data, args->body_sz - sizeof(*p));
       sz += args->body_sz - sizeof(*p);
       *msg_id = M1PPS_MSG_FPGA_LDATAREQ;
     } break;
     case M1PPS_PKTID_FPGA_LDATARSP: {
       const m1pps_pkt_fpga_ldatarsp_t *p = pkt_body;
       m1pps_msg_fpga_ldatarsp_t *m = msg_data;
       if ((*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       __copy_sts_rsp_to_msg(m, p);
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_FPGA_LDATARSP;
     } break;
     case M1PPS_PKTID_FPGA_LOADREQ: {
       const m1pps_pkt_fpga_loadreq_t *p = pkt_body;
       m1pps_msg_fpga_loadreq_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->chip_id = p->chip_id;
       m->offset = ntoh_32(p->offset);
       m->size = ntoh_32(p->size);
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_FPGA_LOADREQ;
     } break;
     case M1PPS_PKTID_FPGA_LOADRSP: {
       const m1pps_pkt_fpga_loadrsp_t *p = pkt_body;
       m1pps_msg_fpga_loadrsp_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       __copy_sts_rsp_to_msg(m, p);
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_FPGA_LOADRSP;
     } break;
     case M1PPS_PKTID_FPGA_RXREQ: {
       const m1pps_pkt_fpga_rxreq_t *p = pkt_body;
       m1pps_msg_fpga_rxreq_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->chip_id = p->chip_id;
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_FPGA_RXREQ;
     } break;
     case M1PPS_PKTID_FPGA_RXRSP: {
       const m1pps_pkt_fpga_rxrsp_t *p = pkt_body;
       m1pps_msg_fpga_rxrsp_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->ret_code = ntoh_16(p->ret_code);
       m->chip_id = p->chip_id;
       m->size = ntoh_16(p->size);
       if ( (*msg_data_sz - sz ) < m->size)
         return -EINVAL;
       memcpy( m->data, p->data, m->size);
       sz += m->size;
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_FPGA_RXRSP;
     } break;
     case M1PPS_PKTID_FPGA_TXREQ: {
       const m1pps_pkt_fpga_txreq_t *p = pkt_body;
       m1pps_msg_fpga_txreq_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->size = ntoh_16(p->size);
       m->chip_id = p->chip_id;
       sz += sizeof(*m);
       if ( (*msg_data_sz - sz ) < m->size)
         return -EINVAL;
       memcpy( m->data, p->data, m->size);
       sz += m->size;
       *msg_id = M1PPS_MSG_FPGA_TXREQ;
     } break;
     case M1PPS_PKTID_FPGA_TXRSP: {
       const m1pps_pkt_fpga_txrsp_t *p = pkt_body;
       m1pps_msg_fpga_txrsp_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->ret_code = ntoh_16(p->ret_code);
       m->chip_id = p->chip_id;
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_FPGA_TXRSP;
     } break;
     default: return -EINVAL;
   }

   *msg_data_sz = sz;
   return 0;
}

#define __max(v1, v2) ((v1 < v2) ? v2 : v1)
#define MAX_FPGA_DATA_SZ (256)

size_t _m1pps_pkt_subsys_fpga_get_max_sz(void)
{
  size_t sz = 0;
  sz = __max(sz, sizeof( m1pps_pkt_fpga_infreq_t));
  sz = __max(sz, sizeof( m1pps_pkt_fpga_infrsp_t));
  sz = __max(sz, sizeof( m1pps_pkt_fpga_txreq_t));
  sz = __max(sz, sizeof( m1pps_pkt_fpga_txrsp_t));
  sz = __max(sz, sizeof( m1pps_pkt_fpga_rxreq_t));
  sz = __max(sz, sizeof( m1pps_pkt_fpga_rxrsp_t));
  return sz;
}

m1pps_pkt_subsys_t m1pps_pkt_subsys_fpga = {
  .name = "fpga",
  .id =  M1PPS_PKTSUBSYS_FPGA,
  .pack_msg =  _m1pps_pkt_subsys_fpga_pack_msg,
  .unpack_msg = _m1pps_pkt_subsys_fpga_unpack_msg,
  .get_max_pkt_sz = _m1pps_pkt_subsys_fpga_get_max_sz
};


