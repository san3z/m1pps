/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_fsm.h"
#include "m1pps_sys.h"

#define M1PPS_MSG_INT         (1<<8)
#define M1PPS_MSG_START       (M1PPS_MSG_INT + 0)
#define M1PPS_MSG_STOP        (M1PPS_MSG_INT + 1)

#define M1PPS_MSG_I2C_INFREQ      (FSM_MSG_USER+0)
#define M1PPS_MSG_I2C_INFRSP      (FSM_MSG_USER+1)
#define M1PPS_MSG_I2C_TXREQ       (FSM_MSG_USER+2)
#define M1PPS_MSG_I2C_TXRSP       (FSM_MSG_USER+3)
#define M1PPS_MSG_I2C_RXREQ       (FSM_MSG_USER+4)
#define M1PPS_MSG_I2C_RXRSP       (FSM_MSG_USER+5)

typedef struct m1pps_msg_i2c_infreq {
} m1pps_msg_i2c_infreq_t;

typedef struct m1pps_msg_i2cbus_desc {
  char name[16];
  uint8_t bus_id;
} m1pps_msg_i2cbus_desc_t;

typedef struct m1pps_msg_i2c_infrsp {
  uint16_t ret_code;
  uint8_t  bus_cnt;
  m1pps_msg_i2cbus_desc_t bus[0];
} m1pps_msg_i2c_infrsp_t;

typedef struct m1pps_msg_i2c_rxreq {
  uint8_t bus_id;
} m1pps_msg_i2c_rxreq_t;

typedef struct m1pps_msg_i2c_rxrsp {
  int16_t ret_code;
  uint8_t bus_id;
  uint16_t size;
  uint8_t data[0];
} m1pps_msg_i2c_rxrsp_t;

typedef struct m1pps_msg_i2c_txreq {
  uint8_t bus_id;
  uint16_t size;
  uint8_t data[0];
} m1pps_msg_i2c_txreq_t;

typedef struct m1pps_msg_i2c_txrsp {
  uint16_t ret_code;
  uint8_t bus_id;
} m1pps_msg_i2c_txrsp_t;


