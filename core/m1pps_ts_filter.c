/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_ts_filter.h"

int m1pps_ts_filter_init( m1pps_ts_filter_t *f, uint8_t type_cnt, uint16_t ts_cnt, uint8_t int_cnt )
{
   uint8_t *m;
   size_t sz=0;

   f->int_cnt = int_cnt;
   f->type_cnt = type_cnt;
   f->ts_cnt = ts_cnt;

   sz += sizeof(m1pps_ts_int_t)*f->int_cnt*f->type_cnt;
   sz += sizeof(m1pps_ts_type_t)*f->type_cnt;
   sz += sizeof(m1pps_time_t)*f->type_cnt*f->ts_cnt*2;
   m = sys_zalloc( sz );
   if (!m)
     return -ENOMEM;

   f->intervals = (m1pps_ts_int_t*) m;
   m += sizeof(m1pps_ts_int_t)*f->int_cnt*f->type_cnt;
   f->types = (m1pps_ts_type_t*)m;
   m += sizeof(m1pps_ts_type_t)*f->type_cnt;
   f->ts = (m1pps_time_t*)m;

   return 0;
}

void m1pps_ts_filter_destroy( m1pps_ts_filter_t *f )
{
   sys_free( f->intervals );
}

void m1pps_ts_filter_set_type_sign( m1pps_ts_filter_t *f, uint8_t ts_type, uint8_t sign  )
{
  m1pps_ts_type_t *type = f->types + ts_type;
  type->sign = sign;
}

void m1pps_ts_filter_clear_type_values( m1pps_ts_filter_t *f, uint8_t ts_type )
{
  m1pps_ts_type_t *type = f->types + ts_type;
  type->widx = 0;
  type->ridx = 0;
}

void m1pps_ts_filter_clear_type( m1pps_ts_filter_t *f, uint8_t ts_type )
{
  m1pps_ts_type_t *type = f->types + ts_type;

  type->widx = 0;
  type->ridx = 0;
  type->range_unchanged_cnt = 0;

  if (type->sign) {
    type->min_tofs = INT64_MAX;
    type->max_tofs = INT64_MIN;
  } else {
    type->min_ts = UINT64_MAX;
    type->max_ts = 0;
  }
}

void m1pps_ts_filter_clear( m1pps_ts_filter_t *f)
{
  uint8_t t;
  for (t=0;t<f->type_cnt;++t)
    m1pps_ts_filter_clear_type(f, t);
}

void m1pps_ts_filter_clear_values( m1pps_ts_filter_t *f)
{
  uint8_t t;
  for (t=0;t<f->type_cnt;++t)
    m1pps_ts_filter_clear_type_values(f, t);
}

uint16_t m1pps_ts_filter_ts_count( m1pps_ts_filter_t *f, uint8_t ts_type )
{
  m1pps_ts_type_t *type = f->types + ts_type;

  if (type->widx==UINT16_MAX)
    return f->ts_cnt;

  if (type->widx < type->ridx)
    return f->ts_cnt - (type->ridx-type->widx);
  else
    return type->widx-type->ridx;
}

uint16_t m1pps_ts_filter_prepare_add( m1pps_ts_filter_t *f, uint8_t ts_type)
{
  uint16_t ts_idx;
  m1pps_ts_type_t *type = f->types + ts_type;

  DBG("t:%u r:%u w:%u", ts_type, type->ridx, type->widx);

  if (type->widx == UINT16_MAX) {
    ts_idx = type->ridx;
    type->ridx = ( type->ridx >= (f->ts_cnt-1) ) ? 0 : type->ridx+1;
  } else {
    ts_idx = type->widx;
    type->widx = ( type->widx >= (f->ts_cnt-1) ) ? 0 : type->widx+1;
    if (type->widx==type->ridx)
      type->widx = UINT16_MAX;
  }
  return ts_idx;
}

void m1pps_ts_filter_add_ts( m1pps_ts_filter_t *f, uint8_t ts_type,  const m1pps_time_t ts)
{
  uint16_t ts_idx = m1pps_ts_filter_prepare_add( f, ts_type ) ;
  f->ts[ts_type*f->ts_cnt + ts_idx ] = ts;
}

void m1pps_ts_filter_add_tofs( m1pps_ts_filter_t *f, uint8_t ts_type,  const m1pps_tofs_t tofs)
{
  uint16_t ts_idx = m1pps_ts_filter_prepare_add( f, ts_type ) ;
#ifdef FILTER_DEBUG
  char t_str1[T_SZ];
  tofs2s( tofs, t_str1, sizeof(t_str1));
  DBG("add_tofs: %u %s", ts_idx, t_str1);
#endif
  f->tofs[ts_type*f->ts_cnt + ts_idx] = tofs;
}

void _m1pps_ts_filter_add_all_ts( m1pps_ts_filter_t *f, const m1pps_time_t *ts)
{
  uint8_t t;
  for (t=0; t<f->type_cnt; ++t) {
    m1pps_ts_filter_add_ts( f, t, ts[t] );
  }
}

void m1pps_ts_filter_calc( m1pps_ts_filter_t *f )
{
  uint8_t i, t, first;
  m1pps_ts_type_t *type;
  m1pps_ts_int_t  *intv;

#ifdef FILTER_DEBUG
    char t_str1[T_SZ];
    char t_str2[T_SZ];
#endif

  for (t=0;t<f->type_cnt;++t) {

    uint16_t ts_idx, ts_cnt, cnt;
    m1pps_time_t ts,st;
    m1pps_tofs_t tofs;

    type = f->types + t;


    ts_idx = type->ridx;
    ts_cnt = cnt = m1pps_ts_filter_ts_count(f, t);
    DBG("t:%u from:%u cnt:%u", t, ts_idx, ts_cnt);

    while (cnt--) {
      if (type->sign) {
        tofs = f->tofs[t*f->ts_cnt + ts_idx];
        if ( tofs < type->min_tofs ) {
          type->min_tofs = tofs;
          type->range_unchanged_cnt = 0;
        }
        if ( tofs > type->max_tofs ) {
          type->max_tofs = tofs;
          type->range_unchanged_cnt = 0;
        }
      } else {
        ts = f->ts[t*f->ts_cnt + ts_idx];
        if ( ts < type->min_ts ) {
          type->min_ts = ts;
          type->range_unchanged_cnt = 0;
        }
        if ( ts > type->max_ts ) {
          type->max_ts = ts;
          type->range_unchanged_cnt = 0;
        }
      }
      //ts_idx = ( type->ridx >= (f->ts_cnt-1) ) ? 0 : ts_idx+1;
      ts_idx = ( ts_idx >= (f->ts_cnt-1) ) ? 0 : ts_idx+1;
    }
    if (type->range_unchanged_cnt != UINT16_MAX)
      type->range_unchanged_cnt++;

#ifdef FILTER_DEBUG
    tofs2s( type->min_tofs, t_str1, sizeof(t_str1));
    tofs2s( type->max_tofs, t_str2, sizeof(t_str2));
    DBG("t:%u min:%s  max:%s", t, t_str1, t_str2);
#endif

    // clear each interval data for current type
    for (i=0; i<f->int_cnt;++i) {
      intv = f->intervals + i*f->type_cnt + t ;
      intv->cnt=0;
      if (type->sign)
        intv->avg.tofs=0;
      else
        intv->avg.time=0;
    }

    // calc interval step
    if ( type->sign )
      st = (type->max_tofs-type->min_tofs)/f->int_cnt;
    else
      st = (type->max_ts-type->min_ts)/f->int_cnt;

    // populate intervals data
    ts_idx = type->ridx;
    cnt = ts_cnt;
    while (cnt--) {

      if (type->sign)
        tofs = f->tofs[t*f->ts_cnt + ts_idx];
      else
        ts = f->ts[t*f->ts_cnt + ts_idx];

      // search interval for each ts and update its counters data
      for (i=0; i<f->int_cnt;++i) {
        //DBG("populate ts:%u int:%u", ts_idx, i);
        intv = f->intervals + i*f->type_cnt + t;
        if (type->sign) {
          m1pps_tofs_t s, e;
          s = type->min_tofs+i*st;
          e = s + st;
          if ( (tofs >= s) && (tofs < e) ) {
            intv->cnt++;
            intv->avg.tofs+=tofs;
            break;
          }
        } else {
          m1pps_time_t s, e;
          s = type->min_ts+i*st ;
          e = s + st;
          if ( (ts >= s) && (ts < e) ) {
            intv->cnt++;
            intv->avg.time+=ts;
            break;
          }
        }
      }

      //ts_idx = ( type->ridx >= (f->ts_cnt-1) ) ? 0 : ts_idx+1;
      ts_idx = ( ts_idx >= (f->ts_cnt-1) ) ? 0 : ts_idx+1;
    }

  }

  // search for interval with maximum count for each type
  first = 1;
  for (i=0; i<f->int_cnt;++i) {
    for (t=0;t<f->type_cnt;++t) {
      intv = f->intervals + i*f->type_cnt + t;
      type = f->types + t;
#ifdef FILTER_DEBUG
      {
        m1pps_time_t st;
        m1pps_tofs_t tofs;
        st = (type->max_tofs-type->min_tofs)/f->int_cnt;
        tofs = type->min_tofs+i*st;
        tofs2s( tofs, t_str1, sizeof(t_str1) );
        tofs2s( tofs + st, t_str2, sizeof(t_str2) );
        DBG("search int:%u t:%u cnt:%u range: %s - %s", i, t, intv->cnt, t_str1, t_str2 );
      }
#endif
      if (first) {
        type->max_int_idx=0;
        type->max_int_cnt=0;
      }
      if (type->max_int_cnt < intv->cnt ) {
        type->max_int_cnt = intv->cnt;
        type->max_int_idx = i;
      }
    }

    first=0;
  }

  // finaly calculate average ts for founded interval
  // for each type
  for (t=0;t<f->type_cnt;++t) {
    type = f->types + t;
    intv = f->intervals +  type->max_int_idx*f->type_cnt + t;
#ifdef FILTER_DEBUG
    DBG("avg t:%u max: int:%u cnt:%u", t, type->max_int_idx, type->max_int_cnt );
#endif

    if (type->sign)
      intv->avg.tofs /= intv->cnt;
    else
      intv->avg.time /= intv->cnt;
  }

}

void m1pps_ts_filter_get_avg_ts( m1pps_ts_filter_t *f, uint8_t ts_type, m1pps_time_t *ts )
{
  m1pps_ts_type_t *type = f->types + ts_type;
  m1pps_ts_int_t  *intv = f->intervals + type->max_int_idx*f->type_cnt + ts_type;
  *ts = intv->avg.time;  // fix-me
}

void m1pps_ts_filter_get_avg_tofs( m1pps_ts_filter_t *f, uint8_t ts_type, m1pps_tofs_t *tofs )
{
  m1pps_ts_type_t *type = f->types + ts_type;
  m1pps_ts_int_t  *intv = f->intervals + type->max_int_idx*f->type_cnt + ts_type;
  *tofs = intv->avg.tofs; // fix-me
}

uint16_t m1pps_ts_filter_get_range_unchanged_cnt( m1pps_ts_filter_t *f, uint8_t ts_type )
{
  m1pps_ts_type_t *type = f->types + ts_type;
  return type->range_unchanged_cnt;
}

void m1pps_ts_filter_get_all_avg_ts( m1pps_ts_filter_t *f, m1pps_time_t *ts )
{
  uint8_t t;
  m1pps_ts_type_t *type;
  m1pps_ts_int_t  *intv;

  for (t=0;t<f->type_cnt;++t) {
     m1pps_ts_filter_get_avg_ts( f, t, ts);
     type = f->types + t;
     intv = f->intervals +  type->max_int_idx*f->type_cnt + t;
     ts[t] = intv->avg.time;
  }
}

