/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_fpga_master.h"
#include "m1pps_fpga.h"

#define M1PPS_FPGA_MASTER_MAX_LOAD_SZ (65535)

#define M1PPS_FPGA_MSTCHIP_T0 (0)
#define M1PPS_FPGA_MSTCHIP_TIMER_LOAD_MS (100)
#define M1PPS_FPGA_MSTCHIP_TIMER_INFRSP_MS   (100)
#define M1PPS_FPGA_MSTCHIP_TIMER_LINITRSP_MS (100)
#define M1PPS_FPGA_MSTCHIP_TIMER_LDATARSP_MS (100)
#define M1PPS_FPGA_MSTCHIP_TIMER_LOADRSP_MS  (100)

#define M1PPS_FPGA_MASTER_T0 (0)
#define M1PPS_FPGA_MASTER_TIMER_INFRSP_MS (100)
#define M1PPS_FPGA_MASTER_TIMER_BEFORE_INFREQ_MS (1000)

int m1pps_fpga_mstchip_process_msg( m1pps_fsm_t *fsm, int ev, const void *data, size_t data_sz);

int m1pps_fpga_mstchip_init(m1pps_fpga_mstchip_t *chip, const m1pps_fsm_ticks_t *ticks,
                            uint8_t id)
{
  m1pps_fsm_ops_t ops={0};
  ops.process_msg = m1pps_fpga_mstchip_process_msg;
  m1pps_fsm_init( __fsm(chip), ticks, 16, 0, 0, &ops);
  chip->id = id;
  return 0;
}

void m1pps_fpga_mstchip_destroy( m1pps_fpga_mstchip_t *chip )
{
  m1pps_fsm_destroy( __fsm(chip) );
}

int m1pps_fpga_mstchip_send_stsreq(m1pps_fpga_mstchip_t *chip)
{
  m1pps_msg_fpga_stsreq_t *msg = __msg_buf(chip);
  msg->chip_id = chip->id;
  return __send_msg(chip, M1PPS_MSG_FPGA_STSREQ, __msg_buf(chip), sizeof(*msg));
}

int m1pps_fpga_mstchip_send_linitreq(m1pps_fpga_mstchip_t *chip)
{
  m1pps_msg_fpga_linitreq_t *msg = __msg_buf(chip);
  msg->chip_id = chip->id;
  return __send_msg(chip, M1PPS_MSG_FPGA_LINITREQ, __msg_buf(chip), sizeof(*msg));
}

int m1pps_fpga_mstchip_send_ldatareq(m1pps_fpga_mstchip_t *chip, size_t offset, void *data, size_t data_sz)
{
  m1pps_msg_fpga_ldatareq_t *msg = __msg_buf(chip);
  msg->chip_id = chip->id;
  msg->offset = offset;
  memmove(msg->data, data, data_sz);
  return __send_msg(chip, M1PPS_MSG_FPGA_LDATAREQ, __msg_buf(chip), sizeof(*msg)+data_sz);
}

int m1pps_fpga_mstchip_send_loadreq(m1pps_fpga_mstchip_t *chip, size_t offset, size_t size)
{
  m1pps_msg_fpga_loadreq_t *msg = __msg_buf(chip);
  msg->chip_id = chip->id;
  msg->offset = offset;
  msg->size = size;
  return __send_msg(chip, M1PPS_MSG_FPGA_LOADREQ, __msg_buf(chip), sizeof(*msg));
}

int m1pps_fpga_mstchip_process_rsp( m1pps_fpga_mstchip_t *chip, const void *data, size_t data_sz )
{
  const m1pps_msg_fpga_chip_sts_rsp_t *msg = data;

  if (msg->ret_code)
    return msg->ret_code;

  chip->status = msg->status;

  return 0;
}

int m1pps_fpga_mstchip_process_stsrsp( m1pps_fpga_mstchip_t *chip, const void *data, size_t data_sz )
{
  return m1pps_fpga_mstchip_process_rsp(chip, data, data_sz);
}

int m1pps_fpga_mstchip_process_linitrsp( m1pps_fpga_mstchip_t *chip, const void *data, size_t data_sz )
{
  return m1pps_fpga_mstchip_process_rsp(chip, data, data_sz);
}

int m1pps_fpga_mstchip_process_ldatarsp(m1pps_fpga_mstchip_t *chip, const void *data, size_t data_sz)
{
  int err;
  err = m1pps_fpga_mstchip_process_rsp(chip, data, data_sz);
  if (err)
    return err;

  chip->offset += chip->size;
  chip->loaded_sz += chip->size;
  return 0;
}

int m1pps_fpga_mstchip_process_loadrsp(m1pps_fpga_mstchip_t *chip, const void *data, size_t data_sz, uint8_t *complete)
{
  int err;

  err = m1pps_fpga_mstchip_process_rsp(chip, data, data_sz);
  if (err)
    return err;

  *complete = (chip->status & M1PPS_FPGA_PIN_CONFDONE) ? 1 : 0;

  return 0;
}

m1pps_fpga_mstchip_st_t
m1pps_fpga_mstchip_process_err( m1pps_fpga_mstchip_t *chip, int err)
{
   //m1pps_slave_reset_sync(slave);
   return M1PPS_FPGA_MSTCHIP_ST_STARTED;
}

m1pps_fpga_mstchip_st_t
m1pps_fpga_mstchip_process_timeout(m1pps_fpga_mstchip_t *chip, const char *timeout_msg)
{
   //slave->stat.err_timeout++;
   ERR("%s", timeout_msg);
   return m1pps_fpga_mstchip_process_err(chip, -ETIME);
}

int m1pps_fpga_mstchip_register(m1pps_fpga_mstchip_t *chip)
{
  return 0;
}

int m1pps_fpga_mstchip_process_msg( m1pps_fsm_t *fsm, int ev, const void *data, size_t data_sz )
{
  int err;
  m1pps_fpga_mstchip_t *chip = container_of(fsm, m1pps_fpga_mstchip_t, fsm);
  m1pps_fpga_mstchip_st_t ns, s;
  s = ns = __state(chip);

  switch (s) {

    case M1PPS_FPGA_MSTCHIP_ST_INITIAL:

      if (ev != M1PPS_FPGA_MSTCHIP_EV_START)
         break;
      ns = M1PPS_FPGA_MSTCHIP_ST_STARTED;

    break;

    case M1PPS_FPGA_MSTCHIP_ST_STARTED:

      if (ev == M1PPS_FPGA_MSTCHIP_EV_STOP) {
        ns = M1PPS_FPGA_MSTCHIP_ST_INITIAL;
        break;
      }

      err = m1pps_fpga_mstchip_send_linitreq(chip);
      __start_timer(chip, M1PPS_FPGA_MSTCHIP_T0,
                    M1PPS_FPGA_MSTCHIP_TIMER_LINITRSP_MS);

      ns = M1PPS_FPGA_MSTCHIP_ST_INITLOADING;

    break;
    case M1PPS_FPGA_MSTCHIP_ST_INITLOADING:

      if (ev == M1PPS_FPGA_MSTCHIP_EV_STOP) {
        ns = M1PPS_FPGA_MSTCHIP_ST_INITIAL;
        break;
      }

      if (ev == M1PPS_FPGA_MSTCHIP_EV_TIMER) {
        if (!__timer(chip, M1PPS_FPGA_MSTCHIP_T0 )) {
          ns = m1pps_fpga_mstchip_process_timeout(chip, "waiting for LINITRSP");
          break;
        }
      }

      if (ev != M1PPS_FPGA_MSTCHIP_EV_LINITRSP)
        break;

      __stop_timer(chip, M1PPS_FPGA_MSTCHIP_T0);

      err = m1pps_fpga_mstchip_process_linitrsp(chip, data, data_sz);
      if (err) {
        ns = m1pps_fpga_mstchip_process_err(chip, err);
        break;
      }

      err = m1pps_fpga_mstchip_send_ldatareq(chip, chip->offset, 0, 0);
      __start_timer(chip, M1PPS_FPGA_MSTCHIP_T0, M1PPS_FPGA_MSTCHIP_TIMER_LDATARSP_MS);
      ns = M1PPS_FPGA_MSTCHIP_ST_TRANSFERING;

    break;
    case M1PPS_FPGA_MSTCHIP_ST_TRANSFERING:

      if (ev == M1PPS_FPGA_MSTCHIP_EV_STOP) {
        ns = M1PPS_FPGA_MSTCHIP_ST_INITIAL;
        break;
      }

      if (ev == M1PPS_FPGA_MSTCHIP_EV_TIMER) {
        if (!__timer(chip, M1PPS_FPGA_MSTCHIP_T0 )) {
           ns = m1pps_fpga_mstchip_process_timeout( chip, "waiting for LDATARSP");
           break;
        }
      }

      if (ev != M1PPS_FPGA_MSTCHIP_EV_LDATARSP)
        break;

      __stop_timer(chip, M1PPS_FPGA_MSTCHIP_T0);

      if ((err = m1pps_fpga_mstchip_process_ldatarsp(chip, data, data_sz))) {
        //ns = m1pps_slave_process_err(chip, err);
        break;
      }

      if (chip->offset < chip->master->max_load_sz && chip->loaded_sz < chip->full_sz) {
        err = m1pps_fpga_mstchip_send_ldatareq(chip, chip->offset, 0, 0 );
        __start_timer(chip, M1PPS_FPGA_MSTCHIP_T0,
                      M1PPS_FPGA_MSTCHIP_TIMER_LDATARSP_MS);
        break;
      }

      err = m1pps_fpga_mstchip_send_loadreq(chip, 0, chip->loaded_sz);
      __start_timer(chip, M1PPS_FPGA_MSTCHIP_T0, M1PPS_FPGA_MSTCHIP_TIMER_LOADRSP_MS);
      ns =  M1PPS_FPGA_MSTCHIP_ST_LOADING;

    break;
    case M1PPS_FPGA_MSTCHIP_ST_LOADING: {

      uint8_t complete;

      if (ev == M1PPS_FPGA_MSTCHIP_EV_STOP) {
        ns = M1PPS_FPGA_MSTCHIP_ST_INITIAL;
        break;
      }

      if (ev == M1PPS_FPGA_MSTCHIP_EV_TIMER) {
        if (!__timer(chip, M1PPS_FPGA_MSTCHIP_T0 )) {
           ns = m1pps_fpga_mstchip_process_timeout( chip, "waiting for LOADRSP");
           break;
        }
      }

      if (ev != M1PPS_FPGA_MSTCHIP_EV_LOADRSP)
        break;

      __stop_timer(chip, M1PPS_FPGA_MSTCHIP_T0);

      if ((err = m1pps_fpga_mstchip_process_loadrsp(chip, data, data_sz, &complete))) {
        //ns = m1pps_slave_process_err(chip, err);
        break;
      }

      if (!complete) {
        if (chip->loaded_sz < chip->full_sz) {
          err = m1pps_fpga_mstchip_send_ldatareq(chip, chip->offset, 0, 0);
          __start_timer(chip, M1PPS_FPGA_MSTCHIP_T0,
                        M1PPS_FPGA_MSTCHIP_TIMER_LDATARSP_MS);
          ns = M1PPS_FPGA_MSTCHIP_ST_TRANSFERING;
          break;
        }
      }

      err = m1pps_fpga_mstchip_register(chip);
      ns =  M1PPS_FPGA_MSTCHIP_ST_WORKING;

    } break;
    case M1PPS_FPGA_MSTCHIP_ST_WORKING:

    break;

  }

  return ns;
}

/* container */



int m1pps_fpga_master_process_msg( m1pps_fsm_t *fsm, int ev,
                             const void *data, size_t data_sz );

int m1pps_fpga_master_init(m1pps_fpga_master_t *master, const m1pps_fsm_ticks_t *ticks,
                           uint8_t max_cnt)
{
  uint8_t i;
  m1pps_fsm_ops_t ops={0};
  ops.process_msg = m1pps_fpga_master_process_msg;

  // fix-me
  m1pps_fsm_init(__fsm(master), ticks, M1PPS_FPGA_MASTER_MAX_LOAD_SZ+sizeof(m1pps_msg_fpga_ldatareq_t), 0, 0, &ops);

  master->max_chip_cnt = max_cnt;
  master->chip = sys_zalloc(sizeof(m1pps_fpga_mstchip_t)*master->max_chip_cnt);

  for (i=0; i<master->chip_cnt; ++i)
    m1pps_fpga_mstchip_init(master->chip + i, ticks, i);

  return 0;
}


void m1pps_fpga_master_destroy(m1pps_fpga_master_t *master )
{
  uint8_t i;
  for (i=0; i<master->chip_cnt; ++i)
    m1pps_fpga_mstchip_destroy(master->chip + i);
  m1pps_fsm_destroy(__fsm(master));
}
/*
void m1pps_fpga_master_set_cbs(m1pps_fpga_master_t *master, const m1pps_fpga_mastercbs_t *cbs )
{
  master->cbs = *cbs;
}

void m1pps_fpga_master_get_info(const m1pps_fpga_master_t *master, m1pps_info_t *info )
{
  (*info) = master->info;
}

void m1pps_fpga_master_get_stat(const m1pps_fpga_master_t *master, m1pps_stat_t *stat )
{
  (*stat) = master->stat;
}
*/

uint8_t m1pps_fpga_master_get_cnt( const m1pps_fpga_master_t *master)
{
  return master->chip_cnt;
}

m1pps_fpga_mstchip_t *m1pps_fpga_master_get_chip( const m1pps_fpga_master_t *master, uint8_t chip_idx)
{
  return master->chip + chip_idx;
}

m1pps_fpga_master_st_t m1pps_fpga_master_process_err( m1pps_fpga_master_t *master, int err )
{
   //m1pps_slave_reset_sync(master);
   return M1PPS_FPGA_MASTER_ST_STARTED;
}

m1pps_fpga_master_st_t m1pps_fpga_master_process_timeout( m1pps_fpga_master_t *master, const char *timeout_msg )
{
  // master->stat.err_timeout++;
   ERR("%s", timeout_msg);
   return m1pps_fpga_master_process_err(master, -ETIME);
}

int m1pps_fpga_master_process_allchip_ev(m1pps_fpga_master_t *master, int ev, const void *data, size_t data_sz)
{
  uint8_t i;
  for (i=0; i<master->chip_cnt; ++i)
    m1pps_fpga_mstchip_process_msg(__fsm(master->chip + i), ev, data, data_sz);
  return 0;
}

int m1pps_fpga_master_process_chip_ev(m1pps_fpga_master_t *master, int ev, const void *data, size_t data_sz )
{
  const m1pps_msg_fpga_chiprsp_t *msg = data;

  if ( data_sz != sizeof(msg->chip_id) )
    return -EINVAL;

  if ( msg->chip_id >= master->chip_cnt)
    return -EINVAL;

  return  m1pps_fpga_mstchip_process_msg(__fsm(master->chip + msg->chip_id), ev, data, data_sz);
}

int m1pps_fpga_master_send_infreq(m1pps_fpga_master_t *master)
{
  m1pps_msg_fpga_infreq_t *msg = __msg_buf(master);
  return __send_msg(master, M1PPS_MSG_FPGA_INFREQ, __msg_buf(master), sizeof(*msg));
}

int m1pps_fpga_master_process_infrsp( m1pps_fpga_master_t *master, const void *data, size_t data_sz )
{
  m1pps_msg_fpga_infrsp_t *msg = __msg_buf(master);
  uint8_t i;

  if (msg->chip_cnt>=master->max_chip_cnt)
    return -EINVAL;

  master->chip_cnt = msg->chip_cnt;
  master->max_load_sz = msg->max_load_sz;
  for (i=0; i<master->chip_cnt; ++i) {
    m1pps_fpga_mstchip_desc_t *dst;
    m1pps_msg_fpgachip_desc_t *src = msg->chip + i;

    if (src->chip_id >= master->max_chip_cnt)
      continue;
    dst = &master->chip[src->chip_id].desc;
    strncpy(dst->name, src->name, sizeof(dst->name));
    master->chip[src->chip_id].id = src->chip_id;
    m1pps_fpga_master_process_chip_ev(master, M1PPS_FPGA_MSTCHIP_EV_START, 0, 0);
  }

  return 0;
}

int m1pps_fpga_master_process_msg( m1pps_fsm_t *fsm, int ev, const void *data, size_t data_sz )
{
  int err;
  m1pps_fpga_master_t *master = container_of(fsm, m1pps_fpga_master_t, fsm);
  m1pps_fpga_master_st_t ns, s;
  s = ns = __state(master);

  switch (s) {
    case M1PPS_FPGA_MASTER_ST_INITIAL:
      if (ev != M1PPS_FPGA_MASTER_EV_START)
         break;

      if ((err=m1pps_fpga_master_process_allchip_ev(master, ev, data, data_sz))) {
         break;
      }
      ns = M1PPS_FPGA_MASTER_ST_STARTED;
    break;

    case M1PPS_FPGA_MASTER_ST_STARTED:

      if (ev == M1PPS_FPGA_MASTER_EV_STOP) {
        err=m1pps_fpga_master_process_allchip_ev(master, ev, data, data_sz);
        ns = M1PPS_FPGA_MASTER_ST_INITIAL;
        break;
      }

      if ((err=m1pps_fpga_master_send_infreq(master))) {
        ns = m1pps_fpga_master_process_err(master,err);
        break;
      }

      __start_timer(master, M1PPS_FPGA_MASTER_T0,
                    M1PPS_FPGA_MASTER_TIMER_INFRSP_MS);

      ns=M1PPS_FPGA_MASTER_ST_WAIT_INFRSP;
    break;

    case M1PPS_FPGA_MASTER_ST_WAIT_INFRSP:

      if (ev == M1PPS_FPGA_MASTER_EV_STOP) {
        if ((err=m1pps_fpga_master_process_allchip_ev(master, ev, data, data_sz))) {
          ns = m1pps_fpga_master_process_err(master,err);
          break;
        }
        ns = M1PPS_FPGA_MASTER_ST_INITIAL;
        break;
      }

      if (ev == M1PPS_FPGA_MASTER_EV_TIMER) {
        if (!__timer(master, M1PPS_FPGA_MASTER_T0 )) {
           __start_timer(master, M1PPS_FPGA_MASTER_T0,
                         M1PPS_FPGA_MASTER_TIMER_BEFORE_INFREQ_MS);
           ns = M1PPS_FPGA_MASTER_ST_WAIT_BEFORE_INFREQ;
           break;
        }
      }

      if (ev != M1PPS_FPGA_MASTER_EV_INFRSP)
        break;

      m1pps_fpga_master_process_infrsp(master, data, data_sz);
      ns = M1PPS_FPGA_MASTER_ST_WORKING;

    break;

    case M1PPS_FPGA_MASTER_ST_WAIT_BEFORE_INFREQ:

      if (ev == M1PPS_FPGA_MASTER_EV_STOP) {
        err=m1pps_fpga_master_process_allchip_ev(master, ev, data, data_sz);
        ns = M1PPS_FPGA_MASTER_ST_INITIAL;
        break;
      }

      if (ev != M1PPS_FPGA_MASTER_EV_TIMER)
        break;

      if (__timer(master, M1PPS_FPGA_MASTER_T0))
        break;

      err = m1pps_fpga_master_send_infreq(master);
      __start_timer(master, M1PPS_FPGA_MASTER_T0,
                    M1PPS_FPGA_MASTER_TIMER_INFRSP_MS);
      ns = M1PPS_FPGA_MASTER_ST_WAIT_INFRSP;

    break;

    case M1PPS_FPGA_MASTER_ST_WORKING:

      if (ev == M1PPS_FPGA_MASTER_EV_STOP) {
        err=m1pps_fpga_master_process_allchip_ev(master, ev, data, data_sz);
        ns = M1PPS_FPGA_MASTER_ST_INITIAL;
        break;
      }

      if ( ! (ev == M1PPS_FPGA_MASTER_EV_STSRSP ||
              ev == M1PPS_FPGA_MASTER_EV_LINITRSP ||
              ev == M1PPS_FPGA_MASTER_EV_LOADRSP ||
              ev == M1PPS_FPGA_MASTER_EV_LOADRSP ) )
        break;

      m1pps_fpga_master_process_chip_ev(master, ev, data, data_sz);

    break;
  }

  return ns;
}
