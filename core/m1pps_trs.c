/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_trs.h"

int m1pps_trs_init( m1pps_trs_t *t, const m1pps_trs_ops_t *ops, m1pps_clk_t *clk )
{
  t->ops = *ops;
  t->clk = clk;
  return 0;
}

void m1pps_trs_destroy( m1pps_trs_t *t)
{
}

int m1pps_trs_open( m1pps_trs_t *t,  void *opendata )
{
  memset( &t->stat, 0, sizeof(t->stat) );
  return t->ops.open(t, opendata);
}

int m1pps_trs_close( m1pps_trs_t *t)
{
  return t->ops.close(t);
}

int m1pps_trs_send_pkt( m1pps_trs_t *t, const void *data, size_t data_sz )
{
  return t->ops.send_pkt(t, data, data_sz);
}

void m1pps_trs_set_cbs( m1pps_trs_t *t, const m1pps_trs_cbs_t *cbs )
{
  t->cbs = *cbs;
}

void m1pps_trs_set_ctx( m1pps_trs_t *t, void *ctx )
{
  t->ctx = ctx;
}

void *m1pps_trs_get_ctx( const m1pps_trs_t *t )
{
  return t->ctx;
}

m1pps_time_t m1pps_trs_get_time( const m1pps_trs_t *t )
{
  if (!t->clk)
    return 0;
  return m1pps_clk_get_time( t->clk );
}

void m1pps_trs_get_stat( const m1pps_trs_t *t,  m1pps_trs_stat_t *stat )
{
  *stat = t->stat;
}

int m1pps_trs_call_pkt_sended( struct m1pps_trs *t, m1pps_time_t ts )
{
  if ( ! t->cbs.pkt_sended )
     return -EINVAL;

  return t->cbs.pkt_sended( t, ts );

}

int m1pps_trs_call_pkt_recieved( struct m1pps_trs *t, const void *data, size_t data_sz, m1pps_time_t ts )
{
  if ( ! t->cbs.pkt_recieved )
     return -EINVAL;

  return t->cbs.pkt_recieved( t, data, data_sz, ts );
}


