/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#define M1PPS_FPGA_PIN_NCONFIG     (0) // outputs
#define M1PPS_FPGA_PIN_DATA        (1)
#define M1PPS_FPGA_PIN_DCLK        (2)

#define M1PPS_FPGA_PIN_NSTATUS     (0) // inputs
#define M1PPS_FPGA_PIN_CONFDONE    (1)

