/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_sys.h"
//#include "m1pps_msg.h"

/*
 * this layer provide pack/unpack fsm data units to line format
*/

struct m1pps_pkt;

typedef struct m1pps_pkt_up_args {
  size_t  body_ofs;
  size_t  body_sz;
  uint8_t ver;
  uint8_t id;
  m1pps_time_t rx_ts;
} m1pps_pkt_up_args_t;

typedef size_t (*m1pps_pack_msg_cb_t)(struct m1pps_pkt *pkt, size_t offset, int msg_id,
                                      const void *msg_data, size_t msg_data_sz, uint8_t *pkt_id);
typedef int (*m1pps_unpack_msg_cb_t)(const struct m1pps_pkt *pkt, const m1pps_pkt_up_args_t *up_args,
                                     int *msg_id, void *msg_data, size_t *msg_data_sz  );


/* pkt subsystem */

typedef struct m1pps_pkt_subsys {
  char *name;
  uint8_t id;
  m1pps_pack_msg_cb_t   pack_msg;
  m1pps_unpack_msg_cb_t unpack_msg;
  size_t                (*get_max_pkt_sz)(void);
} m1pps_pkt_subsys_t;

/* pkt subsystems collection */

typedef struct m1pps_pkt_substms {
  uint8_t max_subsys_cnt;
  uint8_t subsys_cnt;
  const m1pps_pkt_subsys_t **subsys;
  void  **ctx;
} m1pps_pkt_substms_t;

int  m1pps_pkt_substms_init(m1pps_pkt_substms_t *substms, uint8_t max_subsys_cnt );
void m1pps_pkt_substms_destroy(m1pps_pkt_substms_t *substms);

int m1pps_pkt_substms_register_subsys(m1pps_pkt_substms_t *substms, const m1pps_pkt_subsys_t *subsys);
void *m1pps_pkt_substms_get_subsys_ctx(const m1pps_pkt_substms_t *substms, uint8_t subsys_id);
void m1pps_pkt_substms_set_subsys_ctx(m1pps_pkt_substms_t *substms, uint8_t subsys_id, void *ctx);
size_t m1pps_pkt_substms_get_max_pkt_sz(const m1pps_pkt_substms_t *substms);

/* pkt packer/unpacker */

typedef struct m1pps_pkt {
  size_t   buf_sz;
  void     *buf;
  m1pps_pkt_substms_t *substms;
} m1pps_pkt_t;

int m1pps_pkt_init(m1pps_pkt_t *p, m1pps_pkt_substms_t *substms, size_t buf_sz);
void m1pps_pkt_destroy(m1pps_pkt_t *pkt);

void *m1pps_pkt_get_buf(m1pps_pkt_t *pkt);
size_t m1pps_pkt_get_buf_sz(const m1pps_pkt_t *pkt);

size_t m1pss_pkt_get_max_pkt_sz(void);

size_t m1pps_pkt_pack_msg( m1pps_pkt_t *pkt, int subsys_id, int msg_id, const void *msg_data, size_t msg_data_sz );
int m1pps_pkt_unpack_msg( const m1pps_pkt_t *pkt, size_t pkt_data_sz, m1pps_time_t pkt_rx_ts,
                          int *subsys_id, int *msg_id, void *msg_data, size_t *msg_data_sz  );

