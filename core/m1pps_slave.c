/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_slave.h"

#define M1PPS_SLAVE_T0 (0)
#define M1PPS_SLAVE_T1 (1)
#define M1PPS_SLAVE_TIMER_WAIT_FLUP         (100)
#define M1PPS_SLAVE_TIMER_WAIT_DLRESP       (100)
#define M1PPS_SLAVE_TIMER_WAIT_NEXT_SYNC    (400)
#define M1PPS_SLAVE_TIMER_WAIT_MODE_CHANGE  (200)
#define M1PPS_SLAVE_TIMER_ADJUST            (60000) // adjact not frequently than

#define M1PPS_SLAVE_MIN_CIRCLES_FOR_SYNC (3)
#define M1PPS_SLAVE_MIN_OFFSET_NS        (60000000000) // 1 min

#define M1PPS_SLAVE_FILTER_VALUE_CNT     (128)
#define M1PPS_SLAVE_FILTER_INTVL_CNT     (16)

#define M1PPS_SLAVE_FILTER_VALUE_MIN_CNT (64)
#define M1PPS_SLAVE_PERIOD_FILTER_MIN_CNT  (128)
#define M1PPS_SLAVE_MODE_DEFAULT (M1PPS_SLAVE_MODE_NORMAL)

//#define M1PPS_SLAVE_MODE_DEFAULT (M1PPS_SLAVE_MODE_DIRECTPULSE)

int m1pps_slave_process_msg( m1pps_fsm_t *fsm, int ev,
                             const void *data, size_t data_sz );
int m1pps_slave_state_changed( struct m1pps_fsm *fsm, int msg_id, int old_st, int new_st );

int m1pps_slave_init( m1pps_slave_t *slave, const m1pps_fsm_ticks_t *ticks, m1pps_clk_t *clk )
{
  int r=0;
  m1pps_fsm_ops_t ops={0};
  ops.process_msg = m1pps_slave_process_msg;
  ops.state_changed = m1pps_slave_state_changed;
  m1pps_fsm_init( __fsm(slave), ticks, 512, 2, 2, &ops);
  memset( &slave->cmn, 0, sizeof(slave->cmn));
  memset( &slave->info, 0, sizeof(slave->info));
  memset( &slave->stat, 0, sizeof(slave->stat));
  slave->clk = clk;
  // 2 type of timestamps t2-t1 & t4-t3
  // 256 value recoding
  // 16 intervals
  r = m1pps_ts_filter_init(&slave->filter, 2, M1PPS_SLAVE_FILTER_VALUE_CNT, M1PPS_SLAVE_FILTER_INTVL_CNT);
  if (r) {
    ERR("ts_filter init error %i", r);
    return r;
  }

  m1pps_ts_filter_set_type_sign(&slave->filter, 0, 1 );
  m1pps_ts_filter_set_type_sign(&slave->filter, 1, 1 );
  m1pps_ts_filter_clear( &slave->filter );

  r = m1pps_ts_filter_init(&slave->period_filter, 1, M1PPS_SLAVE_FILTER_VALUE_CNT, M1PPS_SLAVE_FILTER_INTVL_CNT);
  if (r) {
    ERR("ts_filter init error %i", r);
    return r;
  }

  slave->mode = M1PPS_SLAVE_MODE_UNKNOWN;
  slave->adj_mode = M1PPS_SYNC_SLAVE_ADJMODE_PERIOD;
  return 0;
}

void m1pps_slave_destroy( m1pps_slave_t *slave )
{
   m1pps_fsm_destroy( __fsm(slave) );
   m1pps_ts_filter_destroy( &slave->filter );
}

void m1pps_slave_set_cbs( m1pps_slave_t *slave, const m1pps_slave_cbs_t *cbs )
{
  slave->cbs = *cbs;
}

void m1pps_slave_change_mode( m1pps_slave_t *slave, uint8_t mode, uint8_t start )
{
  if ( slave->mode != mode )  {
    if (start) {
      slave->new_mode = mode;
      m1pps_clk_set_output(slave->clk, 0);
    }

    if ( slave->cbs.set_mode )
       slave->cbs.set_mode( __fsm(slave), mode, start );

    if (!start) {
      slave->mode = mode;
    }
  }
}

int m1pps_slave_process_start( m1pps_slave_t *slave )
{
//  m1pps_slave_change_mode( slave, M1PPS_SLAVE_MODE_DEFAULT, 1);
  return 0;
}

int m1pps_slave_process_sync( m1pps_slave_t *slave, const void *data, size_t data_sz )
{
  const m1pps_msg_sync_sync_t *msg = data;
  m1pps_time_t ct, dt;
  char t_str[T_SZ];

  if ( data_sz != sizeof(*msg) )
    return -EINVAL;

  slave->t[2] = msg->rx_ts;

  /* if time difference too big -> setup clock */
  ct = m1pps_clk_get_time( slave->clk );
  dt = (msg->t_low > ct) ? msg->t_low - ct : ct - msg->t_low;

  if ( dt > M1PPS_SLAVE_MIN_OFFSET_NS ) {
    m1pps_clk_set_time( slave->clk, msg->t_low );
    time2s( msg->t_low, t_str, sizeof(t_str));
    INF("Time offset too big!!! Clock assigned to %s", t_str);
  }

  slave->cmn.stat.cyrcles++;

  return 0;
}

int m1pps_slave_process_flup( m1pps_slave_t *slave, const void *data, size_t data_sz )
{
  const m1pps_msg_sync_flup_t *msg = data;

  if ( data_sz != sizeof(*msg) )
    return -EINVAL;

  slave->t[1] = msg->t1;

  return 0;
}

int m1pps_slave_send_dlreq( m1pps_slave_t *slave )
{
  m1pps_msg_sync_dlreq_t *msg = __msg_buf( slave );
  msg->t2 = slave->t[2];
  //msg->t3 = slave->t3;
  return __send_msg( slave , M1PPS_SLAVE_MSG_DLREQ, __msg_buf(slave), sizeof(*msg));
}

int m1pps_slave_process_pulse( m1pps_slave_t *slave, const void *data, size_t data_sz )
{
  const m1pps_msg_sync_pulse_t *msg = data;

  if ( data_sz != sizeof(m1pps_msg_sync_pulse_t) )
    return -EINVAL;

  if ( ! slave->cbs.send_pulse )
    return 0;

  return  slave->cbs.send_pulse( __fsm(slave), msg->value );
}

uint8_t m1pps_slave_get_mode( const m1pps_slave_t *slave )
{
  return slave->mode;
}

int m1pps_slave_process_dlreq_sended( m1pps_slave_t *slave, const void *data, size_t data_sz )
{
  const m1pps_msg_sync_sended_t *msg = data;

  if ( data_sz != sizeof(*msg) )
    return -EINVAL;

  slave->t[3] = msg->tx_ts;

  return 0;
}

void m1pps_slave_adjust_time( m1pps_slave_t *slave, m1pps_tofs_t offset)
{
  m1pps_clk_adj_time(slave->clk, offset);
  slave->stat.time_adjust_cnt++;
}

void m1pps_slave_adjust_period( m1pps_slave_t *slave, m1pps_tofs_t period_k)
{
  m1pps_clk_adj_period(slave->clk, period_k);
  slave->stat.period_adjust_cnt++;
}

void m1pps_slave_process_adjust_time( m1pps_slave_t *slave)
{
   if (slave->cmn.info.proto_sync) {
       if (llabs(slave->cmn.info.offset) > 50000 ) {

         m1pps_tofs_t offset = slave->cmn.info.offset;

         if (llabs(offset) < 100000 ) {
           if (llabs(offset) > 5000 )
             offset = (offset < 0) ? 5000 : -5000;
         }

         m1pps_slave_adjust_time(slave, -offset);
         m1pps_ts_filter_clear_values(&slave->filter);
       }
   }
   __start_timer( slave, M1PPS_SLAVE_T1, M1PPS_SLAVE_TIMER_ADJUST);
}

void m1pps_slave_process_adjust_period( m1pps_slave_t *slave)
{
   if (slave->cmn.info.proto_sync) {
     if (!slave->period_adjusted) {
       m1pps_slave_adjust_period(slave, slave->info.period_k_filtered);
       slave->info.period_k_adjusted = slave->info.period_k_filtered;
       slave->period_adjusted=1;
       m1pps_ts_filter_clear(&slave->filter);
     }
   }
}

m1pps_time_t  __correct_time( m1pps_time_t time_ns, m1pps_time_t accurancy_ns )
{
  return time_ns / accurancy_ns * accurancy_ns;
}

void m1pps_slave_reset_sync( m1pps_slave_t *slave )
{
   if (slave->cmn.info.proto_sync)
     slave->cmn.stat.err_sync_lost++;
   slave->cyrcles = 0;
   slave->cmn.info.proto_sync = 0;
}

int m1pps_slave_preprocess_dlresp( m1pps_slave_t *slave, const void *data, size_t data_sz )
{
  const m1pps_msg_sync_dlresp_t *msg = data;
  char t_str[T_SZ];
  m1pps_sync_slave_preproc_t *pp = &slave->preproc;
  m1pps_tofs_t prev_offset;
  m1pps_time_t accurancy_ns, prev_t4;
  uint8_t i, sync;

  if ( data_sz != sizeof(*msg) )
    return -EINVAL;

  prev_t4 = slave->t[4];
  slave->t[4] = msg->t4;
  prev_offset = slave->cmn.info.offset;

  accurancy_ns = m1pps_clk_get_accurancy(slave->clk);
  for (i=1;i<=4;++i)
    slave->t[i] = __correct_time(slave->t[i], accurancy_ns);

  if ( slave->t[2] > slave->t[1] )
    pp->frwd = slave->t[2] - slave->t[1];
  else
    pp->frwd = -(slave->t[1] - slave->t[2]);

  if ( slave->t[4] > slave->t[3] )
    pp->bkwd = slave->t[4] - slave->t[3];
  else
    pp->bkwd = -(slave->t[3] - slave->t[4]);

  tofs2s(pp->frwd, t_str, sizeof(t_str));
  DBG("frwd: %s", t_str);
  tofs2s(pp->bkwd, t_str, sizeof(t_str));
  DBG("bkwd: %s", t_str);

  DBG("cycle: %u", slave->cyrcles);

  // average delay must not be smaler than zero -> this is hw timestamp mistake
  // so skip this cyrle with error delay
  if ((pp->frwd + pp->bkwd)/2 < 0) {
     slave->cmn.stat.err_delay++;
     m1pps_slave_reset_sync(slave);
     return -EINVAL;
  }

  // ok now calc offset on average values
  slave->cmn.info.offset = (pp->frwd - pp->bkwd)/2;

  tofs2s(slave->cmn.info.offset, t_str, sizeof(t_str));
  //INF("offset: %s", t_str);

  if ((pp->frwd + pp->bkwd)/2 > 0)
    slave->cmn.info.delay = (pp->frwd + pp->bkwd)/2;
  else
    slave->cmn.info.delay = 0;

  if (slave->cyrcles < UINT16_MAX)
    slave->cyrcles++;

  if (slave->cyrcles >= 2) {
    pp->offset_diff = slave->cmn.info.offset-prev_offset;
    if (slave->t[4] <= prev_t4) {
       slave->cmn.stat.err_delay++;
       return -EINVAL;
    }
    pp->t_diff = slave->t[4] - prev_t4;
  }

  sync = ( slave->cyrcles >= M1PPS_SLAVE_MIN_CIRCLES_FOR_SYNC ) ? 1 : 0;
  if (!slave->cmn.info.proto_sync && sync)
    __start_timer(slave, M1PPS_SLAVE_T1, M1PPS_SLAVE_TIMER_ADJUST);
  slave->cmn.info.proto_sync = sync;
  slave->cmn.stat.good_cyrcles++;

  return 0;
}

int m1pps_slave_process_period_dlresp( m1pps_slave_t *slave, const void *data, size_t data_sz )
{
  int r=0;
  m1pps_sync_slave_preproc_t *pp = &slave->preproc;
  m1pps_tofs_t period_k;
  uint16_t range_cnt;
  // char t_str[T_SZ];

  r =  m1pps_slave_preprocess_dlresp(slave, data, data_sz);
  if (r)
    return r;

  if (!slave->cmn.info.proto_sync)
    return -EINVAL;

  //tofs2s(pp->offset_diff, t_str, sizeof(t_str));
  //INF("off_diff: %s", t_str);

  m1pps_ts_filter_add_tofs(&slave->filter, 0, pp->offset_diff);
  m1pps_ts_filter_add_tofs(&slave->filter, 1, pp->t_diff);

  if (m1pps_ts_filter_ts_count(&slave->filter, 0) < M1PPS_SLAVE_FILTER_VALUE_MIN_CNT)
    return -EINVAL;

  m1pps_ts_filter_calc(&slave->filter);

  range_cnt = m1pps_ts_filter_get_range_unchanged_cnt(&slave->filter, 0);
  //INF("range1 %u", range_cnt)
  if (range_cnt < 128)
    return -EINVAL;

  range_cnt = m1pps_ts_filter_get_range_unchanged_cnt(&slave->filter, 1);
  //INF("range2 %u", range_cnt)
  if (range_cnt < 64)
    return -EINVAL;

  m1pps_ts_filter_get_avg_tofs(&slave->filter, 0, &slave->info.offset_diff_filtered);
  m1pps_ts_filter_get_avg_tofs(&slave->filter, 1, &slave->info.t_diff_filtered);

  if (slave->info.offset_diff_filtered == 0) {
    return 0;
  }

  period_k = (uint64_t)slave->info.t_diff_filtered / (uint64_t)llabs(slave->info.offset_diff_filtered);

  if (slave->info.offset_diff_filtered < 0)
       period_k = -period_k;

  slave->info.period_k_filtered = period_k;

  m1pps_slave_adjust_period(slave, slave->info.period_k_filtered);

  slave->info.period_k_adjusted = slave->info.period_k_filtered;
  m1pps_ts_filter_clear(&slave->filter);
  //m1pps_clk_set_output(slave->clk, 1);

  slave->adj_mode=M1PPS_SYNC_SLAVE_ADJMODE_TIME;

  return 0;
}

int m1pps_slave_process_time_dlresp(m1pps_slave_t *slave, const void *data, size_t data_sz)
{
  int r=0;
  m1pps_sync_slave_preproc_t *pp = &slave->preproc;
  m1pps_tofs_t frwd, bkwd;
  //uint16_t range_cnt;

  slave->allow_time_adjust = 0;

  r =  m1pps_slave_preprocess_dlresp(slave, data, data_sz);
  if (r)
    return r;

  if (!slave->cmn.info.proto_sync)
    return -EINVAL;

  m1pps_ts_filter_add_tofs(&slave->filter, 0, pp->frwd);
  m1pps_ts_filter_add_tofs(&slave->filter, 1, pp->bkwd);

  if (m1pps_ts_filter_ts_count(&slave->filter, 0) < M1PPS_SLAVE_FILTER_VALUE_MIN_CNT)
    return -EINVAL;

  m1pps_ts_filter_calc(&slave->filter);
/*
  range_cnt = m1pps_ts_filter_get_range_unchanged_cnt(&slave->filter, 0);
  //INF("range1 %u", range_cnt)
  if (range_cnt < 128)
    return -EINVAL;

  range_cnt = m1pps_ts_filter_get_range_unchanged_cnt(&slave->filter, 1);
  //INF("range2 %u", range_cnt);
  if (range_cnt < 128)
    return -EINVAL;
*/
  m1pps_ts_filter_get_avg_tofs(&slave->filter, 0, &frwd);
  m1pps_ts_filter_get_avg_tofs(&slave->filter, 1, &bkwd);

  slave->info.offset_filtered = (frwd - bkwd)/2;
  slave->info.delay_filtered = (frwd + bkwd)/2;

  if (!slave->allow_time_adjust) {
    m1pps_slave_process_adjust_time(slave);
    m1pps_clk_set_output(slave->clk, 1);
    slave->allow_time_adjust = 1;
  }

  return 0;
}

int m1pps_slave_process_dlresp(m1pps_slave_t *slave, const void *data, size_t data_sz)
{
  int r = 0;
  if (slave->adj_mode == M1PPS_SYNC_SLAVE_ADJMODE_PERIOD) {
    r = m1pps_slave_process_period_dlresp(slave, data, data_sz);
  } else {
    r = m1pps_slave_process_time_dlresp(slave, data, data_sz);
  }
  return r;
}

void m1pps_slave_get_cmn_info( const m1pps_slave_t *slave, m1pps_sync_cmn_info_t *info )
{
  (*info) = slave->cmn.info;
}

void m1pps_slave_get_cmn_stat( const m1pps_slave_t *slave, m1pps_sync_cmn_stat_t *stat )
{
  (*stat) = slave->cmn.stat;
}

void m1pps_slave_get_info(const m1pps_slave_t *slave, m1pps_sync_slave_info_t *info)
{
  (*info) = slave->info;
}

void m1pps_slave_get_stat( const m1pps_slave_t *slave, m1pps_sync_slave_stat_t *stat )
{
  (*stat) = slave->stat;
}

void m1pps_slave_get_pll_info(const m1pps_slave_t *slave, m1pps_sync_slave_pll_info_t *info)
{
  (*info) = slave->pll_info;
}

m1pps_slave_st_t m1pps_slave_process_err( m1pps_slave_t *slave, int err )
{
   m1pps_slave_reset_sync(slave );
   return M1PPS_SLAVE_ST_WAIT_SYNC;
}

m1pps_slave_st_t m1pps_slave_process_timeout( m1pps_slave_t *slave, const char *timeout_msg )
{
   slave->cmn.stat.err_timeout++;
   ERR("%s", timeout_msg);
   return m1pps_slave_process_err( slave, -ETIME );
}

int m1pps_slave_state_changed( struct m1pps_fsm *fsm, int msg_id, int old_st, int new_st )
{
   return 0;
}

int m1pps_slave_process_infreq(m1pps_slave_t *slave, const void *data, size_t data_sz)
{
  m1pps_msg_sync_infrsp_t *msg = __msg_buf(slave);

  memset(msg, 0, sizeof(*msg));

  msg->offset = slave->cmn.info.offset;
  msg->delay = slave->cmn.info.delay;
  msg->time_adjust_cnt = slave->stat.time_adjust_cnt;
  msg->period_adjust_cnt = slave->stat.period_adjust_cnt;

  if (slave->cbs.read_pll_info) {
    slave->cbs.read_pll_info(__fsm(slave), &slave->pll_info);
    msg->pll_flags = slave->pll_info.flags;
    msg->pll_phlock_dur_ms = slave->pll_info.phlock_dur_ms;
    msg->pll_frlock_dur_ms = slave->pll_info.frlock_dur_ms;
  }

  return __send_msg(slave, M1PPS_SLAVE_MSG_INFRSP, __msg_buf(slave), sizeof(*msg));
}

int m1pps_slave_process_msg( m1pps_fsm_t *fsm, int ev, const void *data, size_t data_sz )
{
  int err;
  m1pps_slave_t *slave = container_of( fsm, m1pps_slave_t, fsm);
  m1pps_slave_st_t ns, s;
  s = ns = __state(slave);

  switch ( s ) {
    case M1PPS_SLAVE_ST_INITIAL:
      if ( ev != M1PPS_SLAVE_EV_START )
         break;

      if ((err=m1pps_slave_process_start( slave ))) {
         break;
      }
      ns = M1PPS_SLAVE_ST_WAIT_SYNC;
    break;

    case M1PPS_SLAVE_ST_WAIT_SYNC:

      if ( ev == M1PPS_SLAVE_EV_STOP ) {
        ns = M1PPS_SLAVE_ST_INITIAL;
        break;
      }

      if ( ev == M1PPS_SLAVE_EV_RECIEVED_PULSE ) {
        __stop_timer(slave,  M1PPS_SLAVE_T0);
        m1pps_slave_change_mode( slave, M1PPS_SLAVE_MODE_DIRECTPULSE, 1);
        __start_timer( slave, M1PPS_SLAVE_T0, M1PPS_SLAVE_TIMER_WAIT_MODE_CHANGE );
        ns = M1PPS_SLAVE_ST_WAIT_MODE_CHANGE;
        break;
      }

      if (ev == M1PPS_SLAVE_EV_RECIEVED_INFREQ) {
        m1pps_slave_process_infreq(slave, data, data_sz);
        break;
      }

      if ( ev == M1PPS_SLAVE_EV_TIMER ) {
        if ( ! __timer( slave, M1PPS_SLAVE_T0 )) {
           m1pps_slave_process_timeout( slave, "waiting for next SYNC" );
           break;
        }
      }

      if ( ev != M1PPS_SLAVE_EV_RECIEVED_SYNC )
        break;

      __stop_timer(slave,  M1PPS_SLAVE_T0);

      if ( slave->mode == M1PPS_SLAVE_MODE_UNKNOWN ) {
        m1pps_slave_change_mode( slave, M1PPS_SLAVE_MODE_NORMAL, 1);
        __start_timer( slave, M1PPS_SLAVE_T0, M1PPS_SLAVE_TIMER_WAIT_MODE_CHANGE );
        ns = M1PPS_SLAVE_ST_WAIT_MODE_CHANGE;
        break;
      }

      if ((err=m1pps_slave_process_sync( slave, data, data_sz ))) {
        break;
      }

      __start_timer( slave, M1PPS_SLAVE_T0, M1PPS_SLAVE_TIMER_WAIT_FLUP );
      ns = M1PPS_SLAVE_ST_WAIT_FLUP;

    break;
    case M1PPS_SLAVE_ST_WAIT_FLUP:

      if ( ev == M1PPS_SLAVE_EV_STOP ) {
        ns = M1PPS_SLAVE_ST_INITIAL;
        break;
      }
      if ( ev == M1PPS_SLAVE_EV_RECIEVED_PULSE ) {
        __stop_timer(slave,  M1PPS_SLAVE_T0);
        m1pps_slave_change_mode( slave, M1PPS_SLAVE_MODE_DIRECTPULSE, 1);
        __start_timer( slave, M1PPS_SLAVE_T0, M1PPS_SLAVE_TIMER_WAIT_MODE_CHANGE );
        ns = M1PPS_SLAVE_ST_WAIT_MODE_CHANGE;
        break;
      }

      if ( ev == M1PPS_SLAVE_EV_TIMER ) {
        if ( ! __timer( slave, M1PPS_SLAVE_T0 )) {
           ns = m1pps_slave_process_timeout( slave, "waiting for FLUP message");
           break;
        }
      }

      if ( ev != M1PPS_SLAVE_EV_RECIEVED_FLUP )
        break;

      __stop_timer(slave, M1PPS_SLAVE_T0);

      if ((err = m1pps_slave_process_flup( slave, data, data_sz ))) {
        ns = m1pps_slave_process_err(slave, err);
        break;
      }

      if ((err = m1pps_slave_send_dlreq( slave ))) {
        ns = m1pps_slave_process_err(slave, err);
        break;
      }

      __start_timer( slave, M1PPS_SLAVE_T0, M1PPS_SLAVE_TIMER_WAIT_DLRESP );
      ns = M1PPS_SLAVE_ST_WAIT_DLREQ_SENDED;

    break;
    case M1PPS_SLAVE_ST_WAIT_DLREQ_SENDED:

      if ( ev == M1PPS_SLAVE_EV_STOP ) {
        ns = M1PPS_SLAVE_ST_INITIAL;
        break;
      }
      if ( ev == M1PPS_SLAVE_EV_RECIEVED_PULSE ) {
        __stop_timer(slave,  M1PPS_SLAVE_T0);
        m1pps_slave_change_mode( slave, M1PPS_SLAVE_MODE_DIRECTPULSE, 1);
        __start_timer( slave, M1PPS_SLAVE_T0, M1PPS_SLAVE_TIMER_WAIT_MODE_CHANGE );
        ns = M1PPS_SLAVE_ST_WAIT_MODE_CHANGE;
        break;
      }

      if ( ev == M1PPS_SLAVE_EV_TIMER ) {
        if ( ! __timer( slave, M1PPS_SLAVE_T0 )) {
           ns = m1pps_slave_process_timeout( slave, "waiting for DLREQ message sended");
           break;
        }
      }

      if ( ev != M1PPS_SLAVE_EV_SENDED )
        break;

      __stop_timer( slave, M1PPS_SLAVE_T0);

      if ((err=m1pps_slave_process_dlreq_sended( slave, data, data_sz ))) {
        ns = m1pps_slave_process_err(slave, err);
        break;
      }

      __start_timer( slave, M1PPS_SLAVE_T0, M1PPS_SLAVE_TIMER_WAIT_DLRESP );
      ns = M1PPS_SLAVE_ST_WAIT_DLRESP;

    break;

    case M1PPS_SLAVE_ST_WAIT_DLRESP:

      if ( ev == M1PPS_SLAVE_EV_STOP ) {
        ns = M1PPS_SLAVE_ST_INITIAL;
        break;
      }
      if ( ev == M1PPS_SLAVE_EV_RECIEVED_PULSE ) {
        __stop_timer(slave,  M1PPS_SLAVE_T0);
        m1pps_slave_change_mode( slave, M1PPS_SLAVE_MODE_DIRECTPULSE, 1);
        __start_timer( slave, M1PPS_SLAVE_T0, M1PPS_SLAVE_TIMER_WAIT_MODE_CHANGE );
        ns = M1PPS_SLAVE_ST_WAIT_MODE_CHANGE;
        break;
      }

      if ( ev == M1PPS_SLAVE_EV_TIMER ) {
        if ( ! __timer( slave, M1PPS_SLAVE_T0 ) ) {
          ns = m1pps_slave_process_timeout( slave, "waiting for DLRESP message");
          break;
        }
      }

      if ( ev != M1PPS_SLAVE_EV_RECIEVED_DLRESP)
        break;

      __stop_timer( slave, M1PPS_SLAVE_T0);

      m1pps_slave_process_dlresp(slave, data, data_sz);

      if (!__timer( slave, M1PPS_SLAVE_T1 ) && slave->allow_time_adjust)
        m1pps_slave_process_adjust_time( slave );

      __start_timer( slave, M1PPS_SLAVE_T0, M1PPS_SLAVE_TIMER_WAIT_NEXT_SYNC );
      ns = M1PPS_SLAVE_ST_WAIT_SYNC;

    break;
    case M1PPS_SLAVE_ST_WAIT_MODE_CHANGE:

      if ( ev == M1PPS_SLAVE_EV_STOP ) {
        ns = M1PPS_SLAVE_ST_INITIAL;
        break;
      }
      if ( ev != M1PPS_SLAVE_EV_TIMER )
        break;
      if ( __timer( slave, M1PPS_SLAVE_T0 ))
        break;
      __stop_timer( slave, M1PPS_SLAVE_T0);

      m1pps_slave_change_mode( slave, slave->new_mode, 0);
      ns = ( slave->mode == M1PPS_SLAVE_MODE_NORMAL ) ? M1PPS_SLAVE_ST_WAIT_SYNC : M1PPS_SLAVE_ST_WAIT_PULSE;

    break;
    case M1PPS_SLAVE_ST_WAIT_PULSE:

      if ( ev == M1PPS_SLAVE_EV_STOP ) {
        ns = M1PPS_SLAVE_ST_INITIAL;
        break;
      }
      if ( ev == M1PPS_SLAVE_EV_RECIEVED_SYNC ) {
        m1pps_slave_change_mode( slave, M1PPS_SLAVE_MODE_NORMAL, 1);
        __start_timer( slave, M1PPS_SLAVE_T0, M1PPS_SLAVE_TIMER_WAIT_MODE_CHANGE );
        ns = M1PPS_SLAVE_ST_WAIT_MODE_CHANGE;
        break;
      }

      if ( ev != M1PPS_SLAVE_EV_RECIEVED_PULSE )
        break;

      if ( slave->mode == M1PPS_SLAVE_MODE_UNKNOWN ) {
        m1pps_slave_change_mode( slave, M1PPS_SLAVE_MODE_DIRECTPULSE, 1);
        __start_timer( slave, M1PPS_SLAVE_T0, M1PPS_SLAVE_TIMER_WAIT_MODE_CHANGE );
        ns = M1PPS_SLAVE_ST_WAIT_MODE_CHANGE;
        break;
      }

      if ((err=m1pps_slave_process_pulse( slave, data, data_sz ))) {
      }

    break;

  }

  return ns;
}


