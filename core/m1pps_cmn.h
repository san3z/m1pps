/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_sys.h"

typedef struct m1pps_sync_cmn_info {
  uint8_t  proto_sync;
  m1pps_tofs_t delay;
  m1pps_tofs_t offset;
  m1pps_tofs_t offset_diff;
} m1pps_sync_cmn_info_t;

typedef struct m1pps_sync_cmn_stat {
  uint32_t cyrcles;
  uint32_t good_cyrcles;
  uint32_t err_timeout;
  uint32_t err_delay;
  uint32_t err_sync_lost;
} m1pps_sync_cmn_stat_t;

typedef struct m1pps_sync_cmn {
  m1pps_sync_cmn_info_t info;
  m1pps_sync_cmn_stat_t stat;
} m1pps_sync_cmn_t;
