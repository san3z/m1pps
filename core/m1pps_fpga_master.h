/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_fsm.h"
#include "m1pps_msg_fpga.h"

struct m1pps_fpga_mstchip;

/* fpga_mstchip - fpga chip object */

typedef struct m1pps_fpga_mstchip_cbs {
  int (*start_linit)(struct m1pps_fpga_mstchip *chip, size_t data_sz);
  int (*start_load)(struct m1pps_fpga_mstchip *chip, size_t data_sz);
} m1pps_fpga_mstchip_cbs_t;

typedef struct m1pps_fpga_mstchip_desc {
  char name[16];
  m1pps_fpga_mstchip_cbs_t cbs;
  void *ctx;
} m1pps_fpga_mstchip_desc_t;

typedef struct m1pps_fpga_mstchip {
  m1pps_fsm_t fsm;
  uint8_t id;
  m1pps_fpga_mstchip_desc_t desc;
  struct m1pps_fpga_master *master;
  uint8_t status;
  uint32_t offset, size;
  uint8_t *data;
  uint32_t loaded_sz;
  uint32_t full_sz;
} m1pps_fpga_mstchip_t;

typedef enum m1pps_fpga_mstchip_st {
  M1PPS_FPGA_MSTCHIP_ST_INITIAL = FSM_ST_INITIAL,
  M1PPS_FPGA_MSTCHIP_ST_STARTED,                  // 1
  M1PPS_FPGA_MSTCHIP_ST_INITLOADING,              // 2
  M1PPS_FPGA_MSTCHIP_ST_TRANSFERING,              // 2
  M1PPS_FPGA_MSTCHIP_ST_LOADING,                  // 2
  M1PPS_FPGA_MSTCHIP_ST_WORKING,                  // 2
} m1pps_fpga_mstchip_st_t;

typedef enum m1pps_fpga_mstchip_ev {
  M1PPS_FPGA_MSTCHIP_EV_TIMER = FSM_MSG_TIMER,              // 0
  M1PPS_FPGA_MSTCHIP_EV_START = M1PPS_MSG_START,            // 256
  M1PPS_FPGA_MSTCHIP_EV_STOP  = M1PPS_MSG_STOP,             // 257
  M1PPS_FPGA_MSTCHIP_EV_STARTLOAD = M1PPS_MSG_FPGA_STARTLOAD,

  M1PPS_FPGA_MSTCHIP_EV_STSRSP = M1PPS_MSG_FPGA_STSRSP,     // 1
  M1PPS_FPGA_MSTCHIP_EV_LINITRSP = M1PPS_MSG_FPGA_LINITRSP, // 2
  M1PPS_FPGA_MSTCHIP_EV_LDATARSP =  M1PPS_MSG_FPGA_LDATARSP,  // 4
  M1PPS_FPGA_MSTCHIP_EV_LOADRSP = M1PPS_MSG_FPGA_LOADRSP, // 4
} m1pps_fpga_mstchip_ev_t;

int m1pps_fpga_mstchip_init(m1pps_fpga_mstchip_t *chip, const m1pps_fsm_ticks_t *ticks,
                            uint8_t id);
void m1pps_fpga_mstchip_destroy( m1pps_fpga_mstchip_t *chip );

void m1pps_fpga_mstchip_set_cbs(m1pps_fpga_mstchip_t *chip, const m1pps_fpga_mstchip_cbs_t *cbs);

int m1pps_fpga_mstchip_load(m1pps_fpga_mstchip_t *chip, void *fpga_data, size_t fpga_data_sz);

/* fpga master - container of chips */

typedef struct m1pps_fpga_master_cbs {
  void *(*get_data)(struct m1pps_fsm *fsm, uint32_t offset, size_t size);
  void *(*init_chip)(struct m1pps_fsm *fsm, const struct m1pps_fpga_mstchip_desc *desc);
  void (*destroy_chip)(struct m1pps_fsm *fsm, const struct m1pps_fpga_mstchip_desc *desc);
  void (*load_complete)(struct m1pps_fsm *fsm, const struct m1pps_fpga_mstchip_desc *desc);
} m1pps_fpga_master_cbs_t;

typedef struct m1pps_fpga_master {
  m1pps_fsm_t fsm;
  m1pps_fpga_master_cbs_t cbs;
  m1pps_fpga_mstchip_t *chip;
  uint32_t max_load_sz;
  uint8_t  max_chip_cnt;
  uint8_t  chip_cnt;
} m1pps_fpga_master_t;

typedef enum m1pps_fpga_master_st {
  M1PPS_FPGA_MASTER_ST_INITIAL = FSM_ST_INITIAL,
  M1PPS_FPGA_MASTER_ST_STARTED,                  // 1
  M1PPS_FPGA_MASTER_ST_WAIT_INFRSP,
  M1PPS_FPGA_MASTER_ST_WAIT_BEFORE_INFREQ,
  M1PPS_FPGA_MASTER_ST_WORKING,                   // 3
} m1pps_fpga_master_st_t;

typedef enum m1pps_fpga_master_ev {
  M1PPS_FPGA_MASTER_EV_TIMER = FSM_MSG_TIMER,          // 0
  M1PPS_FPGA_MASTER_EV_START = M1PPS_MSG_START,        // 256
  M1PPS_FPGA_MASTER_EV_STOP = M1PPS_MSG_STOP,          // 257
  M1PPS_FPGA_MASTER_EV_STARTLOAD = M1PPS_MSG_FPGA_STARTLOAD,     // 258

  M1PPS_FPGA_MASTER_EV_INFRSP = M1PPS_MSG_FPGA_INFRSP, // 3
  M1PPS_FPGA_MASTER_EV_STSRSP = M1PPS_MSG_FPGA_STSRSP, // 5
  M1PPS_FPGA_MASTER_EV_LINITRSP = M1PPS_MSG_FPGA_LINITRSP,   // 3
  M1PPS_FPGA_MASTER_EV_LOADRSP = M1PPS_MSG_FPGA_LOADRSP      // 5
} m1pps_fpga_master_ev_t;

/*
typedef enum m1pps_fpga_master_msg {
} m1pps_fpga_master_msg_t;
*/

/* internal */

int m1pps_fpga_master_init(m1pps_fpga_master_t *master, const m1pps_fsm_ticks_t *ticks,
                           uint8_t max_cnt);
void m1pps_fpga_master_destroy( m1pps_fpga_master_t *master );

void m1pps_fpga_master_set_cbs( m1pps_fpga_master_t *master, const m1pps_fpga_master_cbs_t *cbs );

void m1pps_fpga_master_start_load( m1pps_fpga_master_t *master, uint8_t chip_id );
