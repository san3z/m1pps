/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_pkt_sync.h"
#include "m1pps_msg_sync.h"

#define M1PPS_PKTSUBSYS_SYNC      (0)

#pragma pack(push, 1)

#define M1PPS_PKTID_SYNC_SYNC        (1)
#define M1PPS_PKTID_SYNC_FLUP        (2)
#define M1PPS_PKTID_SYNC_DLREQ       (3)
#define M1PPS_PKTID_SYNC_DLRESP      (4)
#define M1PPS_PKTID_SYNC_PULSE       (5)
#define M1PPS_PKTID_SYNC_INFREQ      (6)
#define M1PPS_PKTID_SYNC_INFRSP      (7)

typedef struct m1pps_pkt_sync_sync {
  uint64_t t_high;
  uint64_t t_low;
} m1pps_pkt_sync_sync_t;

typedef struct m1pps_pkt_sync_flup {
  uint64_t     t1;
} m1pps_pkt_sync_flup_t;

typedef struct m1pps_pkt_sync_dlresp {
  uint64_t     t4;
} m1pps_pkt_sync_dlresp_t;

typedef struct m1pps_pkt_sync_dlreq {
  uint64_t     t2;
} m1pps_pkt_sync_dlreq_t;

typedef struct m1pps_pkt_sync_pulse {
  uint8_t      value;
} m1pps_pkt_sync_pulse_t;

typedef struct m1pps_pkt_sync_infreq {
} m1pps_pkt_sync_infreq_t;

typedef struct m1pps_pkt_sync_infrsp {
  uint64_t     offset;
  uint64_t     delay;
  uint64_t     time_adjust_cnt;
  uint64_t     period_adjust_cnt;
  uint8_t      pll_flags;
  uint32_t     pll_phlock_dur_ms;
  uint32_t     pll_frlock_dur_ms;
} m1pps_pkt_sync_infrsp_t;

#define M1PPS_PKT_SYNC_PLL_PHASELOCK  (0)
#define M1PPS_PKT_SYNC_PLL_FREQLOCK   (1)

#pragma pack(pop)

// sync subsys

size_t _m1pps_pkt_subsys_sync_pack_msg( m1pps_pkt_t *pkt, size_t offset, int msg_id, const void *msg_data, size_t msg_data_sz, uint8_t *pkt_id )
{
   size_t sz=0, max_sz = pkt->buf_sz - offset;
   void * pkt_body = pkt->buf + offset;

   switch ( msg_id ) {
     case M1PPS_MSG_SYNC_SYNC: {
       m1pps_pkt_sync_sync_t *p = pkt_body;
       const m1pps_msg_sync_sync_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->t_high = hton_64(m->t_high);
       p->t_low = hton_64(m->t_low);
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_SYNC_SYNC;
     } break;
     case M1PPS_MSG_SYNC_FLUP: {
       m1pps_pkt_sync_flup_t *p = pkt_body;
       const m1pps_msg_sync_flup_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->t1 = hton_64(m->t1);
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_SYNC_FLUP;
     } break;
     case M1PPS_MSG_SYNC_DLRESP: {
       m1pps_pkt_sync_dlresp_t *p = pkt_body;
       const m1pps_msg_sync_dlresp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->t4 = hton_64(m->t4);
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_SYNC_DLRESP;
     } break;
     case M1PPS_MSG_SYNC_DLREQ: {
       m1pps_pkt_sync_dlreq_t *p = pkt_body;
       const m1pps_msg_sync_dlreq_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->t2 = hton_64(m->t2);
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_SYNC_DLREQ;
     } break;
     case M1PPS_MSG_SYNC_PULSE: {
       m1pps_pkt_sync_pulse_t *p = pkt_body;
       const m1pps_msg_sync_pulse_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->value = m->value;
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_SYNC_PULSE;
     } break;
     case M1PPS_MSG_SYNC_INFREQ: {
       *pkt_id = M1PPS_PKTID_SYNC_INFREQ;
     } break;
     case M1PPS_MSG_SYNC_INFRSP: {
       m1pps_pkt_sync_infrsp_t *p = pkt_body;
       const m1pps_msg_sync_infrsp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->offset = hton_64(m->offset);
       p->delay = hton_64(m->delay);
       p->time_adjust_cnt = hton_32(m->time_adjust_cnt);
       p->period_adjust_cnt = hton_32(m->period_adjust_cnt);
       p->pll_flags = m->pll_flags;
       p->pll_phlock_dur_ms = hton_32(m->pll_phlock_dur_ms);
       p->pll_frlock_dur_ms = hton_32(m->pll_frlock_dur_ms);
       *pkt_id = M1PPS_PKTID_SYNC_INFRSP;
       sz += sizeof(*p);
     } break;
     default: return 0;
   }

   return sz;
}

int _m1pps_pkt_subsys_sync_unpack_msg( const m1pps_pkt_t *pkt, const m1pps_pkt_up_args_t *args,
                                       int *msg_id, void *msg_data, size_t *msg_data_sz  )
{
   size_t sz=0;
   void *pkt_body =  pkt->buf + args->body_ofs;
   switch ( args->id ) {
     case M1PPS_PKTID_SYNC_SYNC: {
       const m1pps_pkt_sync_sync_t *p = pkt_body;
       m1pps_msg_sync_sync_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->rx_ts = args->rx_ts;
       m->t_high = ntoh_64(p->t_high);
       m->t_low = ntoh_64(p->t_low);
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_SYNC_SYNC;
     } break;
     case M1PPS_PKTID_SYNC_FLUP: {
       const m1pps_pkt_sync_flup_t *p = pkt_body;
       m1pps_msg_sync_flup_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->t1 = ntoh_64(p->t1);
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_SYNC_FLUP;
     } break;
     case M1PPS_PKTID_SYNC_DLRESP: {
       const m1pps_pkt_sync_dlresp_t *p = pkt_body;
       m1pps_msg_sync_dlresp_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->t4 = ntoh_64(p->t4);
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_SYNC_DLRESP;
     } break;
     case M1PPS_PKTID_SYNC_DLREQ: {
       const m1pps_pkt_sync_dlreq_t *p = pkt_body;
       m1pps_msg_sync_dlreq_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->rx_ts = args->rx_ts;
       m->t2 = ntoh_64(p->t2);
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_SYNC_DLREQ;
     } break;
     case M1PPS_PKTID_SYNC_PULSE: {
       const m1pps_pkt_sync_pulse_t *p = pkt_body;
       m1pps_msg_sync_pulse_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->value = p->value;
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_SYNC_PULSE;
     } break;
     case M1PPS_PKTID_SYNC_INFREQ: {
       *msg_id = M1PPS_MSG_SYNC_INFREQ;
     } break;
     case M1PPS_PKTID_SYNC_INFRSP: {
       const m1pps_pkt_sync_infrsp_t *p = pkt_body;
       m1pps_msg_sync_infrsp_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->offset = ntoh_64(p->offset);
       m->delay = ntoh_64(p->delay);
       m->time_adjust_cnt = ntoh_32(p->time_adjust_cnt);
       m->period_adjust_cnt = ntoh_32(p->period_adjust_cnt);
       m->pll_flags = p->pll_flags;
       m->pll_phlock_dur_ms = ntoh_32(p->pll_phlock_dur_ms);
       m->pll_frlock_dur_ms = ntoh_32(p->pll_frlock_dur_ms);
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_SYNC_INFRSP;
     } break;
     default: return -EINVAL;
   }

   *msg_data_sz = sz;
   return 0;
}

#define __max(v1, v2) ((v1 < v2) ? v2 : v1)

size_t _m1pps_pkt_subsys_sync_get_max_sz(void)
{
  size_t sz = 0;
  sz = __max(sz, sizeof( m1pps_pkt_sync_sync_t));
  sz = __max(sz, sizeof( m1pps_pkt_sync_flup_t));
  sz = __max(sz, sizeof( m1pps_pkt_sync_dlresp_t));
  sz = __max(sz, sizeof( m1pps_pkt_sync_dlreq_t));
  sz = __max(sz, sizeof( m1pps_pkt_sync_pulse_t));
  sz = __max(sz, sizeof( m1pps_pkt_sync_infrsp_t));
  return sz;
}

m1pps_pkt_subsys_t m1pps_pkt_subsys_sync = {
  .name = "sync",
  .id =  M1PPS_PKTSUBSYS_SYNC,
  .pack_msg =  _m1pps_pkt_subsys_sync_pack_msg,
  .unpack_msg = _m1pps_pkt_subsys_sync_unpack_msg,
  .get_max_pkt_sz = _m1pps_pkt_subsys_sync_get_max_sz
};

