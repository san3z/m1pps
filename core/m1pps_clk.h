/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_sys.h"

/* abstract clock */
struct m1pps_clk;

typedef struct m1pps_clk_ops {
  int (*adj_time)( struct m1pps_clk *c, int64_t offset);
  int (*adj_period)( struct m1pps_clk *c, m1pps_tofs_t period_k);
  int (*set_output)( struct m1pps_clk *c, uint8_t enabled);
  m1pps_time_t (*get_time)( const struct m1pps_clk *c);
  int (*set_time)( struct m1pps_clk *c, m1pps_time_t time);
} m1pps_clk_ops_t;

typedef struct m1pps_clk {
  void *ctx;
  m1pps_time_t accurancy_ns;
  m1pps_clk_ops_t ops;
} m1pps_clk_t;

int m1pps_clk_init( m1pps_clk_t *c, const  m1pps_clk_ops_t *ops, m1pps_time_t accurancy_ns );
void m1pps_clk_destroy( m1pps_clk_t *c );

m1pps_time_t m1pps_clk_get_time(  const m1pps_clk_t *c );
int m1pps_clk_set_time( m1pps_clk_t *c, m1pps_time_t time );
int m1pps_clk_adj_time( m1pps_clk_t *c, int64_t offset );

int m1pps_clk_adj_period(m1pps_clk_t *c, m1pps_tofs_t period_k);
int m1pps_clk_set_output(m1pps_clk_t *c, uint8_t enabled);

void m1pps_clk_set_ctx( m1pps_clk_t *c, void *ctx );
void *m1pps_clk_get_ctx( const m1pps_clk_t *c );

m1pps_time_t m1pps_clk_get_accurancy( const m1pps_clk_t *c );


/* system main clock - implementation provide by OS specific code */
extern struct m1pps_clk  *m1pps_clk_sys;

int m1pps_clk_sys_init(void);
void m1pps_clk_sys_destroy(void);

#define __clk(c) (&((c)->clk))
