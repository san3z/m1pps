/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_fsm.h"
#include "m1pps_msg_fpga.h"
#include "m1pps_fpga.h"


/* fpga_slvchip - fpgs chip object */

struct m1pps_fpga_slvchip;

typedef struct m1pps_fpga_slvchip_cbs {
  int (*set_pin)(struct m1pps_fpga_slvchip *chip, uint8_t pin, uint8_t value);
  int (*get_pin)(struct m1pps_fpga_slvchip *chip, uint8_t pin, uint8_t *value);
  int (*start_linit)(struct m1pps_fpga_slvchip *chip, size_t data_sz);
  int (*start_load)(struct m1pps_fpga_slvchip *chip, void *data, size_t data_sz);
} m1pps_fpga_slvchip_cbs_t;

typedef struct m1pps_fpga_slvchip_desc {
  char name[16];
  m1pps_fpga_slvchip_cbs_t cbs;
  void *ctx;
} m1pps_fpga_slvchip_desc_t;

typedef struct m1pps_fpga_slvchip {
  m1pps_fsm_t fsm;
  uint8_t id;
  m1pps_fpga_slvchip_desc_t desc;
  struct m1pps_fpga_slave *slave;
  void *load_data;
} m1pps_fpga_slvchip_t;

typedef enum m1pps_fpga_slvchip_st {
  M1PPS_FPGA_SLVCHIP_ST_INITIAL = FSM_ST_INITIAL,
  M1PPS_FPGA_SLVCHIP_ST_STARTED,                  // 1
  M1PPS_FPGA_SLVCHIP_ST_LOADING,                  // 2
  M1PPS_FPGA_SLVCHIP_ST_WAIT_DATA,
  M1PPS_FPGA_SLVCHIP_ST_WORKED,
} m1pps_fpga_slvchip_st_t;

typedef enum m1pps_fpga_slvchip_ev {
  M1PPS_FPGA_SLVCHIP_EV_TIMER = FSM_MSG_TIMER,              // 0
  M1PPS_FPGA_SLVCHIP_EV_START = M1PPS_MSG_START,            // 256
  M1PPS_FPGA_SLVCHIP_EV_STOP  = M1PPS_MSG_STOP,             // 257
  M1PPS_FPGA_SLVCHIP_EV_STSREQ = M1PPS_MSG_FPGA_STSREQ,     // 1
  M1PPS_FPGA_SLVCHIP_EV_LINITREQ = M1PPS_MSG_FPGA_LINITREQ, // 2
  M1PPS_FPGA_SLVCHIP_EV_LOADREQ =  M1PPS_MSG_FPGA_LOADREQ,  // 4
  M1PPS_FPGA_SLVCHIP_EV_LOADCMPL = M1PPS_MSG_FPGA_LOADCMPL, // 4
  M1PPS_FPGA_SLVCHIP_EV_LDATAREQ = M1PPS_MSG_FPGA_LDATAREQ
} m1pps_fpga_slvchip_ev_t;

int m1pps_fpga_slvchip_init( m1pps_fpga_slvchip_t *chip, const m1pps_fsm_ticks_t *ticks,
                             uint8_t id, const m1pps_fpga_slvchip_desc_t *chip_desc );
void m1pps_fpga_slvchip_destroy( m1pps_fpga_slvchip_t *chip );

//void m1pps_fpga_slvchip_set_cbs( m1pps_fpga_slvchip_t *chip, const m1pps_fpga_slvchip_cbs_t *cbs );

int m1pps_fpga_slvchip_load( m1pps_fpga_slvchip_t *chip, void *fpga_data, size_t fpga_data_sz );

/* fpga_slave - container of fpga chips */

typedef struct m1pps_fpga_slave {
  m1pps_fsm_t fsm;
  m1pps_fpga_slvchip_t *chip;
  uint8_t chip_cnt;
} m1pps_fpga_slave_t;

typedef enum m1pps_fpga_slave_st {
  M1PPS_FPGA_SLAVE_ST_INITIAL = FSM_ST_INITIAL,
  M1PPS_FPGA_SLAVE_ST_STARTED,                    // 1
} m1pps_fpga_slave_st_t;

typedef enum m1pps_fpga_slave_ev {
  M1PPS_FPGA_SLAVE_EV_TIMER = FSM_MSG_TIMER,               // 0
  M1PPS_FPGA_SLAVE_EV_START = M1PPS_MSG_START,             // 256
  M1PPS_FPGA_SLAVE_EV_STOP  = M1PPS_MSG_STOP,              // 257
  M1PPS_FPGA_SLAVE_EV_STSREQ = M1PPS_MSG_FPGA_STSREQ,      // 1
  M1PPS_FPGA_SLAVE_EV_INFREQ = M1PPS_MSG_FPGA_INFREQ,      // 1
  M1PPS_FPGA_SLAVE_EV_LINITREQ = M1PPS_MSG_FPGA_LINITREQ,  // 2
  M1PPS_FPGA_SLAVE_EV_LOADREQ = M1PPS_MSG_FPGA_LOADREQ,    // 4
  M1PPS_FPGA_SLAVE_EV_LOADCMPL = M1PPS_MSG_FPGA_LOADCMPL,  // 4
} m1pps_fpga_slave_ev_t;

int m1pps_fpga_slave_init( m1pps_fpga_slave_t *slave, const m1pps_fsm_ticks_t *ticks,
                           m1pps_fpga_slvchip_desc_t *chip_desc, uint8_t count);
void m1pps_fpga_slave_destroy( m1pps_fpga_slave_t *slave );

uint8_t m1pps_fpga_slave_get_cnt( const m1pps_fpga_slave_t *slave);
m1pps_fpga_slvchip_t *m1pps_fpga_slave_get_chip( const m1pps_fpga_slave_t *slave, uint8_t chip_idx);

/*
void m1pps_fpga_slave_get_info( const m1pps_fpga_slave_t *slave, m1pps_info_t *info);
void m1pps_fpga_slave_get_stat( const m1pps_fpga_slave_t *slave, m1pps_stat_t *stat);
*/
