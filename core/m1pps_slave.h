/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_fsm.h"
#include "m1pps_clk.h"
#include "m1pps_msg_sync.h"
#include "m1pps_cmn.h"
#include "m1pps_ts_filter.h"

typedef struct m1pps_sync_slave_info {
  m1pps_tofs_t delay_filtered;
  m1pps_tofs_t offset_filtered;
  m1pps_tofs_t period_k;
  m1pps_tofs_t period_k_filtered;
  m1pps_tofs_t period_k_adjusted;
  m1pps_tofs_t offset_diff_filtered;
  m1pps_tofs_t t_diff_filtered;
  uint8_t flags;
} m1pps_sync_slave_info_t;

typedef struct m1pps_sync_slave_stat {
  uint32_t period_adjust_cnt;
  uint32_t time_adjust_cnt;
} m1pps_sync_slave_stat_t;

typedef struct m1pps_sync_slave_pll_info {
  uint8_t  flags;
  uint32_t phlock_dur_ms;
  uint32_t frlock_dur_ms;
} m1pps_sync_slave_pll_info_t;

typedef struct m1pps_slave_cbs {
  int (*set_mode)(struct m1pps_fsm *fsm, uint8_t mode, uint8_t start_to_change);
  int (*send_pulse)(struct m1pps_fsm *fsm, uint8_t value);
  int (*read_pll_info)(struct m1pps_fsm *fsm, m1pps_sync_slave_pll_info_t *pll_info);
} m1pps_slave_cbs_t;

typedef struct m1pps_sync_slave_preproc {
  m1pps_tofs_t bkwd, frwd;
  m1pps_tofs_t offset_diff;
  m1pps_tofs_t t_diff;
} m1pps_sync_slave_preproc_t;

typedef struct m1pps_slave {
  m1pps_fsm_t fsm;
  m1pps_sync_cmn_t cmn; // common fields
  m1pps_time_t t[5]; // t0 not used .. t1 t2 t3 t4
  m1pps_sync_slave_info_t info;
  m1pps_sync_slave_stat_t stat;
  m1pps_sync_slave_pll_info_t pll_info;
  uint16_t cyrcles;
  m1pps_clk_t *clk;
  m1pps_slave_cbs_t cbs;
  m1pps_ts_filter_t filter;
  m1pps_ts_filter_t period_filter;
  m1pps_tofs_t bkwd, frwd;
  m1pps_tofs_t prev_offset;
  uint8_t mode, new_mode;
  uint8_t period_adjusted;
  uint8_t allow_time_adjust;
  uint8_t adj_mode;
  m1pps_sync_slave_preproc_t preproc;
} m1pps_slave_t;

#define M1PPS_SLAVE_MODE_UNKNOWN     (0)
#define M1PPS_SLAVE_MODE_NORMAL      (1)
#define M1PPS_SLAVE_MODE_DIRECTPULSE (2)

#define M1PPS_SYNC_SLAVE_ADJMODE_PERIOD  (0)
#define M1PPS_SYNC_SLAVE_ADJMODE_TIME    (1)

typedef enum m1pps_slave_st {
  M1PPS_SLAVE_ST_INITIAL = FSM_ST_INITIAL,
  M1PPS_SLAVE_ST_WAIT_SYNC,           // 1
  M1PPS_SLAVE_ST_WAIT_FLUP,           // 2
  M1PPS_SLAVE_ST_WAIT_DLREQ_SENDED,   // 3
  M1PPS_SLAVE_ST_WAIT_DLRESP,         // 4
  M1PPS_SLAVE_ST_WAIT_PULSE,          // 5
  M1PPS_SLAVE_ST_WAIT_MODE_CHANGE,    // 6
} m1pps_slave_st_t;

typedef enum m1pps_slave_ev {
  M1PPS_SLAVE_EV_TIMER = FSM_MSG_TIMER,             // 0
  M1PPS_SLAVE_EV_START = M1PPS_MSG_START,           // 256
  M1PPS_SLAVE_EV_STOP = M1PPS_MSG_STOP,             // 257
  M1PPS_SLAVE_EV_SENDED = M1PPS_MSG_SYNC_SENDED,         // 5
  M1PPS_SLAVE_EV_RECIEVED_SYNC = M1PPS_MSG_SYNC_SYNC,    // 1
  M1PPS_SLAVE_EV_RECIEVED_FLUP = M1PPS_MSG_SYNC_FLUP,    // 2
  M1PPS_SLAVE_EV_RECIEVED_DLRESP = M1PPS_MSG_SYNC_DLRESP,// 4
  M1PPS_SLAVE_EV_RECIEVED_PULSE = M1PPS_MSG_SYNC_PULSE,    // 5
  M1PPS_SLAVE_EV_RECIEVED_INFREQ = M1PPS_MSG_SYNC_INFREQ   // 5
} m1pps_slave_ev_t;

typedef enum m1pps_slave_msg {
  M1PPS_SLAVE_MSG_DLREQ = M1PPS_MSG_SYNC_DLREQ,             // 3
  M1PPS_SLAVE_MSG_INFRSP = M1PPS_MSG_SYNC_INFRSP           // 3
} m1pps_slave_msg_t;

int m1pps_slave_init( m1pps_slave_t *slave, const m1pps_fsm_ticks_t *ticks, m1pps_clk_t *clk );
void m1pps_slave_destroy( m1pps_slave_t *slave );

void m1pps_slave_set_cbs( m1pps_slave_t *slave, const m1pps_slave_cbs_t *cbs );

void m1pps_slave_get_cmn_info(const m1pps_slave_t *slave, m1pps_sync_cmn_info_t *info );
void m1pps_slave_get_cmn_stat(const m1pps_slave_t *slave, m1pps_sync_cmn_stat_t *stat );

void m1pps_slave_get_info(const m1pps_slave_t *slave, m1pps_sync_slave_info_t *info );
void m1pps_slave_get_stat(const m1pps_slave_t *slave, m1pps_sync_slave_stat_t *stat );

void m1pps_slave_get_pll_info(const m1pps_slave_t *slave, m1pps_sync_slave_pll_info_t *info);

uint8_t m1pps_slave_get_mode( const m1pps_slave_t *slave );
