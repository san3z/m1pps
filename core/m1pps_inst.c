/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_inst.h"


#define M1PPS_INST_MUTEX_TRS (1)
#define M1PPS_INST_MUTEX_FSM (0)
#define M1PPS_INST_MUTEX_CNT (2)

int m1pps_inst_pkt_sended( struct m1pps_trs *t, m1pps_time_t pkt_tx_ts );
int m1pps_inst_pkt_recieved( struct m1pps_trs *t, const void *pkt_data,
                             size_t pkt_data_sz, m1pps_time_t pkt_rx_ts );
int m1pps_inst_send_msg( struct m1pps_fsm *fsm, int ev,
                          const void *data, size_t data_sz );

inline m1pps_fsm_t *m1pps_inst_get_fsm( m1pps_inst_t *inst, uint8_t fsm_idx )
{
  return inst->fsm[fsm_idx];
}

static uint8_t _m1pps_inst_add_fsm( m1pps_inst_t *inst, m1pps_fsm_t *fsm )
{
  if (inst->fsm_cnt >= M1PPS_INST_FSM_MAX_CNT)
    return UINT8_MAX;

  inst->fsm[inst->fsm_cnt]=fsm;

  return inst->fsm_cnt++;
}

uint8_t m1pps_inst_register_fsm( m1pps_inst_t *inst, m1pps_fsm_t *fsm )
{
  m1pps_fsm_trs_ops_t trs_ops;
  trs_ops.send_msg = m1pps_inst_send_msg;
  m1pps_fsm_set_trs(fsm, &trs_ops );
  return _m1pps_inst_add_fsm( inst, fsm );
}

uint8_t m1pps_inst_register_subsys( m1pps_inst_t *inst, const m1pps_pkt_subsys_t *subsys, m1pps_fsm_t *fsm, uint8_t *fsmidx )
{
  uint8_t fsm_idx;
  m1pps_fsm_set_ctx(fsm, 1, (void*)subsys );
  fsm_idx = m1pps_inst_register_fsm( inst, fsm );
  if ( fsm_idx == UINT8_MAX )
    return -EINVAL;
  if ( m1pps_pkt_substms_register_subsys( &inst->substms, subsys ))
    return -EINVAL;
  m1pps_pkt_substms_set_subsys_ctx( &inst->substms, subsys->id, fsm );
  DBG("registered subsys %s (%d)", subsys->name, subsys->id );
  if (fsmidx)
     *fsmidx = fsm_idx;
  return 0;
}

int m1pps_inst_send_pulse( struct m1pps_fsm *fsm, uint8_t value )
{
  struct m1pps_inst *inst = m1pps_fsm_get_ctx(fsm, 0);
  if (! (inst && inst->cbs.send_pulse) )
    return 0;
  return inst->cbs.send_pulse(inst, value);
}

int m1pps_inst_set_mode( struct m1pps_fsm *fsm, uint8_t mode, uint8_t start_to_change )
{
  struct m1pps_inst *inst = m1pps_fsm_get_ctx(fsm, 0);
  if (! (inst && inst->cbs.set_mode) )
    return 0;
  return inst->cbs.set_mode(inst, mode, start_to_change);
}

int m1pps_inst_read_pll_info( struct m1pps_fsm *fsm, m1pps_sync_slave_pll_info_t *info )
{
  struct m1pps_inst *inst = m1pps_fsm_get_ctx(fsm, 0);
  if (! (inst && inst->cbs.read_pll_info) )
    return 0;
  return inst->cbs.read_pll_info(inst, info);
}

int m1pps_inst_init(m1pps_inst_t *inst, const m1pps_inst_prm_t *prm, const m1pps_inst_cbs_t *cbs,
                    m1pps_clk_t *clk )
{
  int err=0;
  size_t max_pkt_sz;

  inst->role = prm->role;
  inst->trs = 0;
  inst->clk = clk;
  inst->fsm_cnt = 0;

  DBG("role:%u pkt_sz:%"PRIsz" ctx_cnt:%u", prm->role, prm->max_pkt_sz, prm->ctx_cnt);

  m1pps_pkt_substms_init( &inst->substms, M1PPS_INST_SUBSYS_MAX_CNT );

  m1pps_fsm_ticks_init(&inst->ticks, 1000000/prm->timer_period_us);

  if ( cbs )
    inst->cbs = *cbs;

  /* initialize main fsm */
  switch (inst->role) {
    case M1PPS_INST_ROLE_MASTER:
      if ((err=m1pps_master_init( &inst->master, &inst->ticks, clk )))
        goto error;
      inst->sync_fsm = __fsm(&inst->master);
    break;
    case M1PPS_INST_ROLE_SLAVE: {
      m1pps_slave_cbs_t slave_cbs={0};
      if ((err=m1pps_slave_init( &inst->slave, &inst->ticks, clk )))
        goto error;
      if (inst->cbs.send_pulse || inst->cbs.set_mode || slave_cbs.read_pll_info ) {
        slave_cbs.send_pulse = m1pps_inst_send_pulse;
        slave_cbs.set_mode = m1pps_inst_set_mode;
        slave_cbs.read_pll_info = m1pps_inst_read_pll_info;
        m1pps_slave_set_cbs( &inst->slave, &slave_cbs );
      }
      inst->sync_fsm = __fsm(&inst->slave);
    } break;
    default:
      err = -EINVAL;
      goto error;
  }

  m1pps_fsm_set_ctx(inst->sync_fsm, 0, inst);

  if ( (err = m1pps_inst_register_subsys( inst, &m1pps_pkt_subsys_sync, inst->sync_fsm, 0 )) ) {
    goto error;
  }

  /* initialized mutexes */
  if ( inst->cbs.mutexes_create ) {
    inst->mutexes = inst->cbs.mutexes_create( inst, M1PPS_INST_MUTEX_CNT, &inst->mutex_sz );
    if ( !inst->mutexes ) {
      err = -ENOMEM;
      goto error;
    }
  }

  /* get max pkt size */
  if ( prm->max_pkt_sz )
    max_pkt_sz = prm->max_pkt_sz;
  else
    max_pkt_sz = m1pps_pkt_substms_get_max_pkt_sz( &inst->substms );

  /* initialize pkt packer/unpacker buffers */
  if ((err=m1pps_pkt_init( inst->pkt+RX, &inst->substms, max_pkt_sz )))
    goto error;

  if ((err=m1pps_pkt_init( inst->pkt+TX, &inst->substms, max_pkt_sz )))
    goto error;

  /* create buffer for msg */
  inst->buf_sz = max_pkt_sz + 16;
  if ( !(inst->buf[RX] = sys_zalloc( inst->buf_sz<<1 ))) {
     err = -ENOMEM;
     goto error;
  }

  inst->buf[TX] = (uint8_t*)inst->buf[RX] + inst->buf_sz;

  if ( prm->ctx_cnt ) {
    if ( !(inst->ctx=sys_zalloc(sizeof(void*)*prm->ctx_cnt)) ) {
      err= -ENOMEM;
      goto error;
    }
  }

error:

  if (err)
    m1pps_inst_destroy( inst );

  return err;
}

void m1pps_inst_destroy(m1pps_inst_t *inst)
{
  m1pps_pkt_substms_destroy(&inst->substms);

  if (inst->ctx) {
    sys_free(inst->ctx);
    inst->ctx=0;
  }
  if (inst->mutexes && inst->cbs.mutexes_free ) {
    inst->cbs.mutexes_free(inst, inst->mutexes);
  }
  if (inst->buf)  {
    sys_free( inst->buf[0] );
    inst->buf[0]=0;
  }
  if ( inst->sync_fsm ) {
    switch (inst->role) {
      case M1PPS_INST_ROLE_MASTER:
        m1pps_master_destroy( &inst->master );
      break;
      case M1PPS_INST_ROLE_SLAVE:
        m1pps_slave_destroy( &inst->slave );
      break;
      default:;
    }
    inst->sync_fsm = 0;
  }
  m1pps_pkt_destroy( inst->pkt+RX );
  m1pps_pkt_destroy( inst->pkt+TX );
}

static void m1pps_inst_mutex_lock( m1pps_inst_t *inst, uint8_t mutex_idx )
{
  if (inst->cbs.mutex_lock )
    inst->cbs.mutex_lock(inst, (uint8_t*)inst->mutexes + inst->mutex_sz*mutex_idx );
}

static void m1pps_inst_mutex_unlock( m1pps_inst_t *inst, uint8_t mutex_idx )
{
  if (inst->cbs.mutex_unlock )
    inst->cbs.mutex_unlock(inst, (uint8_t*)inst->mutexes + inst->mutex_sz*mutex_idx );
}

int m1pps_inst_set_trs( m1pps_inst_t *inst, m1pps_trs_t *trs )
{
  m1pps_trs_cbs_t trs_cbs={0};

  inst->trs = trs;
  /* set transport cb */
  trs_cbs.pkt_recieved = m1pps_inst_pkt_recieved;
  trs_cbs.pkt_sended = m1pps_inst_pkt_sended;
  m1pps_trs_set_cbs(inst->trs, &trs_cbs );

  m1pps_trs_set_ctx(inst->trs, inst);
  return 0;
}

void m1pps_inst_tick( m1pps_inst_t *inst )
{
  uint8_t i;
  m1pps_fsm_ticks_inc( &inst->ticks );
  m1pps_inst_mutex_lock( inst, M1PPS_INST_MUTEX_FSM );
  for (i=0;i<inst->fsm_cnt;++i) {
    m1pps_fsm_t *fsm = m1pps_inst_get_fsm( inst, i);
    if (fsm)
      m1pps_fsm_tick(fsm);
  }
  m1pps_inst_mutex_unlock( inst, M1PPS_INST_MUTEX_FSM );
}

int m1pps_inst_pkt_sended( struct m1pps_trs *t, m1pps_time_t pkt_tx_ts )
{
  struct m1pps_inst *inst = m1pps_trs_get_ctx(t);
  m1pps_msg_sync_sended_t *m = (m1pps_msg_sync_sended_t*)inst->buf[RX];

  #ifdef M1PPS_DEBUG
  char t_str[T_SZ];
  time2s(pkt_tx_ts, t_str, sizeof(t_str));
  DBG("psz:%"PRIsz" tx_ts:%s",  (size_t)0, t_str );
  #endif


  m->tx_ts = pkt_tx_ts;


  // fix-me !!!!! need to search for fsm
  m1pps_inst_mutex_lock( inst, M1PPS_INST_MUTEX_FSM );
  m1pps_fsm_process_msg( inst->sync_fsm, M1PPS_MSG_SYNC_SENDED, inst->buf[RX], sizeof(*m) );
  m1pps_inst_mutex_unlock( inst, M1PPS_INST_MUTEX_FSM );
  return 0;
}

int m1pps_inst_pkt_recieved( struct m1pps_trs *t, const void *pkt_data,
                             size_t pkt_data_sz, m1pps_time_t pkt_rx_ts )
{
  struct m1pps_inst *inst = m1pps_trs_get_ctx(t);
  size_t msg_data_sz;
  int subsys_id, msg_id;
  m1pps_fsm_t *fsm;

  #ifdef M1PPS_DEBUG
  char t_str[T_SZ];
  time2s(pkt_rx_ts, t_str, sizeof(t_str));
  DBG("psz:%"PRIsz" ts:%s", pkt_data_sz, t_str );
  #endif

  if (pkt_data_sz > m1pps_pkt_get_buf_sz(inst->pkt+RX))
     return -EINVAL;

  /* copy pkt from trasnport buffer */
  memcpy( m1pps_pkt_get_buf(inst->pkt + RX), pkt_data, pkt_data_sz );

  /* unpack message from line format */
  msg_data_sz = inst->buf_sz;
  if ( m1pps_pkt_unpack_msg( inst->pkt + RX, pkt_data_sz, pkt_rx_ts,
                             &subsys_id,  &msg_id, inst->buf[RX], &msg_data_sz )) {
     ERR("Can't unpack packet with size %"PRIsz, pkt_data_sz);
     return -EINVAL;
  }

  DBG("upacked to %"PRIsz" bytes", msg_data_sz);

  if (msg_id & M1PPS_MSG_INT) {
     ERR("msg_id:%d is internal and can't be processed", msg_id );
     return -EINVAL;
  }

  fsm = (m1pps_fsm_t*)m1pps_pkt_substms_get_subsys_ctx( &inst->substms, subsys_id );

  if (!fsm)
    return -EINVAL;

  m1pps_inst_mutex_lock( inst, M1PPS_INST_MUTEX_FSM );
  m1pps_fsm_process_msg( fsm, msg_id, inst->buf[RX], msg_data_sz );
  m1pps_inst_mutex_unlock( inst, M1PPS_INST_MUTEX_FSM );
  return 0;
}

int m1pps_inst_send_msg( struct m1pps_fsm *fsm, int msg_id,
                          const void *data, size_t data_sz )
{
  struct m1pps_inst *inst = m1pps_fsm_get_ctx(fsm, 0);
  struct m1pps_pkt_subsys *subsys = m1pps_fsm_get_ctx(fsm, 1);
  size_t pkt_sz;
  DBG("msg_id:%d dsz:%"PRIsz, msg_id, data_sz );

  /* pack message to line format */
  pkt_sz = m1pps_pkt_pack_msg( inst->pkt + TX, subsys->id, msg_id, data, data_sz );
  if ( !pkt_sz ) {
    ERR("Can't pack msg id:%d dsz:%"PRIsz" to packet!", msg_id, data_sz);
    return -EINVAL;
  }

  /* send message to transport */

  if ( !inst->trs )
    return -EINVAL;

  return  m1pps_trs_send_pkt( inst->trs, m1pps_pkt_get_buf( inst->pkt + TX ), pkt_sz);
}


void m1pps_inst_set_ctx( const m1pps_inst_t *inst, uint8_t ctx_idx, void *ctx )
{
  inst->ctx[ctx_idx] = ctx;
}

void *m1pps_inst_get_ctx( const m1pps_inst_t *inst, uint8_t ctx_idx )
{
  return inst->ctx[ctx_idx];
}

int m1pps_inst_process_msg( m1pps_inst_t *inst, int subsys_id, int msg_id, const void *data, size_t data_sz )
{
  m1pps_fsm_t *fsm;

  fsm = (m1pps_fsm_t*)m1pps_pkt_substms_get_subsys_ctx( &inst->substms, subsys_id );

  if ( !fsm )
    return -EINVAL;

  return m1pps_fsm_process_msg( fsm, msg_id, data, data_sz );
}

int m1pps_inst_set_role( const m1pps_inst_t *inst, m1pps_inst_role_t role )
{
  return -EINVAL;
}

m1pps_inst_role_t m1pps_inst_get_role( const m1pps_inst_t *inst )
{
  return inst->role;
}

int m1pps_inst_get_sync_info( const m1pps_inst_t *inst, m1pps_sync_cmn_info_t *info )
{
  if ( !inst->fsm )
    return -EINVAL;

  switch (inst->role) {
    case M1PPS_INST_ROLE_MASTER:
      m1pps_master_get_cmn_info( &inst->master, info );
    break;
    case M1PPS_INST_ROLE_SLAVE:
      m1pps_slave_get_cmn_info( &inst->slave, info );
    break;
    default:
      return -EINVAL;
  }

  return 0;
}

int m1pps_inst_get_sync_stat( const m1pps_inst_t *inst, m1pps_sync_cmn_stat_t *stat )
{
  if ( !inst->fsm )
    return -EINVAL;

  switch (inst->role) {
    case M1PPS_INST_ROLE_MASTER:
      m1pps_master_get_cmn_stat( &inst->master, stat );
    break;
    case M1PPS_INST_ROLE_SLAVE:
      m1pps_slave_get_cmn_stat( &inst->slave, stat );
    break;
    default:
      return -EINVAL;
  }

  return 0;

}

int m1pps_inst_get_trs_stat( const m1pps_inst_t *inst, m1pps_trs_stat_t *stat )
{
  if ( !inst->trs )
    return -EINVAL;

  m1pps_trs_get_stat( inst->trs, stat );
  return 0;
}

uint8_t m1pps_inst_get_sync_mode( const m1pps_inst_t *inst )
{
  if ( !inst->sync_fsm )
    return 0;

  if ( inst->role != M1PPS_INST_ROLE_SLAVE)
    return 0;

  return  m1pps_slave_get_mode( &inst->slave);
}

inline size_t m1pps_inst_get_max_pkt_sz( const m1pps_inst_t *inst )
{
  return m1pps_pkt_substms_get_max_pkt_sz( &inst->substms);
}
