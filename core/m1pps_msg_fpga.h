/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_fsm.h"
#include "m1pps_sys.h"
#include "m1pps_msg.h"

#define M1PPS_MSG_FPGA_START       (M1PPS_MSG_START)
#define M1PPS_MSG_FPGA_STOP        (M1PPS_MSG_STOP)
#define M1PPS_MSG_FPGA_STARTLOAD   (M1PPS_MSG_USER_INT+0)

#define M1PPS_MSG_FPGA_INFREQ        (FSM_MSG_USER+0)
#define M1PPS_MSG_FPGA_INFRSP        (FSM_MSG_USER+1)
#define M1PPS_MSG_FPGA_STSREQ        (FSM_MSG_USER+2)
#define M1PPS_MSG_FPGA_STSRSP        (FSM_MSG_USER+3)
#define M1PPS_MSG_FPGA_LINITREQ      (FSM_MSG_USER+4)
#define M1PPS_MSG_FPGA_LINITRSP      (FSM_MSG_USER+5)
#define M1PPS_MSG_FPGA_LDATAREQ      (FSM_MSG_USER+6)
#define M1PPS_MSG_FPGA_LDATARSP      (FSM_MSG_USER+7)
#define M1PPS_MSG_FPGA_LOADREQ       (FSM_MSG_USER+8)
#define M1PPS_MSG_FPGA_LOADRSP       (FSM_MSG_USER+9)
#define M1PPS_MSG_FPGA_TXREQ         (FSM_MSG_USER+10)
#define M1PPS_MSG_FPGA_TXRSP         (FSM_MSG_USER+11)
#define M1PPS_MSG_FPGA_RXREQ         (FSM_MSG_USER+12)
#define M1PPS_MSG_FPGA_RXRSP         (FSM_MSG_USER+13)

#define M1PPS_MSG_FPGA_LOADCMPL      (FSM_MSG_USER+32)

#define __M1PPS_MSG_FPGA_RSP \
  int16_t ret_code; \

#define __M1PPS_MSG_FPGA_CHIPRSP \
  int16_t ret_code; \
  uint8_t chip_id

#define __M1PPS_MSG_FPGA_CHIPREQ \
  uint8_t chip_id

typedef struct m1pps_msg_fpga_rsp {
  __M1PPS_MSG_FPGA_RSP;
} m1pps_msg_fpga_rsp_t;

typedef struct m1pps_msg_fpga_chiprsp {
  __M1PPS_MSG_FPGA_CHIPRSP;
} m1pps_msg_fpga_chiprsp_t;

typedef struct m1pps_msg_fpga_chipreq {
  __M1PPS_MSG_FPGA_CHIPREQ;
} m1pps_msg_fpga_chipreq_t;

typedef struct m1pps_msg_fpga_chip_sts_rsp {
  __M1PPS_MSG_FPGA_CHIPRSP;
  uint8_t status;
} m1pps_msg_fpga_chip_sts_rsp_t;

typedef struct m1pps_msg_fpgachip_desc {
  char name[16];
  uint8_t chip_id;
} m1pps_msg_fpgachip_desc_t;

typedef struct m1pps_msg_fpga_infreq {
} m1pps_msg_fpga_infreq_t;

typedef struct m1pps_msg_fpga_infrsp {
  int16_t  ret_code;
  uint32_t max_load_sz;
  uint8_t  chip_cnt;
  m1pps_msg_fpgachip_desc_t chip[0];
} m1pps_msg_fpga_infrsp_t;

typedef m1pps_msg_fpga_chipreq_t m1pps_msg_fpga_stsreq_t;
typedef m1pps_msg_fpga_chip_sts_rsp_t m1pps_msg_fpga_stsrsp_t;

typedef m1pps_msg_fpga_chipreq_t m1pps_msg_fpga_linitreq_t;
typedef m1pps_msg_fpga_chip_sts_rsp_t m1pps_msg_fpga_linitrsp_t;

typedef struct m1pps_msg_fpga_ldatareq {
  __M1PPS_MSG_FPGA_CHIPREQ;
  uint32_t offset;
  uint8_t  data[0];
} m1pps_msg_fpga_ldatareq_t;

typedef m1pps_msg_fpga_chip_sts_rsp_t m1pps_msg_fpga_ldatarsp_t;

typedef struct m1pps_msg_fpga_loadreq {
  __M1PPS_MSG_FPGA_CHIPREQ;
  uint32_t offset;
  uint32_t size;
} m1pps_msg_fpga_loadreq_t;

typedef m1pps_msg_fpga_chip_sts_rsp_t m1pps_msg_fpga_loadrsp_t;

typedef m1pps_msg_fpga_chip_sts_rsp_t m1pps_msg_fpga_loadcmpl_t;

typedef m1pps_msg_fpga_chipreq_t m1pps_msg_fpga_startload_t;

typedef struct m1pps_msg_fpga_rxreq {
  __M1PPS_MSG_FPGA_CHIPREQ;
} m1pps_msg_fpga_rxreq_t;

typedef struct m1pps_msg_fpga_rxrsp {
  __M1PPS_MSG_FPGA_CHIPRSP;
  uint16_t size;
  uint8_t data[0];
} m1pps_msg_fpga_rxrsp_t;

typedef struct m1pps_msg_fpga_txreq {
  __M1PPS_MSG_FPGA_CHIPREQ;
  uint16_t size;
  uint8_t data[0];
} m1pps_msg_fpga_txreq_t;

typedef m1pps_msg_fpga_chiprsp_t m1pps_msg_fpga_txrsp_t;



