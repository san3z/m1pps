/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_master.h"
#include "m1pps_msg_sync.h"

#define M1PPS_MASTER_T0 (0)
#define M1PPS_MASTER_TIMER_BEFORE_SYNC      (250)
#define M1PPS_MASTER_TIMER_BEFORE_DLREQ     (100)
#define M1PPS_MASTER_TIMER_WAIT_SENDED      (100)
#define M1PPS_MASTER_TIMER_RESTART          (1000)

#define M1PPS_MASTER_MIN_CIRCLES_FOR_SYNC   (3)

int m1pps_master_process_msg( m1pps_fsm_t *fsm, int msg_id,
                              const void *msg_data, size_t msg_data_sz );

int m1pps_master_init( m1pps_master_t *master, const m1pps_fsm_ticks_t *ticks, m1pps_clk_t *clk )
{
  m1pps_fsm_ops_t ops={0};
  ops.process_msg = m1pps_master_process_msg;
  m1pps_fsm_init( __fsm(master), ticks, 512, 1, 2, &ops);
  memset(&master->cmn.info, 0, sizeof(master->cmn.info));
  memset(&master->cmn.stat, 0, sizeof(master->cmn.stat));
  memset(&master->info, 0, sizeof(master->info));
  memset(&master->stat, 0, sizeof(master->stat));
  master->clk = clk;
  return 0;
}

void m1pps_master_destroy( m1pps_master_t *master )
{
   m1pps_fsm_destroy( __fsm(master) );
}

int m1pps_master_send_sync( m1pps_master_t *master )
{
  int r = 0;
  m1pps_msg_sync_sync_t *msg = __msg_buf( master );
  msg->t_high = 0; // around of 300 years we may not touch this :-)
  msg->t_low = m1pps_clk_get_time(master->clk);
  r = __send_msg( master , M1PPS_MASTER_MSG_SYNC, __msg_buf(master), sizeof(*msg));
  return r;
}

int m1pps_master_process_sync_sended( m1pps_master_t *master, const void *data, size_t data_sz )
{
   const m1pps_msg_sync_sended_t *m = data;
   if ( sizeof(*m) != data_sz )
     return -EINVAL;
   master->cmn.stat.cyrcles++;
   master->t1 = m->tx_ts;
   return 0;
}

int m1pps_master_send_flup( m1pps_master_t *master )
{
  int err;
  m1pps_msg_sync_flup_t *msg = __msg_buf( master );
  msg->t1 = master->t1;
  err = __send_msg( master, M1PPS_MASTER_MSG_FLUP, __msg_buf(master), sizeof(*msg));
  return err;
}

int m1pps_master_process_dlreq( m1pps_master_t *master, const void *data, size_t data_sz )
{
   const m1pps_msg_sync_dlreq_t *m = data;
   char t_str[T_SZ];
   if ( sizeof(*m) != data_sz )
     return -EINVAL;
   master->t4 = m->rx_ts;
   time2s(master->t4, t_str, sizeof(t_str));
   DBG("T4: %s", t_str);
   return 0;
}

int m1pps_master_send_dlresp( m1pps_master_t *master )
{
  m1pps_msg_sync_dlresp_t *msg = __msg_buf( master );
  msg->t4 =  master->t4;
  return __send_msg( master, M1PPS_MASTER_MSG_DLRESP, __msg_buf(master), sizeof(*msg));
}

int m1pps_master_send_infreq( m1pps_master_t *master )
{
  m1pps_msg_sync_infreq_t *msg = __msg_buf(master);
  return __send_msg(master, M1PPS_MASTER_MSG_INFREQ, __msg_buf(master), sizeof(*msg));
}

void m1pps_master_process_end( m1pps_master_t *master )
{
  if (master->cyrcles < UINT16_MAX)
     master->cyrcles++;
  master->cmn.info.proto_sync = ( master->cyrcles >= M1PPS_MASTER_MIN_CIRCLES_FOR_SYNC ) ? 1 : 0;

  master->cmn.stat.good_cyrcles++;
}

int m1pps_master_process_infrsp(m1pps_master_t *master, const void *data, size_t data_sz)
{
   const m1pps_msg_sync_infrsp_t *m = data;
   if (sizeof(*m) != data_sz)
     return -EINVAL;
   master->cmn.info.offset = m->offset;
   master->cmn.info.delay = m->delay;
   master->slv_info.pll_flags = m->pll_flags;
   master->slv_info.pll_phlock_dur_ms = m->pll_phlock_dur_ms;
   master->slv_info.pll_frlock_dur_ms = m->pll_frlock_dur_ms;
   master->slv_stat.time_adjust_cnt = m->time_adjust_cnt;
   master->slv_stat.period_adjust_cnt = m->period_adjust_cnt;
   return 0;
}

void m1pps_master_get_cmn_info( const m1pps_master_t *master, m1pps_sync_cmn_info_t *info )
{
  (*info) = master->cmn.info;
}

void m1pps_master_get_cmn_stat( const m1pps_master_t *master, m1pps_sync_cmn_stat_t *stat )
{
  (*stat) = master->cmn.stat;
}

void m1pps_master_get_info( const m1pps_master_t *master, m1pps_sync_master_info_t *info )
{
  (*info) = master->info;
}

void m1pps_master_get_stat( const m1pps_master_t *master, m1pps_sync_master_stat_t *stat )
{
  (*stat) = master->stat;
}

void m1pps_master_get_slave_info( const m1pps_master_t *master, m1pps_sync_master_slave_info_t *info )
{
  (*info) = master->slv_info;
}

void m1pps_master_get_slave_stat( const m1pps_master_t *master, m1pps_sync_master_slave_stat_t *stat )
{
  (*stat) = master->slv_stat;
}

m1pps_master_st_t m1pps_master_process_err( m1pps_master_t *master, int err )
{
  master->cyrcles = 0;
  master->cmn.info.proto_sync = 0;
  __start_timer( master,  M1PPS_MASTER_T0, M1PPS_MASTER_TIMER_RESTART );
  return M1PPS_MASTER_ST_RESTARTING;
}

m1pps_master_st_t m1pps_master_process_timeout( m1pps_master_t *master, const char *timeout_msg )
{
   master->cmn.stat.err_timeout++;
   ERR("%s", timeout_msg);
   return m1pps_master_process_err( master, -ETIME );
}

int m1pps_master_process_msg( m1pps_fsm_t *fsm, int ev, const void *data, size_t data_sz )
{
  int err;
  m1pps_master_t *master = container_of( fsm, m1pps_master_t, fsm);
  m1pps_master_st_t ns, s;
  s = ns = __state(master);

  switch ( s ) {
    case M1PPS_MASTER_ST_INITIAL:
      if ( ev != M1PPS_MASTER_EV_START )
         break;
      if ((err = m1pps_master_send_sync( master ))) {
         ns = m1pps_master_process_err(master, err );
         break;
      }
      __start_timer( master, M1PPS_MASTER_T0, M1PPS_MASTER_TIMER_WAIT_SENDED );
      ns = M1PPS_MASTER_ST_WAIT_SYNC_SENDED;
    break;
    case M1PPS_MASTER_ST_WAIT_SYNC_SENDED:

      if ( ev == M1PPS_MASTER_EV_STOP ) {
        ns = M1PPS_MASTER_ST_INITIAL;
        break;
      }
      if ( ev == M1PPS_MASTER_EV_TIMER ) {
        if ( ! __timer( master, M1PPS_MASTER_T0 ) ) {
          ns = m1pps_master_process_timeout( master, "Timeout waiting for SYNC message sended");
          break;
        }
      }

      if ( ev != M1PPS_MASTER_EV_SENDED )
        break;

      if (m1pps_master_process_sync_sended( master, data, data_sz ))
        break;

      if ((err = m1pps_master_send_flup( master ))) {
        ns = m1pps_master_process_err(master, err );
        break;
      }
      __start_timer( master, M1PPS_MASTER_T0,
                     M1PPS_MASTER_TIMER_BEFORE_DLREQ+M1PPS_MASTER_TIMER_WAIT_SENDED );
      ns = M1PPS_MASTER_ST_FLUP_SENDED;
    break;
    case M1PPS_MASTER_ST_FLUP_SENDED:
      if ( ev == M1PPS_MASTER_EV_STOP ) {
        ns = M1PPS_MASTER_ST_INITIAL;
        break;
      }
      if ( ev == M1PPS_MASTER_EV_TIMER ) {
        if ( ! __timer( master, M1PPS_MASTER_T0 ) ) {
          ns = m1pps_master_process_timeout( master, "Timeout waiting for FLUP sended message");
        }
        break;
      }

      if ( ev != M1PPS_MASTER_EV_DLREQ )
        break;

      __stop_timer(master, M1PPS_MASTER_T0);

      if (m1pps_master_process_dlreq( master, data, data_sz ))
        break;

      if ((err = m1pps_master_send_dlresp( master ))) {
        ns = m1pps_master_process_err(master, err );
        break;
      }

      m1pps_master_process_end( master );

      __start_timer(master, M1PPS_MASTER_T0, M1PPS_MASTER_TIMER_WAIT_SENDED);
      ns = M1PPS_MASTER_ST_WAIT_DLRESP_SENDED;

    break;
    case M1PPS_MASTER_ST_WAIT_DLRESP_SENDED:

      if ( ev == M1PPS_MASTER_EV_STOP ) {
        ns = M1PPS_MASTER_ST_INITIAL;
        break;
      }

      if ( ev == M1PPS_MASTER_EV_TIMER ) {
        if ( ! __timer( master, M1PPS_MASTER_T0 ) ) {
          ns = m1pps_master_process_timeout( master, "Timeout waiting for INFREQ message sended");
          break;
        }
      }

      if ( ev != M1PPS_MASTER_EV_SENDED )
        break;

      __stop_timer(master, M1PPS_MASTER_T0);

      m1pps_master_send_infreq(master);

      __start_timer( master, M1PPS_MASTER_T0, M1PPS_MASTER_TIMER_BEFORE_SYNC);
      ns = M1PPS_MASTER_ST_RESTARTING;

    break;
    case M1PPS_MASTER_ST_RESTARTING:

      if ( ev == M1PPS_MASTER_EV_STOP ) {
        ns = M1PPS_MASTER_ST_INITIAL;
        break;
      }

      if (ev == M1PPS_MASTER_EV_INFRSP) {
        m1pps_master_process_infrsp(master, data, data_sz);
        break;
      }

      if (ev != M1PPS_MASTER_EV_TIMER)
        break;

      if ( __timer( master, M1PPS_MASTER_T0 ) )
        break;

      if ((err = m1pps_master_send_sync( master ))) {
        __start_timer( master, M1PPS_MASTER_T0, M1PPS_MASTER_TIMER_RESTART );
        break;
      }

      __start_timer( master, M1PPS_MASTER_T0, M1PPS_MASTER_TIMER_WAIT_SENDED );
      ns = M1PPS_MASTER_ST_WAIT_SYNC_SENDED;

    break;
  }

  return ns;
}
