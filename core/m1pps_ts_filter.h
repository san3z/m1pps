/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

/*
 * This file implement timestamp filter for N types of timestamps
 *
 * for each type:
 *   1 caculate min/max value of timestamps
 *   2 define intervals:  0: min_ts .. (max_ts-min_ts)/int_cnt*1
 *                        1: (max_ts-min_ts)/int_cnt*1 .. (max_ts-min_ts)/int_cnt*2
 *                        ...
 *                int_cnt-1: (max_ts-min_ts)/int_cnt*(int_cnt-2) .. (max_ts-min_ts)/int_cnt*(int_cnt-1)
 *   3 calc count of ts corespondding for each interval
 *   4 find interval with maximum count
 *   5 calc average value for values of this interval
 */

#include "m1pps_sys.h"

typedef union {
  m1pps_time_t time;
  m1pps_tofs_t tofs;
} ts_t;

typedef struct m1pps_ts_int {
  uint16_t cnt;
  ts_t     avg;
} m1pps_ts_int_t;

typedef struct m1pps_ts_type {
  uint16_t widx;                // index of ts for write
  uint16_t ridx;                // index of ts for read
  uint8_t  last_interval_idx;   // last index of interval with max count
  uint8_t  sign;
  uint16_t range_unchanged_cnt;
  union {
    m1pps_time_t max_ts;          // max ts for this type
    m1pps_tofs_t max_tofs;
  };
  union {
    m1pps_time_t min_ts;          // min ts for this type
    m1pps_tofs_t min_tofs;
  };
  uint8_t   max_int_idx;        // index of interval with max count
  uint16_t  max_int_cnt;        // max count of interval
} m1pps_ts_type_t;

typedef struct m1pps_ts_filter {
  uint8_t  int_cnt;                 // timestamp values intervals count
  uint8_t  type_cnt;                // timestamp types count
  uint16_t ts_cnt;                  // timestamp values count
  m1pps_ts_int_t  *intervals;
  m1pps_ts_type_t *types;
  union {
    m1pps_time_t    *ts;
    m1pps_tofs_t    *tofs;
  };
} m1pps_ts_filter_t;

int  m1pps_ts_filter_init( m1pps_ts_filter_t *f, uint8_t ts_type_cnt, uint16_t ts_cnt, uint8_t int_cnt );
void m1pps_ts_filter_destroy( m1pps_ts_filter_t *f );

void m1pps_ts_filter_set_type_sign( m1pps_ts_filter_t *f, uint8_t ts_type, uint8_t sign );
void m1pps_ts_filter_clear( m1pps_ts_filter_t *f);
void m1pps_ts_filter_clear_values(m1pps_ts_filter_t *f);

uint16_t m1pps_ts_filter_ts_count( m1pps_ts_filter_t *f, uint8_t ts_type );

void m1pps_ts_filter_add_ts( m1pps_ts_filter_t *f, uint8_t ts_type, m1pps_time_t ts);
void m1pps_ts_filter_add_tofs( m1pps_ts_filter_t *f, uint8_t ts_type,  const m1pps_tofs_t tofs);
void m1pps_ts_filter_calc( m1pps_ts_filter_t *f );
void m1pps_ts_filter_get_avg_ts( m1pps_ts_filter_t *f, uint8_t ts_type, m1pps_time_t *ts );
void m1pps_ts_filter_get_avg_tofs( m1pps_ts_filter_t *f, uint8_t ts_type, m1pps_tofs_t *tofs );
uint16_t m1pps_ts_filter_get_range_unchanged_cnt( m1pps_ts_filter_t *f, uint8_t ts_type );

void  m1pps_ts_filter_add_all_ts( m1pps_ts_filter_t *f, const m1pps_time_t *ts);
void  m1pps_ts_filter_get_all_avg_ts_( m1pps_ts_filter_t *f, m1pps_time_t *ts);
