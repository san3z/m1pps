/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

/*----------------------------------------------------------------------*/

#ifdef __KERNEL__

#include <linux/version.h>
#include <linux/types.h>
#include <linux/slab.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,10,0)
 #define ktime_t u64
#endif

typedef ktime_t m1pps_time_t;
typedef int64_t m1pps_tofs_t;
#define PRIts "llu"
#define PRIto "lli"

#define PRIu64 "llu"
#define PRIi64 "lli"

#define PRIsz "zu"
#define PRIu32 "u"
#define UINT32_MAX U32_MAX
#define UINT16_MAX U16_MAX
#define UINT64_MAX U64_MAX
#define INT64_MAX  S64_MAX
#define INT64_MIN  S64_MIN
#define UINT8_MAX  U8_MAX

#define sys_zalloc(size) kzalloc(size, GFP_KERNEL)
#define sys_free(p) kfree(p)

#define ntoh_64(v) __be64_to_cpu(v)
#define hton_64(v) __cpu_to_be64(v)
#define ntoh_32(v) __be32_to_cpu(v)
#define hton_32(v) __cpu_to_be32(v)
#define ntoh_16(v) __be16_to_cpu(v)
#define hton_16(v) __cpu_to_be16(v)

#define DBG(f, ... )  pr_debug( f, ##__VA_ARGS__ )
#define ERR(f, ... )  pr_err( KERN_ERR "ERR: "f, ##__VA_ARGS__ )
#define INF(f, ... )  pr_info( KERN_INFO f, ##__VA_ARGS__ )

#define llabs(v) ((v<0)?-v:v)

#endif // __KERNEL__

/*----------------------------------------------------------------------*/

#ifdef __FX3__

#include "cyu3system.h"
#include "cyu3os.h"
#include <sys/param.h>

typedef uint64_t m1pps_time_t;
typedef int64_t  m1pps_tofs_t;

#define PRIts "llu"

#define PRIsz "u"
#define PRIu32 "u"
#define PRIx32 "x"

#ifndef container_of
#define container_of(ptr, type, member) ({                      \
                const typeof( ((type *)0)->member ) *__mptr = (ptr);    \
                (type *)( (char *)__mptr - offsetof(type,member) );})
#endif

static inline  void *CyU3PMemZAlloc(size_t size)
{
   void *p = CyU3PMemAlloc(size);
   if (p) CyU3PMemSet( p, 0, size );
   return p;
}

#define sys_zalloc(size) CyU3PMemZAlloc(size)
#define sys_free(p) CyU3PMemFree(p)


#if BYTE_ORDER == LITTLE_ENDIAN
uint64_t sys_swap_64(uint64_t v);
#define ntoh_64(v) sys_swap_64(v)
#define hton_64(v) sys_swap_64(v)
uint32_t sys_swap_32(uint32_t v);
#define ntoh_32(v) sys_swap_32(v)
#define hton_32(v) sys_swap_32(v)
uint16_t sys_swap_16(uint16_t v);
#define ntoh_16(v) sys_swap_16(v)
#define hton_16(v) sys_swap_16(v)
#else
#define ntoh_64(v) (v)
#define hton_64(v) (v)
#define ntoh_32(v) (v)
#define hton_32(v) (v)
#define ntoh_16(v) (v)
#define hton_16(v) (v)
#endif

//#define DBG(f, ... ) {  CyU3PDebugPrint (4, "D: %s "f"\r\n", __FUNCTION__,##__VA_ARGS__ ); }
//#define ERR(f, ... ) {  CyU3PDebugPrint (4, "ERR: "f"\r\n", ##__VA_ARGS__ ); }
//#define INF(f, ... ) {  CyU3PDebugPrint (4, f"\r\n", ##__VA_ARGS__ ); }

//#include <tx_api.h>
#define DBG(f, ... ) {  CyU3PDebugPrint (4, "%"PRIu32" D: %s: "f"\r\n", CyU3PGetTime(), __FUNCTION__,##__VA_ARGS__ ); }
#define ERR(f, ... ) {  CyU3PDebugPrint (4, "%"PRIu32" ERR: "f"\r\n", CyU3PGetTime(), ##__VA_ARGS__ ); }
#define INF(f, ... ) {  CyU3PDebugPrint (4, "%"PRIu32" "f"\r\n", CyU3PGetTime(), ##__VA_ARGS__ ); }

#define snprintf(s,sz,fmt,...) ( CyU3PDebugStringPrint( (uint8_t*)s, sz+1, fmt, ##__VA_ARGS__ ), s[sz-1]=0, strlen(s) )
//int snprintf(char *s,size_t sz, const char *fmt,...);
//#define snprintf(s,sz,fmt, ...) __snprintf(s,sz,fmt, ##__VA_ARGS__)
//int __snprintf(char *s,size_t sz, const char *fmt,...);

#define EINVAL (22)
#define ENOMEM (23)
#define EIO    (5)
#define ETIME  (62)

#endif

/*----------------------------------------------------------------------*/

/* disable DBG macros in case of non debug build */
#if !defined(M1PPS_DEBUG) || defined(NODEBUG)
  #undef DBG
  #define DBG(f,...)
#endif

#define LOG(...) INF(##__VA_ARGS__)
#define RX  (0)
#define TX  (1)
#define IN  (0)
#define OUT (1)

void m1pps_sys_init(void);
void m1pps_sys_fi(void);

#define T_SZ (32)

size_t time2s(m1pps_time_t time_ns, char *s, size_t sz);
size_t tofs2s(m1pps_tofs_t time_ns, char *s, size_t sz);

char *__strncpy(char *d, const char *s, size_t sz);
