/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_fsm.h"
#include "m1pps_sys.h"
#include "m1pps_msg.h"

#define M1PPS_SUBSYS_SYNC     (0)

#define M1PPS_MSG_SYNC_SYNC        (FSM_MSG_USER+0)
#define M1PPS_MSG_SYNC_FLUP        (FSM_MSG_USER+1)
#define M1PPS_MSG_SYNC_DLREQ       (FSM_MSG_USER+2)
#define M1PPS_MSG_SYNC_DLRESP      (FSM_MSG_USER+3)
#define M1PPS_MSG_SYNC_SENDED      (FSM_MSG_USER+4)
#define M1PPS_MSG_SYNC_PULSE       (FSM_MSG_USER+5)
#define M1PPS_MSG_SYNC_INFREQ      (FSM_MSG_USER+6)
#define M1PPS_MSG_SYNC_INFRSP      (FSM_MSG_USER+7)

typedef struct m1pps_msg_sync_sync {
  m1pps_time_t rx_ts;
  m1pps_time_t t_high;
  m1pps_time_t t_low;
} m1pps_msg_sync_sync_t;

typedef struct m1pps_msg_sync_flup {
  m1pps_time_t t1;
} m1pps_msg_sync_flup_t;

typedef struct m1pps_msg_sync_dlresp {
  m1pps_time_t t4;
} m1pps_msg_sync_dlresp_t;

typedef struct m1pps_msg_sync_dlreq {
  m1pps_time_t rx_ts;
  m1pps_time_t t2;
} m1pps_msg_sync_dlreq_t;

typedef struct m1pps_msg_sync_sended {
  m1pps_time_t tx_ts;
} m1pps_msg_sync_sended_t;

typedef struct m1pps_msg_sync_pulse {
  uint8_t value;
} m1pps_msg_sync_pulse_t;

typedef struct m1pps_msg_sync_infreq {
} m1pps_msg_sync_infreq_t;

typedef struct m1pps_msg_sync_infrsp {
  m1pps_tofs_t     offset;
  m1pps_time_t     delay;
  uint64_t         time_adjust_cnt;
  uint64_t         period_adjust_cnt;
  uint8_t          pll_flags;
  uint32_t         pll_phlock_dur_ms;
  uint32_t         pll_frlock_dur_ms;
} m1pps_msg_sync_infrsp_t;

#define M1PPS_MSG_SYNC_PLL_PHASELOCK  (0)
#define M1PPS_MSG_SYNC_PLL_FREQLOCK   (1)
