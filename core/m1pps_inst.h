/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_master.h"
#include "m1pps_slave.h"
#include "m1pps_pkt_sync.h"
#include "m1pps_trs.h"
#include "m1pps_clk.h"

#define M1PPS_INST_SUBSYS_MAX_CNT (5)
#define M1PPS_INST_FSM_MAX_CNT    (5)

typedef enum m1pps_inst_role {
  M1PPS_INST_ROLE_MASTER,
  M1PPS_INST_ROLE_SLAVE
} m1pps_inst_role_t;

struct m1pps_inst;

typedef struct m1pps_inst_cbs {
  void *(*mutexes_create)(struct m1pps_inst *, uint8_t mutex_cnt, size_t *mutex_sz);
  void (*mutexes_free)(struct m1pps_inst *, void *mutexes);
  void (*mutex_lock)(struct m1pps_inst *, void *mutex);
  void (*mutex_unlock)(struct m1pps_inst *, void *mutex);
  int  (*send_pulse)(struct m1pps_inst *, uint8_t value);
  int  (*set_mode)(struct m1pps_inst *, uint8_t value, uint8_t start_to_change);
  int  (*read_pll_info)(struct m1pps_inst *, m1pps_sync_slave_pll_info_t *info);
} m1pps_inst_cbs_t;

typedef struct m1pps_inst {

  enum m1pps_inst_role role;

  union {
    m1pps_master_t master;
    m1pps_slave_t  slave;
  };

  m1pps_inst_cbs_t cbs;
  m1pps_fsm_ticks_t ticks;
  m1pps_pkt_t pkt[2];
  m1pps_fsm_t *fsm[M1PPS_INST_FSM_MAX_CNT];
  m1pps_trs_t *trs;
  m1pps_clk_t *clk;

  m1pps_fsm_t *sync_fsm;

  uint8_t fsm_cnt;

  m1pps_pkt_substms_t substms;

  void   *mutexes;
  size_t mutex_sz;

  void *buf[2];
  size_t buf_sz;

  void **ctx;

  uint8_t sendpulse_mode:1;

} m1pps_inst_t;

typedef struct m1pps_inst_prm {
  enum m1pps_inst_role role;
  uint32_t timer_period_us;
  size_t max_pkt_sz;
  uint8_t ctx_cnt;
} m1pps_inst_prm_t;

int m1pps_inst_init(m1pps_inst_t *inst, const m1pps_inst_prm_t *prm, const m1pps_inst_cbs_t *cbs,
                    m1pps_clk_t *clk );
void m1pps_inst_destroy(m1pps_inst_t *inst);


void m1pps_inst_tick( m1pps_inst_t *inst );
int  m1pps_inst_process_msg( m1pps_inst_t *inst, int subsys_id, int msg_id, const void *data, size_t data_sz );

void m1pps_inst_set_ctx( const m1pps_inst_t *inst, uint8_t ctx_idx, void *ctx );
void *m1pps_inst_get_ctx( const m1pps_inst_t *inst, uint8_t ctx_idx );

int m1pps_inst_get_sync_info( const m1pps_inst_t *inst, m1pps_sync_cmn_info_t *info );
int m1pps_inst_get_sync_stat( const m1pps_inst_t *inst, m1pps_sync_cmn_stat_t *stat );

int m1pps_inst_get_trs_stat( const m1pps_inst_t *inst, m1pps_trs_stat_t *stat );
m1pps_inst_role_t m1pps_inst_get_role( const m1pps_inst_t *inst );

uint8_t m1pps_inst_get_sync_mode( const m1pps_inst_t *inst );

int m1pps_inst_set_trs( m1pps_inst_t *inst, m1pps_trs_t *trs );
size_t m1pps_inst_get_max_pkt_sz( const m1pps_inst_t *inst );

