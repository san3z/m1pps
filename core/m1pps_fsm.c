/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_fsm.h"

void m1pps_fsm_ticks_init(m1pps_fsm_ticks_t *ticks, uint16_t tps)
{
  ticks->ticks_cnt = 0;
  ticks->ticks_per_second = tps;
  DBG("tps:%u", tps);
}

uint16_t m1pps_fsm_ticks_get_tps(const m1pps_fsm_ticks_t *ticks)
{
  return ticks->ticks_per_second;
}

uint64_t m1pps_fsm_ticks_ms_to_ticks( const m1pps_fsm_ticks_t *ticks, uint64_t time_ms)
{
  return (uint64_t)m1pps_fsm_ticks_get_tps(ticks)*(uint64_t)time_ms/1000;
}

void m1pps_fsm_ticks_inc(m1pps_fsm_ticks_t *ticks)
{
  if (ticks->ticks_per_second <= ticks->ticks_cnt)
    ticks->ticks_cnt -= ticks->ticks_per_second;
  ticks->ticks_cnt++;
}

uint8_t m1pps_fsm_timer_u32ms_dec(m1pps_fsm_timer_t *timer, const m1pps_fsm_ticks_t *ticks)
{
  if ( ! timer->value )
    return 0;

  uint16_t ticks_cnt;
  if ( ticks->ticks_cnt >= timer->start_ticks_cnt ) {
    ticks_cnt = ticks->ticks_cnt - timer->start_ticks_cnt;
  } else {
    ticks_cnt = ticks->ticks_per_second-ticks->ticks_cnt;
  }

  if ( ( (ticks_cnt*FSM_MIN_TICKS) % ticks->ticks_per_second ) < FSM_MIN_TICKS ) {
    if ( timer->value >= FSM_TIMER_INC_MS)
      timer->value -= FSM_TIMER_INC_MS;
    else
      timer->value = 0;
  }
  //DBG("tics*min/tps:%"PRIu32, (uint32_t)((ticks->ticks_cnt*FSM_MIN_TICKS) % ticks->ticks_per_second) );
  //DBG("ticks:%"PRIu32":%"PRIu32" t:%"PRIu32, ticks->ticks_cnt, ticks_cnt,  timer->value);

  return ( timer->value ) ? 0 : 1;
}

/* fsm */

int m1pps_fsm_init( m1pps_fsm_t *fsm, const m1pps_fsm_ticks_t *ticks, size_t buf_sz, uint8_t timer_cnt,
                    uint8_t ctx_cnt, const m1pps_fsm_ops_t *ops )
{
  int err=0;
  DBG("buf_sz:%"PRIsz" timer_cnt:%d ctx_cnt:%d",buf_sz, timer_cnt, ctx_cnt);
  fsm->timers=0;
  if (timer_cnt) {
    if (! (fsm->timers=sys_zalloc(sizeof(fsm->timers[0])*timer_cnt))) {
      err=-ENOMEM;
      goto exit;
    }
  }
  if (buf_sz) {
    if ( !(fsm->msg_buf=sys_zalloc(buf_sz)) ) {
      err= -ENOMEM;
      goto exit;
    }
  } else
    fsm->msg_buf=0;

  if (ctx_cnt) {
    if ( !(fsm->ctx=sys_zalloc(sizeof(void*)*ctx_cnt)) ) {
      err= -ENOMEM;
      goto exit;
    }
  }

  fsm->msg_buf_sz=buf_sz;
  fsm->ops = *ops;
  fsm->timer_cnt = timer_cnt;
  fsm->ticks = ticks;

exit:

  if (err)
    m1pps_fsm_destroy(fsm);

  return err;
}

void m1pps_fsm_destroy( m1pps_fsm_t *fsm )
{
  if ( fsm->msg_buf ) {
    sys_free( fsm->msg_buf );
    fsm->msg_buf = 0;
  }
  if ( fsm->timers ) {
    sys_free( fsm->timers );
    fsm->timers = 0;
  }
  if ( fsm->ctx ) {
    sys_free( fsm->ctx );
    fsm->ctx = 0;
  }
}

int m1pps_fsm_process_msg( m1pps_fsm_t *fsm, int msg_id, const void *data, size_t data_sz )
{
  int new_st, old_st = fsm->state;

  DBG("state:%d msg_id:%d dsz:%"PRIsz, old_st, msg_id, data_sz);

  if (fsm->ops.process_msg)
    new_st = fsm->ops.process_msg( fsm, msg_id, data, data_sz );
  else
    new_st = old_st;

  if (old_st != new_st) {
    fsm->state = new_st;
    DBG("state %d -> %d ", old_st, new_st);
    if ( fsm->ops.state_changed )
      fsm->ops.state_changed( fsm, msg_id, old_st, new_st );
  }

  return 0;
}

void m1pps_fsm_tick( m1pps_fsm_t *fsm )
{
  uint8_t t, expired=0;

  for (t=0; t<fsm->timer_cnt; ++t) {
    if ( m1pps_fsm_timer_u32ms_dec( fsm->timers + t, fsm->ticks ) ) {
      DBG("timer:%u expired", t);
      expired = 1;
    }
  }

  if ( expired )
     m1pps_fsm_process_msg( fsm, FSM_MSG_TIMER, 0, 0);
}

void m1pps_fsm_restart_timer( m1pps_fsm_t *fsm, uint8_t timer_id, uint32_t value )
{
  if ( timer_id >= fsm->timer_cnt )
    return;

  fsm->timers[ timer_id ].start_ticks_cnt = fsm->ticks->ticks_cnt;
  fsm->timers[ timer_id ].value = value;
  DBG("timer:%u started:%"PRIu32, timer_id, value);
}

void m1pps_fsm_start_timer( m1pps_fsm_t *fsm, uint8_t timer_id, uint32_t value )
{
  if ( timer_id >= fsm->timer_cnt )
    return;

  if ( fsm->timers[ timer_id ].value )
    return;

  fsm->timers[ timer_id ].start_ticks_cnt = fsm->ticks->ticks_cnt;
  fsm->timers[ timer_id ].value = value;
  DBG("timer:%u started:%"PRIu32, timer_id, value);
}

void m1pps_fsm_stop_timer( m1pps_fsm_t *fsm, uint8_t timer_id )
{
  if ( timer_id >= fsm->timer_cnt )
    return;
  fsm->timers[ timer_id ].value = 0;
  DBG("timer:%u stopped", timer_id);
}

uint32_t m1pps_fsm_get_timer( const m1pps_fsm_t *fsm, uint8_t timer_id )
{
  if ( timer_id >= fsm->timer_cnt )
    return 0;
  return fsm->timers[ timer_id ].value;
}

inline int  m1pps_fsm_get_state( const m1pps_fsm_t *fsm )
{
  return fsm->state;
}

inline void  *m1pps_fsm_get_msg_buf( const m1pps_fsm_t *fsm )
{
  return fsm->msg_buf;
}

inline size_t m1pps_fsm_get_msg_buf_sz( const m1pps_fsm_t *fsm )
{
  return fsm->msg_buf_sz;
}

int m1pps_fsm_send_msg( m1pps_fsm_t *fsm, int msg_id, void *msg_data, size_t msg_data_sz )
{
  if ( ! fsm->trs_ops.send_msg )
     return -EINVAL;

  DBG("msg:%d dsz:%"PRIsz, msg_id, msg_data_sz);

  return fsm->trs_ops.send_msg( fsm, msg_id, msg_data, msg_data_sz );
}

void m1pps_fsm_set_trs( m1pps_fsm_t *fsm, const m1pps_fsm_trs_ops_t *trs_ops )
{
  fsm->trs_ops = *trs_ops;
}

void m1pps_fsm_set_ctx( const m1pps_fsm_t *fsm, uint8_t ctx_idx, void *ctx )
{
  fsm->ctx[ctx_idx] = ctx;
}

void *m1pps_fsm_get_ctx( const m1pps_fsm_t *fsm, uint8_t ctx_idx)
{
  return fsm->ctx[ctx_idx];
}

