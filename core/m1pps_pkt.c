/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_pkt.h"
#include "m1pps_sys.h"


#pragma pack(push, 1)
typedef struct m1pps_pkthdr {
  uint16_t len;
  uint8_t  subsys;
  uint8_t  id;
  uint8_t  ver;
  uint16_t csum;
} m1pps_pkthdr_t;

#pragma pack(pop)

#define __max(v1, v2) ((v1 < v2) ? v2 : v1)

int  m1pps_pkt_substms_init(m1pps_pkt_substms_t *substms, uint8_t max_subsys_cnt )
{
  substms->subsys = sys_zalloc(sizeof(substms->subsys[0])+(sizeof(substms->ctx[0]))* max_subsys_cnt);
  if ( !substms->subsys )
    return -ENOMEM;
  substms->ctx = (void **)(substms->subsys + max_subsys_cnt);
  substms->max_subsys_cnt = max_subsys_cnt;
  substms->subsys_cnt = 0;
  return 0;
}

void m1pps_pkt_substms_destroy(m1pps_pkt_substms_t *substms)
{
  sys_free( substms->subsys );
}

int m1pps_pkt_substms_register_subsys(m1pps_pkt_substms_t *substms, const m1pps_pkt_subsys_t *subsys)
{
  if ( subsys->id >= substms->max_subsys_cnt )
    return -EINVAL;
  if ( substms->subsys[subsys->id] )
    return -EINVAL;
  if ( subsys->id >= substms->subsys_cnt )
    substms->subsys_cnt = subsys->id+1;
  substms->subsys[subsys->id] = subsys;
  return 0;
}

const m1pps_pkt_subsys_t *m1pps_pkt_substms_get_subsys(m1pps_pkt_substms_t *substms, uint8_t subsys_id)
{
    return substms->subsys[subsys_id];
}

void *m1pps_pkt_substms_get_subsys_ctx(const m1pps_pkt_substms_t *substms, uint8_t subsys_id)
{
   if ( subsys_id >= substms->subsys_cnt )
      return 0;
   return substms->ctx[subsys_id];
}

inline
void m1pps_pkt_substms_set_subsys_ctx(m1pps_pkt_substms_t *substms, uint8_t subsys_id, void *ctx)
{
   substms->ctx[subsys_id] = ctx;
}

inline
uint8_t m1pps_pkt_substms_get_count(m1pps_pkt_substms_t *substms)
{
   return substms->subsys_cnt;
}

size_t m1pps_pkt_substms_get_max_pkt_sz(const m1pps_pkt_substms_t *substms)
{
  uint8_t i;
  size_t sz = 0;
  for (i=0; i<substms->subsys_cnt;++i) {
    const m1pps_pkt_subsys_t *subsys = substms->subsys[i];
    if ( subsys && subsys->get_max_pkt_sz )
      sz = __max(sz, subsys->get_max_pkt_sz());
  }
  return sz+sizeof(m1pps_pkthdr_t);
}


int m1pps_pkt_init(m1pps_pkt_t *pkt, m1pps_pkt_substms_t *substms,  size_t buf_sz)
{
  pkt->buf = sys_zalloc(buf_sz);
  if (!pkt->buf) {
    return -ENOMEM;
  }
  pkt->buf_sz = buf_sz;
  DBG("bsz:%"PRIsz, pkt->buf_sz);
  pkt->substms = substms;
  return 0;
}

void m1pps_pkt_destroy(m1pps_pkt_t *pkt)
{
  if (pkt->buf) {
    sys_free(pkt->buf);
    pkt->buf=0;
  }
}

inline void *m1pps_pkt_get_buf(m1pps_pkt_t *pkt)
{
  return pkt->buf;
}

inline size_t m1pps_pkt_get_buf_sz(const m1pps_pkt_t *pkt)
{
  return pkt->buf_sz;
}

uint16_t calc_crc16_ccit(const uint8_t *data, uint16_t data_sz)
{
  uint8_t x;
  uint16_t crc = 0xFFFF;

  while (data_sz--) {
    x = crc >> 8 ^ *data++;
    x ^= x>>4;
    crc = (crc << 8) ^ ((uint16_t)(x << 12)) ^ ((uint16_t)(x<<5)) ^ ((uint16_t)x);
  }
  return crc;
}

size_t m1pps_pkt_pack_msg( m1pps_pkt_t *pkt, int subsys_id, int msg_id, const void *msg_data, size_t msg_data_sz )
{
   m1pps_pkthdr_t *hdr;
   const m1pps_pkt_subsys_t *subsys;
   size_t sz=0;

   DBG("bsz:%"PRIsz" id:%d dsz:%"PRIsz, pkt->buf_sz, msg_id, msg_data_sz);

   if ( (pkt->buf_sz-sz) < sizeof(*hdr) )
     return 0;

   if ( subsys_id > m1pps_pkt_substms_get_count(pkt->substms) )
     return 0;

   // pack message header
   hdr = pkt->buf;
   hdr->subsys = subsys_id;
   hdr->id = (uint8_t)msg_id;

   sz += sizeof(*hdr);

   subsys = m1pps_pkt_substms_get_subsys(pkt->substms, subsys_id);

   if (!subsys)
     return 0;

   // pack message body
   hdr->len = subsys->pack_msg(pkt, sizeof(*hdr), msg_id, msg_data, msg_data_sz, &hdr->id );
   sz += hdr->len;
   hdr->len = hton_16(hdr->len);

   // finalize message header
   hdr->csum = 0;
   hdr->csum = hton_16(calc_crc16_ccit((uint8_t*)hdr, sz));

   return sz;
}

static
int _m1pps_pkt_check_hdr( const m1pps_pkthdr_t *hdr, size_t pkt_data_sz )
{
   size_t len=0;

   if ( pkt_data_sz < sizeof(*hdr) )
     return -EINVAL;

   len = ntoh_16(hdr->len);
   if ( len != (pkt_data_sz-sizeof(*hdr)) ) {
     ERR("len %"PRIsz" expected %"PRIsz, len, pkt_data_sz-sizeof(*hdr) );
     return -EINVAL;
   }

   {
     m1pps_pkthdr_t *h = (m1pps_pkthdr_t *)hdr;
     uint16_t csum = hdr->csum;
     h->csum = 0;
     if ( calc_crc16_ccit((uint8_t*)h, pkt_data_sz) != ntoh_16(csum) ) {
       ERR("csum %x != %x", calc_crc16_ccit((uint8_t*)h, pkt_data_sz), ntoh_16(csum) );
       h->csum = csum;
       return -EINVAL;
     }
     h->csum = csum;
   }

   return 0;
}

int m1pps_pkt_unpack_msg( const m1pps_pkt_t *pkt, size_t pkt_data_sz, m1pps_time_t pkt_rx_ts,
                          int *subsys_id, int *msg_id, void *msg_data, size_t *msg_data_sz  )
{
  const m1pps_pkt_subsys_t *subsys;
  const m1pps_pkthdr_t *hdr = pkt->buf;
  m1pps_pkt_up_args_t up_args;

  if (pkt_data_sz > m1pps_pkt_get_buf_sz(pkt)) {
    ERR("Packet size too long %"PRIsz" > %"PRIsz,
         pkt_data_sz, m1pps_pkt_get_buf_sz(pkt));
    return -EINVAL;
  }

  /* check & unpack message header from line format */
  hdr = pkt->buf;
  if ( _m1pps_pkt_check_hdr( hdr, pkt_data_sz ) ) {
    ERR("Wrong packet with size %"PRIsz, pkt_data_sz);
    return -EINVAL;
  }

  if (hdr->subsys >= m1pps_pkt_substms_get_count(pkt->substms)) {
    ERR("Wrong packet subsys %u", hdr->subsys);
    return -EINVAL;
  }

  /* unpack message from line format */
  subsys = m1pps_pkt_substms_get_subsys(pkt->substms, hdr->subsys);

  if ( !subsys ) {
    ERR("Not registered packet subsys %u", hdr->subsys);
    return -EINVAL;
  }

  *subsys_id = hdr->subsys;
  *msg_id = hdr->id;

  up_args.body_ofs = sizeof(*hdr);
  up_args.body_sz = pkt_data_sz-sizeof(*hdr);
  up_args.ver = hdr->ver;
  up_args.id = hdr->id;
  up_args.rx_ts = pkt_rx_ts;

  if ( subsys->unpack_msg( pkt, &up_args, msg_id, msg_data, msg_data_sz )) {
    ERR("Can't unpack packet body with size %"PRIsz, pkt_data_sz-sizeof(*hdr));
    return -EINVAL;
  }

  return 0;
}


