/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_fpga_slave.h"
#include "m1pps_fpga.h"

#define M1PPS_FPGA_SLAVE_MAX_LOAD_SZ (65535)

#define M1PPS_FPGA_SLVCHIP_T0 (0)
#define M1PPS_FPGA_SLAVE_TIMER_1K_MS (100)

int m1pps_fpga_slvchip_process_msg( m1pps_fsm_t *fsm, int ev, const void *data, size_t data_sz);

int m1pps_fpga_slvchip_init( m1pps_fpga_slvchip_t *chip, const m1pps_fsm_ticks_t *ticks,
                             uint8_t id, const m1pps_fpga_slvchip_desc_t *chip_desc )
{
  m1pps_fsm_ops_t ops={0};
  ops.process_msg = m1pps_fpga_slvchip_process_msg;
  m1pps_fsm_init( __fsm(chip), ticks, 16, 0, 0, &ops);
  chip->desc = *chip_desc;
  chip->id = id;
  return 0;
}

void m1pps_fpga_slvchip_destroy( m1pps_fpga_slvchip_t *chip )
{
  m1pps_fsm_destroy( __fsm(chip) );
}

/*
void m1pps_fpga_slvchip_set_cbs(m1pps_fpga_slvchip_t *chip, const m1pps_fpga_slvchip_cbs_t *cbs)
{
  chip->cbs = *cbs;
}
*/

int m1pps_fpga_slvchip_send_lrsp(m1pps_fpga_slvchip_t *chip, uint8_t msg_id, int16_t ret_code, uint8_t status)
{
  m1pps_msg_fpga_linitrsp_t *msg = __msg_buf(chip);
  msg->ret_code = ret_code;
  msg->status = status;
  return __send_msg(chip, msg_id, __msg_buf(chip), sizeof(*msg));
}

int m1pps_fpga_slvchip_read_status( m1pps_fpga_slvchip_t *chip, uint8_t *status )
{
  int err=0;
  uint8_t v;
  err=chip->desc.cbs.get_pin(chip, M1PPS_FPGA_PIN_NSTATUS, &v);
  if (err)
     return err;
  if (v)
    (*status) |= (1<<M1PPS_FPGA_PIN_NSTATUS);
  err=chip->desc.cbs.get_pin(chip, M1PPS_FPGA_PIN_CONFDONE, &v);
  if (err)
     return err;
  if (v)
    (*status) |= (1<<M1PPS_FPGA_PIN_CONFDONE);
  return err;
}

int m1pps_fpga_slvchip_process_stsreq( m1pps_fpga_slvchip_t *chip, const void *data, size_t data_sz )
{
  int err=0;
  uint8_t status=0;
  //const m1pps_msg_fpga_stsreq_t *msg = data;

  if ( data_sz != sizeof(m1pps_msg_fpga_stsreq_t) ) {
    err=-EINVAL;
    goto exit;
  }

  if (!chip->desc.cbs.get_pin) {
    err=-EINVAL;
    goto exit;
  }

  err=m1pps_fpga_slvchip_read_status(chip, &status);

exit:

  m1pps_fpga_slvchip_send_lrsp(chip, M1PPS_MSG_FPGA_STSRSP, err, status);

  return err;
}

int m1pps_fpga_slvchip_process_linitreq( m1pps_fpga_slvchip_t *chip, const void *data, size_t data_sz )
{
  int err=0;
  uint8_t status;

  if (data_sz != sizeof(m1pps_msg_fpga_linitreq_t)) {
    err=-EINVAL;
    goto exit;
  }

  chip->load_data = sys_zalloc( M1PPS_FPGA_SLAVE_MAX_LOAD_SZ );
  if (!chip->load_data) {
    err = -ENOMEM;
    goto exit;
  }

  if (!chip->desc.cbs.set_pin) {
    err = -EINVAL;
    goto exit;
  }

  err=chip->desc.cbs.set_pin(chip, M1PPS_FPGA_PIN_NCONFIG, 1);

  if (err)
    goto exit;

  err=m1pps_fpga_slvchip_read_status(chip, &status);

exit:

  m1pps_fpga_slvchip_send_lrsp(chip, M1PPS_MSG_FPGA_LINITRSP, err, status );

  return err;
}

int m1pps_fpga_slvchip_process_ldatareq(m1pps_fpga_slvchip_t *chip, const void *data, size_t data_sz)
{
  int err=0;
  const m1pps_msg_fpga_ldatareq_t *msg = data;
  size_t loaded_sz;

  if (data_sz < sizeof(m1pps_msg_fpga_loadreq_t)) {
    err=-EINVAL;
    goto exit;
  }

  loaded_sz = data_sz - sizeof(m1pps_msg_fpga_ldatareq_t);

  if (loaded_sz+msg->offset > M1PPS_FPGA_SLAVE_MAX_LOAD_SZ) {
    err=-EINVAL;
    goto exit;
  }

  memcpy(chip->load_data+msg->offset, msg->data, loaded_sz);

exit:

  if (err)
    m1pps_fpga_slvchip_send_lrsp(chip, M1PPS_MSG_FPGA_LDATARSP, err, 0);

  return err;
}

int m1pps_fpga_slvchip_process_loadreq(m1pps_fpga_slvchip_t *chip, const void *data, size_t data_sz, uint32_t *time_ms)
{
  int err;
  const m1pps_msg_fpga_loadreq_t *msg = data;

  if (!chip->desc.cbs.start_load) {
    err=-EINVAL;
    goto exit;
  }

  if (msg->offset >= M1PPS_FPGA_SLAVE_MAX_LOAD_SZ) {
    err=-EINVAL;
    goto exit;
  }

  if ((M1PPS_FPGA_SLAVE_MAX_LOAD_SZ-msg->offset) < msg->size) {
    err=-EINVAL;
    goto exit;
  }

  *time_ms = ((msg->size >> 10) +  (msg->size & 0x7FF) ? 1 : 0) * M1PPS_FPGA_SLAVE_TIMER_1K_MS;
  err = chip->desc.cbs.start_load(chip, chip->load_data+msg->offset, msg->size);

exit:

  if (err)
    m1pps_fpga_slvchip_send_lrsp(chip, M1PPS_MSG_FPGA_LOADRSP, err, 0);

  return err;
}

int m1pps_fpga_slvchip_process_loadcmpl( m1pps_fpga_slvchip_t *chip, const void *data, size_t data_sz)
{
  return 0;
}

m1pps_fpga_slvchip_st_t m1pps_fpga_slcchip_process_err( m1pps_fpga_slvchip_t *chip, int err )
{
   //m1pps_slave_reset_sync(slave);
   return M1PPS_FPGA_SLVCHIP_ST_STARTED;
}

m1pps_fpga_slvchip_st_t m1pps_fpga_slvchip_process_timeout( m1pps_fpga_slvchip_t *chip, const char *timeout_msg )
{
   //slave->stat.err_timeout++;
   ERR("%s", timeout_msg);
   return m1pps_fpga_slcchip_process_err(chip, -ETIME );
}

int m1pps_fpga_slvchip_load(m1pps_fpga_slvchip_t *chip, void *fpga_data, size_t fpga_data_sz)
{
   int err=0;
   size_t i;
   uint8_t j, b, *d = fpga_data;
   m1pps_msg_fpga_loadcmpl_t msg={0};

   while (i<fpga_data_sz) {
     b = d[i];
     for (j=0;j<8;++j) {
       err = chip->desc.cbs.set_pin( chip, M1PPS_FPGA_PIN_DATA, (b>>j)&1 );
       if (err)
         goto exit;
       err = chip->desc.cbs.set_pin( chip, M1PPS_FPGA_PIN_DCLK, 1);
       if (err)
         goto exit;
     }
     i++;
   }

   err = m1pps_fpga_slvchip_read_status(chip, &msg.status);

exit:

   // fix-me mutex req
   msg.ret_code = err;
   m1pps_fpga_slvchip_process_msg(__fsm(chip), M1PPS_MSG_FPGA_LOADCMPL, &msg, sizeof(msg) );

   return err;
}

int m1pps_fpga_slvchip_process_msg( m1pps_fsm_t *fsm, int ev, const void *data, size_t data_sz )
{
  int err;
  m1pps_fpga_slvchip_t *chip = container_of( fsm, m1pps_fpga_slvchip_t, fsm);
  m1pps_fpga_slvchip_st_t ns, s;
  s = ns = __state(chip);

  switch (s) {

    case M1PPS_FPGA_SLVCHIP_ST_INITIAL:

      if (ev != M1PPS_FPGA_SLAVE_EV_START)
         break;
      ns = M1PPS_FPGA_SLAVE_ST_STARTED;

    break;

    case M1PPS_FPGA_SLVCHIP_ST_STARTED:

      if (ev == M1PPS_FPGA_SLVCHIP_EV_STOP) {
        ns = M1PPS_FPGA_SLVCHIP_ST_INITIAL;
        break;
      }

      if (ev != M1PPS_FPGA_SLVCHIP_EV_STSREQ) {
        if ((err = m1pps_fpga_slvchip_process_stsreq(chip, data, data_sz))) {
          //ns = m1pps_slave_process_err(chip, err);
          break;
        }
        break;
      }

      if (ev != M1PPS_FPGA_SLVCHIP_EV_LINITREQ)
        break;

      if ((err = m1pps_fpga_slvchip_process_linitreq(chip, data, data_sz))) {
        //ns = m1pps_slave_process_err(chip, err);
        break;
      }

      ns = M1PPS_FPGA_SLVCHIP_ST_LOADING;

    break;
    case M1PPS_FPGA_SLVCHIP_ST_WAIT_DATA:

      if (ev == M1PPS_FPGA_SLVCHIP_EV_STOP) {
        ns = M1PPS_FPGA_SLVCHIP_ST_INITIAL;
        break;
      }

      if (ev == M1PPS_FPGA_SLVCHIP_EV_LDATAREQ) {
        m1pps_fpga_slvchip_process_ldatareq(chip, data, data_sz);
        break;
      }

      if (ev != M1PPS_FPGA_SLVCHIP_EV_LOADREQ )
        break;

      {
        uint32_t timer_ms;
        if ((err = m1pps_fpga_slvchip_process_loadreq(chip, data, data_sz, &timer_ms))) {
          break;
        }
        __start_timer(chip, M1PPS_FPGA_SLVCHIP_T0, timer_ms);
      }
      ns = M1PPS_FPGA_SLVCHIP_ST_LOADING;

    break;
    case M1PPS_FPGA_SLVCHIP_ST_LOADING:

      if (ev == M1PPS_FPGA_SLVCHIP_EV_STOP) {
        ns = M1PPS_FPGA_SLVCHIP_ST_INITIAL;
        break;
      }

      if (ev == M1PPS_FPGA_SLVCHIP_EV_TIMER) {
        if (!__timer(chip, M1PPS_FPGA_SLVCHIP_T0 )) {
           ns = m1pps_fpga_slvchip_process_timeout(chip,
                                                   "waiting for fpga load data complete");
           break;
        }
      }

      if (ev != M1PPS_FPGA_SLVCHIP_EV_LOADCMPL)
        break;

      __stop_timer(chip, M1PPS_FPGA_SLVCHIP_T0);

      if ((err = m1pps_fpga_slvchip_process_loadcmpl(chip, data, data_sz))) {
        //ns = m1pps_slave_process_err(chip, err);
        break;
      }

      ns = M1PPS_FPGA_SLVCHIP_ST_WORKED;

    break;
    case M1PPS_FPGA_SLVCHIP_ST_WORKED:

      if (ev == M1PPS_FPGA_SLVCHIP_EV_STOP) {
        ns = M1PPS_FPGA_SLVCHIP_ST_INITIAL;
        break;
      }

      if (ev != M1PPS_FPGA_SLVCHIP_EV_STSREQ)
        break;

      m1pps_fpga_slvchip_process_stsreq(chip, data, data_sz);

    break;

  }

  return ns;
}

/* container */


int m1pps_fpga_slave_process_msg( m1pps_fsm_t *fsm, int ev,
                             const void *data, size_t data_sz );

int m1pps_fpga_slave_init( m1pps_fpga_slave_t *slave, const m1pps_fsm_ticks_t *ticks,
                           m1pps_fpga_slvchip_desc_t *chip_desc, uint8_t count)
{
  uint8_t i;
  m1pps_fsm_ops_t ops={0};
  ops.process_msg = m1pps_fpga_slave_process_msg;

  m1pps_fsm_init(__fsm(slave), ticks,
                 M1PPS_FPGA_SLAVE_MAX_LOAD_SZ+sizeof(m1pps_msg_fpga_ldatareq_t),
                 0, 0, &ops);

  slave->chip_cnt = count;
  slave->chip = sys_zalloc(sizeof(m1pps_fpga_slvchip_t)*slave->chip_cnt);

  for (i=0; i<slave->chip_cnt; ++i)
    m1pps_fpga_slvchip_init( slave->chip + i, ticks, i, chip_desc + i);

  return 0;
}


void m1pps_fpga_slave_destroy( m1pps_fpga_slave_t *slave )
{
  uint8_t i;
  for (i=0; i<slave->chip_cnt; ++i)
    m1pps_fpga_slvchip_destroy( slave->chip + i);
  m1pps_fsm_destroy( __fsm(slave) );
}
/*
void m1pps_fpga_slave_set_cbs( m1pps_fpga_slave_t *slave, const m1pps_fpga_slave_cbs_t *cbs )
{
  slave->cbs = *cbs;
}

void m1pps_fpga_slave_get_info( const m1pps_fpga_slave_t *slave, m1pps_info_t *info )
{
  (*info) = slave->info;
}

void m1pps_fpga_slave_get_stat( const m1pps_fpga_slave_t *slave, m1pps_stat_t *stat )
{
  (*stat) = slave->stat;
}
*/

uint8_t m1pps_fpga_slave_get_cnt( const m1pps_fpga_slave_t *slave)
{
  return slave->chip_cnt;
}

m1pps_fpga_slvchip_t *m1pps_fpga_slave_get_chip( const m1pps_fpga_slave_t *slave, uint8_t chip_idx)
{
  return slave->chip + chip_idx;
}

int m1pps_fpga_slave_process_allchip_ev(m1pps_fpga_slave_t *slave, int ev, const void *data, size_t data_sz )
{
  uint8_t i;
  for (i=0; i<slave->chip_cnt; ++i)
    m1pps_fpga_slvchip_process_msg(__fsm(slave->chip + i), ev, data, data_sz);
  return 0;
}

int m1pps_fpga_slave_process_chip_ev( m1pps_fpga_slave_t *slave, int ev, const void *data, size_t data_sz )
{
  const m1pps_msg_fpga_chipreq_t *msg = data;

  if (data_sz != sizeof(msg->chip_id))
    return -EINVAL;

  if (msg->chip_id >= slave->chip_cnt)
    return -EINVAL;

  return m1pps_fpga_slvchip_process_msg(__fsm(slave->chip + msg->chip_id), ev, data, data_sz);
}

int m1pps_fpga_slave_process_infreq( m1pps_fpga_slave_t *slave, const void *data, size_t data_sz )
{
  m1pps_msg_fpga_infrsp_t *msg = __msg_buf(slave);
  uint8_t i;
  msg->max_load_sz = 65536;
  msg->chip_cnt = slave->chip_cnt;
  for (i=0; i<slave->chip_cnt; ++i) {
    m1pps_fpga_slvchip_desc_t *src = &slave->chip[i].desc;
    m1pps_msg_fpgachip_desc_t *dst = &msg->chip[i];
    strncpy(dst->name, src->name, sizeof(dst->name));
    dst->chip_id = i;
  }
  return 0;
}

m1pps_fpga_slave_st_t m1pps_fpga_slave_process_err( m1pps_fpga_slave_t *slave, int err )
{
   //m1pps_slave_reset_sync(slave);
   return M1PPS_FPGA_SLAVE_ST_STARTED;
}

m1pps_fpga_slave_st_t m1pps_fpga_slave_process_timeout( m1pps_fpga_slave_t *slave, const char *timeout_msg )
{
   //slave->stat.err_timeout++;
   ERR("%s", timeout_msg);
   return m1pps_fpga_slave_process_err(slave, -ETIME);
}


int m1pps_fpga_slave_process_msg( m1pps_fsm_t *fsm, int ev, const void *data, size_t data_sz )
{
  int err;
  m1pps_fpga_slave_t *slave = container_of( fsm, m1pps_fpga_slave_t, fsm);
  m1pps_fpga_slave_st_t ns, s;
  s = ns = __state(slave);

  switch (s) {
    case M1PPS_FPGA_SLAVE_ST_INITIAL:
      if (ev != M1PPS_FPGA_SLAVE_EV_START)
         break;

      if ((err=m1pps_fpga_slave_process_allchip_ev( slave, ev, data, data_sz ))) {
         break;
      }
      ns = M1PPS_FPGA_SLAVE_ST_STARTED;
    break;

    case M1PPS_FPGA_SLAVE_ST_STARTED:

      if (ev == M1PPS_FPGA_SLAVE_EV_STOP) {
        err=m1pps_fpga_slave_process_allchip_ev( slave, ev, data, data_sz );
        ns = M1PPS_FPGA_SLAVE_ST_INITIAL;
        break;
      }

      if (ev == M1PPS_FPGA_SLAVE_EV_INFREQ) {
        if ((err = m1pps_fpga_slave_process_infreq( slave, data, data_sz ))) {
          ns = m1pps_fpga_slave_process_err(slave, err);
          break;
        }
        break;
      }

      if ( ! (ev == M1PPS_FPGA_SLAVE_EV_STSREQ ||
              ev == M1PPS_FPGA_SLAVE_EV_LINITREQ ||
              ev == M1PPS_FPGA_SLAVE_EV_LOADREQ ||
              ev == M1PPS_FPGA_SLAVE_EV_LOADCMPL ) )
        break;

      m1pps_fpga_slave_process_chip_ev(slave, ev, data, data_sz);

    break;
  }

  return ns;
}
