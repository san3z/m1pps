/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_clk.h"

int m1pps_clk_init( m1pps_clk_t *c, const  m1pps_clk_ops_t *ops, m1pps_time_t accurancy_ns )
{
  c->ops = *ops;
  c->ctx = 0;
  c->accurancy_ns = accurancy_ns;
  return 0;
}

void m1pps_clk_destroy( m1pps_clk_t *c )
{
}

m1pps_time_t m1pps_clk_get_time(  const m1pps_clk_t *c )
{
  return c->ops.get_time(c);
}

int m1pps_clk_adj_time( m1pps_clk_t *c, int64_t offset )
{
  return c->ops.adj_time( c, offset );
}

int m1pps_clk_set_time( m1pps_clk_t *c, m1pps_time_t time )
{
  return c->ops.set_time( c, time );
}


void m1pps_clk_set_ctx( m1pps_clk_t *c, void *ctx )
{
  c->ctx = ctx;
}

void *m1pps_clk_get_ctx( const m1pps_clk_t *c )
{
  return c->ctx;
}

m1pps_time_t m1pps_clk_get_accurancy( const m1pps_clk_t *c )
{
  return c->accurancy_ns;
}

int m1pps_clk_adj_period( m1pps_clk_t *c, m1pps_tofs_t period_k )
{
  if (!c->ops.adj_period)
    return -EINVAL;
  return c->ops.adj_period( c, period_k );
}

int m1pps_clk_set_output(m1pps_clk_t *c, uint8_t enabled)
{
  if (!c->ops.set_output)
    return -EINVAL;
  return c->ops.set_output( c, enabled );
}
