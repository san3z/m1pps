/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_pkt_i2c.h"
#include "m1pps_msg_i2c.h"

// i2c subsys

#define M1PPS_PKTSUBSYS_I2C      (1)

#define M1PPS_PKTID_I2C_INFREQ     (0)
#define M1PPS_PKTID_I2C_INFRSP     (1)
#define M1PPS_PKTID_I2C_RXREQ      (2)
#define M1PPS_PKTID_I2C_RXRSP      (3)
#define M1PPS_PKTID_I2C_TXREQ      (4)
#define M1PPS_PKTID_I2C_TXRSP      (5)

#pragma pack(push, 1)

typedef struct m1pps_pkt_i2c_infreq {
} m1pps_pkt_i2c_infreq_t;

typedef struct m1pps_pkt_i2cbus_desc {
  char name[16];
  uint8_t bus_id;
} m1pps_pkt_i2cbus_desc_t;

typedef struct m1pps_pkt_i2c_infrsp {
  int16_t ret_code;
  uint8_t bus_cnt;
  m1pps_pkt_i2cbus_desc_t bus[0];
} m1pps_pkt_i2c_infrsp_t;

typedef struct m1pps_pkt_i2c_rxreq {
  uint8_t bus_id;
} m1pps_pkt_i2c_rxreq_t;

typedef struct m1pps_pkt_i2c_rxrsp {
  int16_t ret_code;
  uint8_t bus_id;
  uint16_t size;
  uint8_t data[0];
} m1pps_pkt_i2c_rxrsp_t;

typedef struct m1pps_pkt_i2c_txreq {
  uint8_t bus_id;
  uint16_t size;
  uint8_t data[0];
} m1pps_pkt_i2c_txreq_t;

typedef struct m1pps_pkt_i2c_txrsp {
  int16_t ret_code;
  uint8_t bus_id;
} m1pps_pkt_i2c_txrsp_t;

#pragma pack(pop)

size_t _m1pps_pkt_subsys_i2c_pack_msg( m1pps_pkt_t *pkt, size_t offset, int msg_id,
                                       const void *msg_data, size_t msg_data_sz, uint8_t *pkt_id )
{
   size_t sz=0, max_sz = pkt->buf_sz - offset;
   void * pkt_body = pkt->buf + offset;

   switch ( msg_id ) {
     case M1PPS_MSG_I2C_INFREQ: {
       m1pps_pkt_i2c_infreq_t *p = pkt_body;
       //const m1pps_msg_i2c_infreq_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_I2C_INFREQ;
     } break;
     case M1PPS_MSG_I2C_INFRSP: {
       uint8_t i;
       m1pps_pkt_i2c_infrsp_t *p = pkt_body;
       const m1pps_msg_i2c_infrsp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->ret_code = hton_16(m->ret_code);
       p->bus_cnt = m->bus_cnt;
       if ( (max_sz-sz) < p->bus_cnt*sizeof(p->bus[0]) )
         return 0;
       for (i=0; i<p->bus_cnt;++i) {
         __strncpy(p->bus[i].name, m->bus[i].name, sizeof(p->bus[i].name));
         p->bus[i].bus_id = m->bus[i].bus_id;
       }
       sz += p->bus_cnt*sizeof(p->bus[0]);
       *pkt_id = M1PPS_PKTID_I2C_INFRSP;
     } break;
     case M1PPS_MSG_I2C_RXREQ: {
       m1pps_pkt_i2c_rxreq_t *p = pkt_body;
       const m1pps_msg_i2c_rxreq_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->bus_id = m->bus_id;
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_I2C_RXREQ;
     } break;
     case M1PPS_MSG_I2C_RXRSP: {
       m1pps_pkt_i2c_rxrsp_t *p = pkt_body;
       const m1pps_msg_i2c_rxrsp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->ret_code = hton_16(m->ret_code);
       p->size = hton_16(m->size);
       sz += sizeof(*p);
       if ( (max_sz-sz) < m->size )
         return 0;
       memcpy( p->data, m->data, m->size);
       sz += m->size;
       *pkt_id = M1PPS_PKTID_I2C_RXRSP;
     } break;
     case M1PPS_MSG_I2C_TXREQ: {
       m1pps_pkt_i2c_txreq_t *p = pkt_body;
       const m1pps_msg_i2c_txreq_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->size = hton_16(m->size);
       p->bus_id = m->bus_id;
       sz += sizeof(*p);
       if ( (max_sz-sz) < m->size )
         return 0;
       memcpy( p->data, m->data, m->size);
       sz += sizeof(m->size);
       *pkt_id = M1PPS_PKTID_I2C_TXREQ;
     } break;
     case M1PPS_MSG_I2C_TXRSP: {
       m1pps_pkt_i2c_txrsp_t *p = pkt_body;
       const m1pps_msg_i2c_txrsp_t *m = msg_data;
       if ( (max_sz-sz) < sizeof(*p) )
         return 0;
       p->ret_code = hton_16(m->ret_code);
       sz += sizeof(*p);
       *pkt_id = M1PPS_PKTID_I2C_TXRSP;
     } break;
     default: return 0;
   }

   return sz;
}

int _m1pps_pkt_subsys_i2c_unpack_msg( const m1pps_pkt_t *pkt, const m1pps_pkt_up_args_t *args,
                                      int *msg_id, void *msg_data, size_t *msg_data_sz  )
{
   size_t sz=0;
   void *pkt_body =  pkt->buf + args->body_ofs;
   switch ( args->id ) {
     case M1PPS_PKTID_I2C_INFREQ: {
       //const m1pps_pkt_i2c_infreq_t *p = pkt_body;
       m1pps_msg_i2c_infreq_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_I2C_INFREQ;
     } break;
     case M1PPS_PKTID_I2C_INFRSP: {
       uint8_t i;
       const m1pps_pkt_i2c_infrsp_t *p = pkt_body;
       m1pps_msg_i2c_infrsp_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->ret_code = ntoh_16(p->ret_code);
       m->bus_cnt = p->bus_cnt;
       sz += sizeof(*m);
       if ( (*msg_data_sz-sz) < m->bus_cnt*sizeof(m->bus[0]) )
         return -EINVAL;
       for (i=0; i<p->bus_cnt;++i) {
         __strncpy(m->bus[i].name, p->bus[i].name, sizeof(m->bus[i].name));
         m->bus[i].bus_id = p->bus[i].bus_id;
       }
       sz += m->bus_cnt*sizeof(m->bus[0]);
       *msg_id = M1PPS_MSG_I2C_INFRSP;
     } break;
     case M1PPS_PKTID_I2C_RXREQ: {
       const m1pps_pkt_i2c_rxreq_t *p = pkt_body;
       m1pps_msg_i2c_rxreq_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->bus_id = p->bus_id;
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_I2C_RXREQ;
     } break;
     case M1PPS_PKTID_I2C_RXRSP: {
       const m1pps_pkt_i2c_rxrsp_t *p = pkt_body;
       m1pps_msg_i2c_rxrsp_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->ret_code = ntoh_16(p->ret_code);
       m->bus_id = p->bus_id;
       m->size = ntoh_16(p->size);
       if ( (*msg_data_sz - sz ) < m->size)
         return -EINVAL;
       memcpy( m->data, p->data, m->size);
       sz += m->size;
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_I2C_RXRSP;
     } break;
     case M1PPS_PKTID_I2C_TXREQ: {
       const m1pps_pkt_i2c_txreq_t *p = pkt_body;
       m1pps_msg_i2c_txreq_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->size = ntoh_16(p->size);
       m->bus_id = p->bus_id;
       sz += sizeof(*m);
       if ( (*msg_data_sz - sz ) < m->size)
         return -EINVAL;
       memcpy( m->data, p->data, m->size);
       sz += m->size;
       *msg_id = M1PPS_MSG_I2C_TXREQ;
     } break;
     case M1PPS_PKTID_I2C_TXRSP: {
       const m1pps_pkt_i2c_txrsp_t *p = pkt_body;
       m1pps_msg_i2c_txrsp_t *m = msg_data;
       if ( (*msg_data_sz - sz ) < sizeof(*m))
         return -EINVAL;
       m->ret_code = ntoh_16(p->ret_code);
       m->bus_id = p->bus_id;
       sz += sizeof(*m);
       *msg_id = M1PPS_MSG_I2C_TXRSP;
     } break;
     default: return -EINVAL;
   }

   *msg_data_sz = sz;
   return 0;
}

#define __max(v1, v2) ((v1 < v2) ? v2 : v1)
#define MAX_I2C_DATA_SZ (256)

size_t _m1pps_pkt_subsys_i2c_get_max_sz(void)
{
  size_t sz = 0;
  sz = __max(sz, sizeof( m1pps_pkt_i2c_infreq_t));
  sz = __max(sz, sizeof( m1pps_pkt_i2c_infrsp_t));
  sz = __max(sz, sizeof( m1pps_pkt_i2c_txreq_t));
  sz = __max(sz, sizeof( m1pps_pkt_i2c_txrsp_t));
  sz = __max(sz, sizeof( m1pps_pkt_i2c_rxreq_t));
  sz = __max(sz, sizeof( m1pps_pkt_i2c_rxrsp_t));
  return sz;
}

m1pps_pkt_subsys_t m1pps_pkt_subsys_i2c = {
  .name = "i2c",
  .id =  M1PPS_PKTSUBSYS_I2C,
  .pack_msg =  _m1pps_pkt_subsys_i2c_pack_msg,
  .unpack_msg = _m1pps_pkt_subsys_i2c_unpack_msg,
  .get_max_pkt_sz = _m1pps_pkt_subsys_i2c_get_max_sz
};


