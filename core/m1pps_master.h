/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_fsm.h"
#include "m1pps_clk.h"
#include "m1pps_msg_sync.h"
#include "m1pps_cmn.h"

typedef struct m1pps_sync_master_info {
} m1pps_sync_master_info_t;

typedef struct m1pps_sync_master_stat {
} m1pps_sync_master_stat_t;

typedef struct m1pps_sync_master_slave_info {
  uint8_t          pll_flags;
  uint32_t         pll_phlock_dur_ms;
  uint32_t         pll_frlock_dur_ms;
} m1pps_sync_master_slave_info_t;

typedef struct m1pps_sync_master_slave_stat {
  uint32_t         time_adjust_cnt;
  uint32_t         period_adjust_cnt;
} m1pps_sync_master_slave_stat_t;

typedef struct m1pps_master {
  m1pps_fsm_t fsm;
  m1pps_sync_cmn_t cmn;
  m1pps_time_t t1;
  m1pps_time_t t4;
  m1pps_clk_t *clk;
  m1pps_sync_master_stat_t stat;
  m1pps_sync_master_info_t info;
  m1pps_sync_master_slave_stat_t slv_stat;
  m1pps_sync_master_slave_info_t slv_info;
  uint16_t cyrcles;
} m1pps_master_t;

typedef enum m1pps_master_st {
  M1PPS_MASTER_ST_INITIAL = FSM_ST_INITIAL,
  M1PPS_MASTER_ST_WAIT_SYNC_SENDED,      // 1
  M1PPS_MASTER_ST_FLUP_SENDED,           // 2
  M1PPS_MASTER_ST_WAIT_DLRESP_SENDED,    // 3
  M1PPS_MASTER_ST_RESTARTING,            // 4
} m1pps_master_st_t;

typedef enum m1pps_master_ev {
  M1PPS_MASTER_EV_TIMER = FSM_MSG_TIMER,     // 0
  M1PPS_MASTER_EV_DLREQ = M1PPS_MSG_SYNC_DLREQ,   // 3
  M1PPS_MASTER_EV_START = M1PPS_MSG_START,   // 256
  M1PPS_MASTER_EV_STOP = M1PPS_MSG_STOP,     // 257
  M1PPS_MASTER_EV_SENDED = M1PPS_MSG_SYNC_SENDED,  // 5
  M1PPS_MASTER_EV_INFRSP = M1PPS_MSG_SYNC_INFRSP
} m1pps_master_ev_t;

typedef enum m1pps_master_msg {
  M1PPS_MASTER_MSG_SYNC = M1PPS_MSG_SYNC_SYNC,     // 1
  M1PPS_MASTER_MSG_FLUP = M1PPS_MSG_SYNC_FLUP,     // 2
  M1PPS_MASTER_MSG_DLRESP = M1PPS_MSG_SYNC_DLRESP,  // 4
  M1PPS_MASTER_MSG_INFREQ = M1PPS_MSG_SYNC_INFREQ  // 4
} m1pps_master_msg_t;

/* internal */

int m1pps_master_init( m1pps_master_t *master, const m1pps_fsm_ticks_t *ticks, m1pps_clk_t *clk);
void m1pps_master_destroy( m1pps_master_t *master );

void m1pps_master_get_cmn_info( const m1pps_master_t *master, m1pps_sync_cmn_info_t *info );
void m1pps_master_get_cmn_stat( const m1pps_master_t *master, m1pps_sync_cmn_stat_t *stat );

void m1pps_master_get_info(const m1pps_master_t *master, m1pps_sync_master_info_t *info);
void m1pps_master_get_stat(const m1pps_master_t *master, m1pps_sync_master_stat_t *stat);

void m1pps_master_get_slave_info(const m1pps_master_t *master, m1pps_sync_master_slave_info_t *info);
void m1pps_master_get_slave_stat(const m1pps_master_t *master, m1pps_sync_master_slave_stat_t *stat);
