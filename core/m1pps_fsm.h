/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_sys.h"

/* fsm ticks */

#define FSM_TIMER_INC_MS (10)                     /** internal FSM timer incremented by this value */
#define FSM_MIN_TICKS    (1000/FSM_TIMER_INC_MS)  /** minimum number of tics per second needed for FSM */

typedef struct m1pps_fsm_ticks {
  uint16_t ticks_cnt;           /** Current ticks counter */
  uint16_t ticks_per_second;    /** Ticks per second value */
} m1pps_fsm_ticks_t;


void m1pps_fsm_ticks_init(m1pps_fsm_ticks_t *ticks, uint16_t tick_per_sec);
#define m1pps_fsm_ticks_destroy(ticks)
uint16_t m1pps_fsm_ticks_get_tps(const m1pps_fsm_ticks_t *ticks);

void m1pps_fsm_ticks_inc(m1pps_fsm_ticks_t *ticks);

void m1pps_fsm_timer_u32s_dec(uint32_t *timer, const m1pps_fsm_ticks_t *ticks);

uint64_t m1pps_fsm_ticks_ms_to_ticks( const m1pps_fsm_ticks_t *ticks, uint64_t time_ms);


/* abstract fsm */

#define FSM_ST_INITIAL     (0)
#define FSM_MSG_TIMER      (0)
#define FSM_MSG_USER       (1)

struct m1pps_fsm;

typedef struct m1pps_fsm_trs_ops {
  int (*send_msg)( struct m1pps_fsm *fsm, int msg_id, const void *data, size_t data_sz );
} m1pps_fsm_trs_ops_t;

typedef struct m1pps_fsm_ops {
  int (*process_msg)( struct m1pps_fsm *fsm, int msg_id, const void *data,  size_t data_sz );
  int (*state_changed)( struct m1pps_fsm *fsm, int msg_id, int old_st, int new_st );
} m1pps_fsm_ops_t;

typedef struct m1pps_fsm_timer {
  uint16_t start_ticks_cnt;
  uint32_t value;
} m1pps_fsm_timer_t;

typedef struct m1pps_fsm {
  const m1pps_fsm_ticks_t *ticks;
  m1pps_fsm_timer_t *timers;
  uint8_t timer_cnt;
  int state;
  void **ctx;
  void   *msg_buf;
  size_t  msg_buf_sz;
  m1pps_fsm_ops_t ops;
  m1pps_fsm_trs_ops_t trs_ops;
} m1pps_fsm_t;

int  m1pps_fsm_init_by_mem( m1pps_fsm_t *fsm, const m1pps_fsm_ticks_t *ticks, size_t buf_sz, uint8_t timer_cnt,  uint8_t ctx_cnt, void * );
int  m1pps_fsm_destroy_by_mem( m1pps_fsm_t *fsm );

int  m1pps_fsm_init( m1pps_fsm_t *fsm, const m1pps_fsm_ticks_t *ticks, size_t buf_sz, uint8_t timer_cnt, uint8_t ctx_cnt,
                     const m1pps_fsm_ops_t *ops );
void m1pps_fsm_destroy( m1pps_fsm_t *fsm );

void m1pps_fsm_set_trs( m1pps_fsm_t *fsm, const m1pps_fsm_trs_ops_t *trs_ops );

void m1pps_fsm_tick( m1pps_fsm_t *fsm );
int  m1pps_fsm_process_msg( m1pps_fsm_t *fsm, int msg_id, const void *data, size_t data_sz );

int m1pps_fsm_send_msg( m1pps_fsm_t *fsm, int msg_id, void *data, size_t data_sz );
uint32_t m1pps_fsm_get_timer( const m1pps_fsm_t *fsm, uint8_t timer_id );
void m1pps_fsm_start_timer( m1pps_fsm_t *fsm, uint8_t timer_id, uint32_t value );
void m1pps_fsm_stop_timer( m1pps_fsm_t *fsm, uint8_t timer_id );
int  m1pps_fsm_get_state( const m1pps_fsm_t *fsm );
void * m1pps_fsm_get_msg_buf( const m1pps_fsm_t *fsm );
size_t m1pps_fsm_get_msg_buf_sz( const m1pps_fsm_t *fsm );

void m1pps_fsm_set_ctx( const m1pps_fsm_t *fsm, uint8_t ctx_idx, void *ctx );
void *m1pps_fsm_get_ctx( const m1pps_fsm_t *fsm, uint8_t ctx_idx );


#define __fsm(f)( &(f)->fsm)
#define __restart_timer(f, id, value) m1pps_fsm_restart_timer( __fsm(f), id, value )
#define __start_timer(f, id, value) m1pps_fsm_start_timer( __fsm(f), id, value )
#define __stop_timer(f, id) m1pps_fsm_stop_timer( __fsm(f), id )
#define __timer(f, id) m1pps_fsm_get_timer( __fsm(f), id )
#define __send_msg(f, msg, data, data_sz) m1pps_fsm_send_msg( __fsm(f), msg, data, data_sz )
#define __state(f) m1pps_fsm_get_state( __fsm(f) )
#define __buf_sz(f) m1pps_fsm_get_buf_sz( __fsm(f) )
#define __msg_buf(f) m1pps_fsm_get_msg_buf( __fsm(f) )

