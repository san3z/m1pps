/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_sys.h"
#include "m1pps_trs.h"

#include <linux/spinlock.h>
#include <linux/kthread.h>
#include <linux/usb.h>
#include <linux/workqueue.h>

typedef struct m1pps_trs_usb {
  m1pps_trs_t trs;
  struct list_head events;
  spinlock_t ev_lock;
  struct task_struct *thread;
  uint32_t rx_period_us;
  struct usb_device *device;
  uint8_t ep[2];
  wait_queue_head_t wq_head;
  uint8_t wq_flag;
  size_t   max_pkt_sz;
  struct urb *urb[2];
  uint8_t started[2];
  int8_t xfer_type;
  int interval;
} m1pps_trs_usb_t;

typedef struct m1pps_trs_usb_open_data {
  struct   usb_device *usb_dev;
  uint32_t rx_period_us;
  uint8_t  ep[2];
} m1pps_trs_usb_open_data_t;

int m1pps_trs_usb_init( m1pps_trs_usb_t *t, m1pps_clk_t *clk, size_t max_pkt_sz );
void m1pps_trs_usb_destroy( m1pps_trs_usb_t *t );

