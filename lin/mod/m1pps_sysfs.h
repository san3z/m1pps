/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#pragma once

#include "m1pps_inst.h"

#include <linux/workqueue.h>
#include <linux/kobject.h>

typedef struct m1pps_sysfs {
  m1pps_inst_t *inst;
  m1pps_trs_stat_t trs_stat;
  m1pps_sync_cmn_stat_t cmn_stat;
  m1pps_sync_cmn_info_t cmn_info;
  m1pps_sync_master_slave_info_t slv_info;
  m1pps_sync_master_slave_stat_t slv_stat;
  struct delayed_work update_work;
  struct workqueue_struct *wq;
  struct kobject kobj;
} m1pps_sysfs_t;


void m1pps_sysfs_init(m1pps_sysfs_t *sysfs, m1pps_inst_t *inst, struct device *dev);
void m1pps_sysfs_destroy(m1pps_sysfs_t *sysfs);
void m1pps_sysfs_update(m1pps_sysfs_t *sysfs);

