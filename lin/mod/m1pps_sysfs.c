/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   

#include "m1pps_sysfs.h"
#include "m1pps_inst.h"

#include <linux/sysfs.h>
#include <linux/device.h>

typedef struct m1pps_sysfs_entry {
  struct attribute attr;
  ssize_t(*show) (m1pps_sysfs_t *, char *);
  size_t (*store)(m1pps_sysfs_t *, const char *, size_t);
}  m1pps_sysfs_entry_t;


#define M1PPS_SYSFS_SHOW(NAME) \
static ssize_t m1pps_sysfs_entry_ ## NAME ## _show \
                 (m1pps_sysfs_t *sysfs, char *page)

#define M1PPS_SYSFS_STORE(NAME) \
static size_t m1pps_sysfs_entry_ ## NAME ## _store \
                 (m1pps_sysfs_t *sysfs, const char *page, size_t count)

#define M1PPS_SYSFS_RO_ENTRY(NAME) \
static m1pps_sysfs_entry_t m1pps_sysfs_entry_ ## NAME = { \
    .attr = {.name = #NAME,.mode = S_IRUGO}, \
    .show = m1pps_sysfs_entry_ ## NAME ## _show, \
    .store = 0 \
}

#define M1PPS_SYSFS_RW_ENTRY(NAME) \
static m1pps_sysfs_entry_t m1pps_sysfs_entry_ ## NAME = { \
    .attr = {.name = #NAME,.mode = S_IRUGO | S_IWUSR }, \
    .show = m1pps_sysfs_entry_ ## NAME ## _show, \
    .store = m1pps_sysfs_entry_ ## NAME ## _store \
}

#define M1PPS_SYSFS_ENTRY_ATTR(NAME) m1pps_sysfs_entry_ ## NAME.attr


M1PPS_SYSFS_SHOW(role)
{
  return sprintf(page, "%s\n", m1pps_inst_get_role( sysfs->inst ) == M1PPS_INST_ROLE_MASTER ? "master": "slave");
}

M1PPS_SYSFS_STORE(role)
{
  return -EINVAL;
}

M1PPS_SYSFS_RW_ENTRY(role);


M1PPS_SYSFS_SHOW(offset)
{
  size_t sz=0;
  sz += tofs2s( sysfs->cmn_info.offset, page, T_SZ);
  sz += sprintf(page + sz, "\n");
  return sz;
}
M1PPS_SYSFS_RO_ENTRY(offset);

M1PPS_SYSFS_SHOW(sync)
{
  return sprintf(page, "%u\n", sysfs->cmn_info.proto_sync);
}
M1PPS_SYSFS_RO_ENTRY(sync);

M1PPS_SYSFS_SHOW(delay)
{
  size_t sz=0;
  sz += tofs2s( sysfs->cmn_info.delay, page, T_SZ);
  sz += sprintf(page + sz, "\n");
  return sz;
}
M1PPS_SYSFS_RO_ENTRY(delay);

M1PPS_SYSFS_SHOW(cyrcles)
{
  return sprintf(page , "%"PRIu32"\n", sysfs->cmn_stat.cyrcles);
}
M1PPS_SYSFS_RO_ENTRY(cyrcles);

M1PPS_SYSFS_SHOW(good)
{
  return sprintf(page , "%"PRIu32"\n", sysfs->cmn_stat.good_cyrcles);
}
M1PPS_SYSFS_RO_ENTRY(good);

M1PPS_SYSFS_SHOW(err_timeout)
{
  return sprintf(page , "%"PRIu32"\n", sysfs->cmn_stat.err_timeout);
}
M1PPS_SYSFS_RO_ENTRY(err_timeout);

M1PPS_SYSFS_SHOW(err_delay)
{
  return sprintf(page , "%"PRIu32"\n", sysfs->cmn_stat.err_delay);
}
M1PPS_SYSFS_RO_ENTRY(err_delay);

M1PPS_SYSFS_SHOW(err_sync_lost)
{
  return sprintf(page , "%"PRIu32"\n", sysfs->cmn_stat.err_sync_lost);
}
M1PPS_SYSFS_RO_ENTRY(err_sync_lost);

M1PPS_SYSFS_SHOW(time_adjust)
{
  return sprintf(page, "%"PRIu32"\n", sysfs->slv_stat.time_adjust_cnt);
}
M1PPS_SYSFS_RO_ENTRY(time_adjust);

M1PPS_SYSFS_SHOW(period_adjust)
{
  return sprintf(page , "%"PRIu32"\n", sysfs->slv_stat.period_adjust_cnt);
}
M1PPS_SYSFS_RO_ENTRY(period_adjust);

M1PPS_SYSFS_SHOW(pll_plocked)
{
  return sprintf(page , "%"PRIu32"\n", sysfs->slv_info.pll_flags & 1);
}
M1PPS_SYSFS_RO_ENTRY(pll_plocked);

M1PPS_SYSFS_SHOW(pll_flocked)
{
  return sprintf(page , "%"PRIu32"\n", (sysfs->slv_info.pll_flags & 2) ? 1 : 0);
}
M1PPS_SYSFS_RO_ENTRY(pll_flocked);

M1PPS_SYSFS_SHOW(pll_flock_dur)
{
  return sprintf(page , "%"PRIu32"\n", sysfs->slv_info.pll_frlock_dur_ms);
}
M1PPS_SYSFS_RO_ENTRY(pll_flock_dur);

M1PPS_SYSFS_SHOW(pll_plock_dur)
{
  return sprintf(page , "%"PRIu32"\n", sysfs->slv_info.pll_phlock_dur_ms);
}
M1PPS_SYSFS_RO_ENTRY(pll_plock_dur);

M1PPS_SYSFS_SHOW(summary)
{
  size_t sz=0;
  m1pps_sync_cmn_stat_t *s =  &sysfs->cmn_stat;
  m1pps_sync_cmn_info_t *i =  &sysfs->cmn_info;
  m1pps_trs_stat_t *ts =  &sysfs->trs_stat;
  sz+=sprintf(page+sz, "Protocol sync:%s\n", i->proto_sync?"yes":"no");
  sz+=sprintf(page+sz, "Cyrcles:%"PRIu32" Good:%"PRIu32"\n", s->cyrcles, s->good_cyrcles);
  sz+=sprintf(page+sz, "Offset:%"PRIi64" Delay:%"PRIu64"\n", i->offset, i->delay);
  sz+=sprintf(page+sz, "Protocol errors: Timeouts:%"PRIu32" Delay:%"PRIu32" Sync Lost:%"PRIu32"\n", s->err_timeout, s->err_delay, s->err_sync_lost);
  sz+=sprintf(page+sz, "Transport errors: Recieve:%"PRIu32" Transmit:%"PRIu32"\n", ts->err_rx, ts->err_tx );
  return sz;
}

M1PPS_SYSFS_RO_ENTRY(summary);

static struct attribute *m1pps_sysfs_attrs[] = {
    &M1PPS_SYSFS_ENTRY_ATTR(role),
    &M1PPS_SYSFS_ENTRY_ATTR(sync),
    &M1PPS_SYSFS_ENTRY_ATTR(offset),
    &M1PPS_SYSFS_ENTRY_ATTR(delay),
    &M1PPS_SYSFS_ENTRY_ATTR(cyrcles),
    &M1PPS_SYSFS_ENTRY_ATTR(good),
    &M1PPS_SYSFS_ENTRY_ATTR(err_timeout),
    &M1PPS_SYSFS_ENTRY_ATTR(err_delay),
    &M1PPS_SYSFS_ENTRY_ATTR(err_sync_lost),
    &M1PPS_SYSFS_ENTRY_ATTR(time_adjust),
    &M1PPS_SYSFS_ENTRY_ATTR(period_adjust),
    &M1PPS_SYSFS_ENTRY_ATTR(pll_plocked),
    &M1PPS_SYSFS_ENTRY_ATTR(pll_flocked),
    &M1PPS_SYSFS_ENTRY_ATTR(pll_plock_dur),
    &M1PPS_SYSFS_ENTRY_ATTR(pll_flock_dur),
    &M1PPS_SYSFS_ENTRY_ATTR(summary),
    0,
};

static ssize_t m1pps_sysfs_attr_show(struct kobject *kobj, struct attribute *attr,
                                   char *page)
{
    struct m1pps_sysfs *sysfs = container_of(kobj, struct m1pps_sysfs, kobj);
    struct m1pps_sysfs_entry *entry = container_of(attr, struct m1pps_sysfs_entry, attr);
    if ( !( sysfs && entry && entry->show ) )
      return -EINVAL;
    return entry->show(sysfs, page);
}

static ssize_t m1pps_sysfs_attr_store(struct kobject *kobj,
                                      struct attribute *attr, const char *page,
                                      size_t count)
{
    struct m1pps_sysfs *sysfs = container_of(kobj, struct m1pps_sysfs, kobj);
    struct m1pps_sysfs_entry *entry = container_of(attr, struct m1pps_sysfs_entry, attr);

    if ( !( sysfs && entry && entry->store ) )
      return -EINVAL;

    return entry->store(sysfs, page, count);
}

static const struct sysfs_ops m1pps_sysfs_ops = {
    .show = m1pps_sysfs_attr_show,
    .store = m1pps_sysfs_attr_store,
};

static struct kobj_type m1pps_sysfs_ktype = {
    .default_attrs = m1pps_sysfs_attrs,
    .sysfs_ops = &m1pps_sysfs_ops,
};

void m1pps_sysfs_update(m1pps_sysfs_t *sysfs)
{
}

static void __m1pps_sysfs_update_work_run(struct work_struct *work)
{
   struct m1pps_sysfs *sysfs = container_of( to_delayed_work(work), struct m1pps_sysfs, update_work );

   m1pps_inst_get_sync_info( sysfs->inst, &sysfs->cmn_info);
   m1pps_inst_get_sync_stat( sysfs->inst, &sysfs->cmn_stat);
   if (m1pps_inst_get_role(sysfs->inst) == M1PPS_INST_ROLE_MASTER) {
     m1pps_master_get_slave_info(&sysfs->inst->master, &sysfs->slv_info);
     m1pps_master_get_slave_stat(&sysfs->inst->master, &sysfs->slv_stat);
   }
   m1pps_inst_get_trs_stat( sysfs->inst, &sysfs->trs_stat);

   queue_delayed_work( sysfs->wq, to_delayed_work(work), msecs_to_jiffies(2000));
}

static void _m1pps_sysfs_destroy_kobj(struct kobject *kobj)
{
  if (!kobj)
      return;

  if (!kobj->state_initialized)
      return;

  if (kobj->state_in_sysfs) {
      kobject_uevent(kobj, KOBJ_REMOVE);
      kobject_del(kobj);
  }
  kobject_put(kobj);
}

void m1pps_sysfs_destroy(m1pps_sysfs_t *sysfs)
{
  cancel_delayed_work( &sysfs->update_work );	/* no "new ones" */
	flush_workqueue( sysfs->wq );
  destroy_workqueue( sysfs->wq );
/*
  if (sysfs->dev_class->dev_kobj->state_in_sysfs) {
    sysfs_remove_link(sysfs->dev_class->dev_kobj, "0");
  }
*/
  _m1pps_sysfs_destroy_kobj( &sysfs->kobj );
}

extern char   m1pps_name[];

void m1pps_sysfs_init(m1pps_sysfs_t *sysfs, m1pps_inst_t *inst, struct device *dev)
{
    int err=0;
    sysfs->inst = inst;

    err = kobject_init_and_add(&sysfs->kobj, &m1pps_sysfs_ktype, &dev->kobj, m1pps_name);
    if (err) {
        ERR("Failed to initialize sysfs (%i)", err);
        return;
    }
    kobject_uevent(&sysfs->kobj, KOBJ_ADD);

/*
    err = sysfs_create_link(sysfs->dev_class->dev_kobj, &sysfs->kobj, "0");
    if (err) {
      ERR("Can't create class in sysfs (%i)", err);
    }
*/
    sysfs->wq = create_workqueue("m1pps_stat_wq");
    INIT_DELAYED_WORK( &sysfs->update_work, __m1pps_sysfs_update_work_run );
    queue_delayed_work( sysfs->wq, &sysfs->update_work, msecs_to_jiffies(2000));
}


