/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include "m1pps_trs_usb.h"
#include "m1pps_usb.h"


#define DEV_DBG(p, ... )  dev_dbg( &(( m1pps_trs_usb_t *)p)->device->dev, ##__VA_ARGS__ )
#define DEV_ERR(p, ... )  dev_err( &(( m1pps_trs_usb_t *)p)->device->dev, ##__VA_ARGS__ )
#define DEV_INF(p, ... )  dev_inf( &(( m1pps_trs_usb_t *)p)->device->dev, ##__VA_ARGS__ )

struct evhdr {
  uint8_t type;
  m1pps_time_t ts;
  size_t size;
};

struct event {
  struct list_head list;
  struct evhdr hdr;
};

#define EV_TX_COMPLETE (0)
#define EV_RX          (1)

static void	__m1pps_trs_put_event( struct m1pps_trs_usb *trs_usb, struct event *e )
{
  unsigned long flags;
  spin_lock_irqsave( &trs_usb->ev_lock, flags);
  list_add_tail( &e->list,  &trs_usb->events );
  trs_usb->wq_flag = 1;
  wake_up(&trs_usb->wq_head);
  spin_unlock_irqrestore( &trs_usb->ev_lock, flags );
}

struct event	*__m1pps_trs_usb_get_event( m1pps_trs_usb_t *trs_usb )
{
  struct event *e=0;
  unsigned long flags;

  spin_lock_irqsave(&trs_usb->ev_lock, flags);

  if ( list_empty(&trs_usb->events) )
    goto exit;

  e = list_entry(trs_usb->events.next, struct event, list);

  if (!e)
    goto exit;

  list_del(&e->list);

exit:

  spin_unlock_irqrestore( &trs_usb->ev_lock, flags );

  return e;

}

static void	__m1pps_trs_usb_send_msg_cb( struct urb* urb )
{
  m1pps_trs_usb_t *trs_usb;
  struct event *e;

  if ( ! (urb->context && urb->transfer_buffer ) )
    return;

  trs_usb = urb->context;
  e = urb->transfer_buffer - sizeof(*e);

  DEV_DBG( trs_usb, "urb: buf:%px len:%u sts:%i", urb->transfer_buffer,
                     urb->actual_length, urb->status);

  if ( urb->status ) {
    if ( ! (urb->status == -ENOENT ||
            urb->status == -ECONNRESET ||
            urb->status == -ESHUTDOWN ) ) {
		  DEV_ERR( trs_usb, "urb tx error (%i) ", urb->status);
    }
    __trs(trs_usb)->stat.err_tx++;
    kfree( e );
    return;
  }

  e->hdr.type = EV_TX_COMPLETE;
  //e->hdr.ts   = m1pps_trs_get_time(__trs(trs_usb));
  e->hdr.size = urb->actual_length;

/*
  if ( e->hdr.size )
    memcpy( e + 1, urb->transfer_buffer, e->hdr.size);
*/

  __m1pps_trs_put_event( trs_usb,  e );
}

int m1pps_trs_usb_send_pkt( struct m1pps_trs *t, const void *data, size_t data_sz )
{
  m1pps_trs_usb_t *trs_usb = container_of(t, struct m1pps_trs_usb, trs);
  struct event *e;
  int err=0;
  DEV_DBG( trs_usb, "sz:%lu", data_sz);

  if ( data_sz > trs_usb->max_pkt_sz )
    data_sz = trs_usb->max_pkt_sz;

  if (trs_usb->started[TX]) {
    err = -EBUSY;
    goto exit;
  }

  if ( !(e = kmalloc( trs_usb->max_pkt_sz + sizeof(*e), GFP_ATOMIC )) ) {
    err = -ENOMEM;
    goto exit;
  }

  memcpy( e + 1, data, data_sz);

  switch ( trs_usb->xfer_type ) {

    case M1PPS_USB_XFER_TYPE_BULK:
	    usb_fill_bulk_urb(trs_usb->urb[TX], trs_usb->device,
                        usb_sndbulkpipe(trs_usb->device, trs_usb->ep[TX]),
				                (void*) (e + 1),	data_sz,
				                __m1pps_trs_usb_send_msg_cb, t);
    break;

    case M1PPS_USB_XFER_TYPE_ISOC:
	    usb_fill_int_urb(trs_usb->urb[TX], trs_usb->device,
                       usb_sndisocpipe(trs_usb->device, trs_usb->ep[TX]),
				               (void*) (e + 1),	data_sz,
				               __m1pps_trs_usb_send_msg_cb, t, trs_usb->interval);
    break;

    default:
      err = -EINVAL;
      goto exit;
  }

	err = usb_submit_urb(trs_usb->urb[TX], GFP_ATOMIC);

  if ( !err ) {
    trs_usb->started[TX]=1;
    e->hdr.ts = m1pps_trs_get_time(t);
  }

exit:

  if (err) {
		DEV_ERR(trs_usb, "send pkt error (%i) sz %zu", err, data_sz);
    __trs(trs_usb)->stat.err_tx++;
  }

  return err;
}

int m1pps_trs_usb_start_read( m1pps_trs_usb_t *trs_usb );

static void __m1pps_trs_usb_recv_msg_cb( struct urb* urb )
{
  m1pps_trs_usb_t *trs_usb ;
  struct event *e;

  if ( ! ( urb->context && urb->transfer_buffer )  ) {
		 DEV_ERR( trs_usb, "wrong urb competed (%i)", urb->status );
     __trs(trs_usb)->stat.err_rx++;
     return;
  }

  trs_usb = urb->context;
  e = urb->transfer_buffer - sizeof(*e);
  trs_usb->started[RX] = 0;

  DEV_DBG( trs_usb, "urb: buf:%px len:%u sts:%i",
                    urb->transfer_buffer, urb->actual_length, urb->status);

  if ( urb->status ) {
    if ( ! (urb->status == -ENOENT ||
            urb->status == -ECONNRESET ||
            urb->status == -ESHUTDOWN ) ) {
		  DEV_ERR( trs_usb, "urb rx error (%i) ", urb->status );
    }
    __trs(trs_usb)->stat.err_rx++;

    kfree( e );
    goto next;
  }

  e->hdr.type = EV_RX;
  e->hdr.ts   = m1pps_trs_get_time(__trs(trs_usb));
  e->hdr.size = urb->actual_length;

  __m1pps_trs_put_event( trs_usb, e );

next:

  if ( trs_usb->xfer_type != M1PPS_USB_XFER_TYPE_ISOC )  {
    m1pps_trs_usb_start_read( trs_usb );
  }
}

int m1pps_trs_usb_start_read( m1pps_trs_usb_t *trs_usb )
{
  int err;
  struct event *e;

  DEV_DBG( trs_usb, "t:%u ", trs_usb->xfer_type);

  if ( !(e = kmalloc( trs_usb->max_pkt_sz + sizeof(*e), GFP_ATOMIC )) ) {
    return -ENOMEM;
  }

  switch ( trs_usb->xfer_type ) {

    case M1PPS_USB_XFER_TYPE_BULK:
      usb_fill_bulk_urb(trs_usb->urb[RX], trs_usb->device, usb_rcvbulkpipe(trs_usb->device, trs_usb->ep[RX]),
                        e+1, trs_usb->max_pkt_sz, __m1pps_trs_usb_recv_msg_cb, trs_usb);
    break;

    case M1PPS_USB_XFER_TYPE_ISOC:
      usb_fill_int_urb(trs_usb->urb[RX], trs_usb->device, usb_rcvisocpipe(trs_usb->device, trs_usb->ep[RX]),
                       e+1, trs_usb->max_pkt_sz, __m1pps_trs_usb_recv_msg_cb, trs_usb, trs_usb->interval);
    break;

    default:
      kfree(e);
      return -EINVAL;
  }

  err = usb_submit_urb(trs_usb->urb[RX], GFP_ATOMIC);

  if ( err )
    DEV_ERR( trs_usb, " usb_submit_urb RX (%i)", err );
  else
    trs_usb->started[RX] = 1;

  return err;
}

static int __m1pps_trs_usb_thread( void *ctx )
{
  struct event *e;
  m1pps_trs_t *trs = ctx;
  m1pps_trs_usb_t *trs_usb = ctx;
  unsigned long rx_jiffies=0, cur_jiffies=0;
  int err;

  cur_jiffies = rx_jiffies = usecs_to_jiffies( trs_usb->rx_period_us );
  DEV_DBG( trs_usb, "transport started %uus == %lu jiffies",
                    trs_usb->rx_period_us, rx_jiffies);

  m1pps_trs_usb_start_read( trs_usb );

  while ( ! kthread_should_stop() ) {

     err = wait_event_interruptible_timeout( trs_usb->wq_head, trs_usb->wq_flag, cur_jiffies );

     if ( err<0 )
       continue;

     if (err==0) {
       #if 0
       if ( ! trs_usb->started[RX] ) {
         m1pps_trs_usb_start_read( trs_usb );
       }
       #endif
       cur_jiffies = rx_jiffies;
       continue;
     }

     cur_jiffies = err;

     trs_usb->wq_flag = 0;


     /* */
     while ( (e=__m1pps_trs_usb_get_event( trs_usb )))  {

       DEV_DBG( trs_usb, "event %px type:%u sz:%"PRIsz, e+1, e->hdr.type, e->hdr.size);

       if (e->hdr.type == EV_TX_COMPLETE) {
         trs_usb->started[TX] = 0;
         m1pps_trs_call_pkt_sended( trs, e->hdr.ts );
       } else {
         m1pps_trs_call_pkt_recieved( trs, e+1, e->hdr.size, e->hdr.ts );
       }

       kfree( e );

     }

  }

  if (trs_usb->started[RX]) {
    usb_kill_urb(trs_usb->urb[RX]);
  }
  if (trs_usb->started[TX]) {
    usb_kill_urb(trs_usb->urb[TX]);
  }

  DEV_DBG( trs_usb, "transport stopped");

  return 0;
}

int m1pps_trs_usb_open( m1pps_trs_t *t, void  *opendata )
{
  m1pps_trs_usb_t *trs_usb = container_of(t, struct m1pps_trs_usb, trs);
  m1pps_trs_usb_open_data_t *open_data = opendata;
  trs_usb->device = open_data->usb_dev;
  trs_usb->rx_period_us = open_data->rx_period_us;
  memcpy(trs_usb->ep, open_data->ep, sizeof(trs_usb->ep) );
  trs_usb->wq_flag = 0;
  trs_usb->interval = 16; // fix me !!!
  trs_usb->thread = kthread_run(__m1pps_trs_usb_thread, t, "m1pps usb %s", trs_usb->device->devpath);
  init_waitqueue_head(&trs_usb->wq_head);
  return 0;
}

int m1pps_trs_usb_close( m1pps_trs_t *t)
{
  m1pps_trs_usb_t *trs_usb = container_of(t, struct m1pps_trs_usb, trs);

  if ( trs_usb->thread ) {
    kthread_stop( trs_usb->thread );
    trs_usb->thread = 0;
  }
  return 0;
}

int m1pps_trs_usb_init( m1pps_trs_usb_t *trs_usb, m1pps_clk_t *clk, size_t max_pkt_sz )
{
   m1pps_trs_ops_t ops={0};

   ops.open  = m1pps_trs_usb_open;
   ops.close = m1pps_trs_usb_close;
   ops.send_pkt = m1pps_trs_usb_send_pkt;
   trs_usb->max_pkt_sz = max_pkt_sz;
   trs_usb->xfer_type = M1PPS_USB_XFER_TYPE;

   m1pps_trs_init( __trs(trs_usb) , &ops, clk );

   spin_lock_init(&trs_usb->ev_lock);
   INIT_LIST_HEAD(&trs_usb->events);

   if ( ! (trs_usb->urb[RX] = usb_alloc_urb(0, GFP_KERNEL)) ) {
	  	pr_err("Failed to allocate a URB on rx!");
		  return -ENOMEM;
   }
   if ( ! (trs_usb->urb[TX] = usb_alloc_urb(0, GFP_KERNEL)) ) {
	  	pr_err("Failed to allocate a URB on tx!");
		  return -ENOMEM;
   }

   return 0;
}

void m1pps_trs_usb_destroy( m1pps_trs_usb_t *trs_usb )
{
   m1pps_trs_destroy(__trs(trs_usb));

   if ( trs_usb->urb[RX] ) {
      usb_free_urb( trs_usb->urb[RX] );
      trs_usb->urb[RX]=0;
   }
   if ( trs_usb->urb[TX] ) {
      usb_free_urb( trs_usb->urb[TX] );
      trs_usb->urb[TX]=0;
   }

   //m1pps_trs_usb_t *trs_usb = container_of(t, struct m1pps_trs_usb, trs);
   //m1pps_trs_usb_close(trs_usb);
}



