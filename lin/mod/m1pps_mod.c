/*
   Copyright (C) 2020-2022 Alexander V. Buev <san@zzz.spb.ru>

   This file is part of m1pps project.

   m1pps is free software: you can redistribute it and/or modify it under the terms of 
   the GNU General Public License as published by the Free Software Foundation, 
   either version 3 of the License, or (at your option) any later version.

   m1pps is distributed in the hope that it will be useful, but WITHOUT ANY 
   WARRANTY; without even the implied warranty of MERCHANTABILITY or 
   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along with Foobar. 
   If not, see <https://www.gnu.org/licenses/>. 
*/   
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/usb.h>
#include <linux/timer.h>
#include <linux/version.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

#include "m1pps_usb.h"
#include "m1pps_trs_usb.h"
#include "m1pps_sysfs.h"
#include "m1pps_inst.h"

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,11,0)
  #include <linux/sched.h>
#else
  #include <uapi/linux/sched/types.h>
#endif

#define MAX_PKT_SIZE 512
#define MIN(a,b) (((a) <= (b)) ? (a) : (b))

#define STAT_PRINT_S     (2)

#define M1PPS_TIMER_PERIOD_US (10000)
#define RX_PERIOD_US (50000)

static unsigned long timer_period_us = M1PPS_TIMER_PERIOD_US;
MODULE_PARM_DESC(timer_period, "Timer tick period (us)");
module_param_named(timer_period, timer_period_us, ulong, 0);

static unsigned long rx_period_us = RX_PERIOD_US;
MODULE_PARM_DESC(rx_period, "Usb device read period (us)");
module_param_named(rx_period, rx_period_us, ulong, 0);

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,10,0)
 #define ktime_t u64
#endif

#define PRIkt "llu"

struct time_stat_s {
  ktime_t min;
  ktime_t max;
  ktime_t avg;
};

#define RX (0)
#define TX (1)

struct m1pps_drv_data {
  struct m1pps_sysfs sysfs;
  struct m1pps_inst inst;
  spinlock_t *inst_locks;
  struct m1pps_trs_usb trs_usb;
  struct task_struct *timer_thread;
  struct usb_device *usb_dev;
  struct device *dev;
};

char   m1pps_name[] = "m1pps";

static struct class m1pps_class = {
  .name = m1pps_name,
  .owner = THIS_MODULE,
};

static int m1pps_add( struct device * dev, struct class_interface *iface )
{
  return 1;
}

static void m1pps_remove( struct device * dev, struct class_interface *iface )
{
}

static struct class_interface m1pps_interface = {
  .class  = &m1pps_class,
  .add_dev    = m1pps_add,
  .remove_dev = m1pps_remove
};

void gen_write_callback(struct urb *u)
{
	usb_free_urb(u);
  	return;
}

void m1pps_sleep( uint64_t sleep_us)
{
  if ( sleep_us >= 10 )
    usleep_range( sleep_us, sleep_us );
  else
    udelay( sleep_us );
}

static void m1pps_set_high_priority(void)
{
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,9,0)
   struct sched_param param = { .sched_priority = 1 };
   sched_setscheduler(current, SCHED_FIFO, &param);
#else
   sched_set_fifo_low(current);
#endif
}

int m1pps_timer_thread(void *data)
{
   int err=0;
   struct m1pps_drv_data *drv = data;
   uint64_t sleep_us = ((uint64_t)timer_period_us) >> 3;

   dev_dbg(&drv->usb_dev->dev, "timers started period:%luus %llu", timer_period_us, sleep_us );

   m1pps_set_high_priority();

   while ( ! kthread_should_stop() ) {

     uint64_t ct_us, nt_us, dt_us;
     m1pps_inst_tick( &drv->inst );
     ct_us = ktime_get_real_ns()/1000;
     nt_us = ct_us + timer_period_us;
     //dev_dbg(&drv->usb_dev->dev, "ct:%llu nt:%llu", ct_us, nt_us );

     do {

       if ( ct_us < nt_us ) {
         dt_us = nt_us - ct_us;
         if ( dt_us >= sleep_us ) {
           m1pps_sleep( sleep_us );
           ct_us = ktime_get_real_ns()/1000;
         } else {
           if ( (ct_us + sleep_us - nt_us) < dt_us ) {
             m1pps_sleep( sleep_us );
             ct_us = ktime_get_real_ns()/1000;
           } else
              break;
         }
       } else
         break;

     } while (1);

   }

   dev_dbg(&drv->usb_dev->dev, "timers stopped");

   return err;
}

void *m1pps_inst_mutexes_create( m1pps_inst_t *inst, uint8_t mutex_cnt, size_t *mutex_sz )
{
   spinlock_t *spinlocks = sys_zalloc( sizeof(spinlock_t)*mutex_cnt );
   if ( spinlocks ) {
     *mutex_sz = sizeof(spinlock_t);
     do {
       mutex_cnt--;
       spin_lock_init( spinlocks + mutex_cnt );
     } while (mutex_cnt);
   }
   return spinlocks;
}

void m1pps_inst_mutexes_free( m1pps_inst_t *inst, void *mutexes )
{
   sys_free( mutexes );
}

void m1pps_inst_mutex_lock( m1pps_inst_t *inst,  void *mutex )
{
   spin_lock( (spinlock_t *)mutex );
}

void m1pps_inst_mutex_unlock( m1pps_inst_t *inst, void *mutex )
{
   spin_unlock( (spinlock_t *)mutex );
}

static void __m1pps_stop( struct m1pps_drv_data *drv )
{
  if (drv) {

    m1pps_sysfs_destroy( &drv->sysfs );
    if (drv->dev) {
      //put_device(drv->dev);
      device_unregister(drv->dev);
    }

    if (drv->timer_thread) {
      dev_dbg(&drv->usb_dev->dev, "stopping timers...");
      kthread_stop(drv->timer_thread);
    }
    dev_dbg(&drv->usb_dev->dev, "close transport...");
    m1pps_trs_close(__trs(&drv->trs_usb));
    dev_dbg(&drv->usb_dev->dev, "destroy transport...");
    m1pps_trs_usb_destroy(&drv->trs_usb);
    dev_dbg(&drv->usb_dev->dev, "destroy instance...");
    m1pps_inst_destroy( &drv->inst );
    kfree(drv);
  }
}

static void m1pps_disconnect(struct usb_interface *interface)
{
 	struct usb_device *usb_dev = interface_to_usbdev(interface);
	struct m1pps_drv_data *drv = dev_get_drvdata(&usb_dev->dev);
/*
*/

  __m1pps_stop(drv);

	dev_info(&usb_dev->dev, "detached from %s driver", m1pps_name);
}

static int m1pps_probe(struct usb_interface *interface, const struct usb_device_id *id)
{
  int err=0;
  struct m1pps_drv_data *drv;
  struct m1pps_trs_usb_open_data trs_data;
  struct m1pps_inst_prm inst_prm;
  struct m1pps_inst_cbs inst_cbs;
  size_t max_pkt_sz=0;

  if (!(drv = kzalloc( sizeof(struct m1pps_drv_data), GFP_KERNEL))) {
     err = -ENOMEM;
     goto error;
  }

	drv->usb_dev = interface_to_usbdev(interface);
	dev_set_drvdata(&drv->usb_dev->dev, drv);

  /* initialize system clock */
  if ((err=m1pps_clk_sys_init( )))
    goto error;

  /* initialize protocol instance */
  inst_prm.role = M1PPS_INST_ROLE_MASTER;
  inst_prm.max_pkt_sz = 0;
  inst_prm.timer_period_us = timer_period_us;
  inst_prm.ctx_cnt = 1;
  /* set needed cb */
  memset(&inst_cbs, 0, sizeof(inst_cbs));
  inst_cbs.mutexes_create = m1pps_inst_mutexes_create;
  inst_cbs.mutexes_free = m1pps_inst_mutexes_free;
  inst_cbs.mutex_lock = m1pps_inst_mutex_lock;
  inst_cbs.mutex_unlock = m1pps_inst_mutex_unlock;

  if ((err=m1pps_inst_init(&drv->inst, &inst_prm, &inst_cbs, m1pps_clk_sys ))) {
    dev_err(&drv->usb_dev->dev, "m1pps_inst_init (%i)", err);
    goto error;
  }

  max_pkt_sz = m1pps_inst_get_max_pkt_sz( &drv->inst );
  m1pps_inst_set_ctx(&drv->inst, 0, drv);

  /* initialize transport */
  if ((err=m1pps_trs_usb_init( &drv->trs_usb, m1pps_clk_sys, max_pkt_sz ))) {
    dev_err(&drv->usb_dev->dev, "m1pps_trs_usb_init (%i)", err);
    goto error;
  }

  m1pps_inst_set_trs(&drv->inst, __trs(&drv->trs_usb));

  drv->dev = device_create(&m1pps_class, 0, MKDEV(0, 0), drv, "m1pps-%i", 0);

  if (IS_ERR(drv->dev)) {
    dev_err(&drv->usb_dev->dev, "device_create (%i)", (int)PTR_ERR(drv->dev));
    drv->dev = 0;
    goto error;
  }

  /* init sysfs entry */
  m1pps_sysfs_init( &drv->sysfs, &drv->inst, drv->dev);


  /* open transport */
  trs_data.ep[RX] = M1PPS_USB_EP_IN;
  trs_data.ep[TX] = M1PPS_USB_EP_OUT;
  trs_data.rx_period_us = RX_PERIOD_US;
  trs_data.usb_dev = drv->usb_dev;

  dev_dbg(&drv->usb_dev->dev, "starting timers");
  /* initialize timer thread for fsm */
  drv->timer_thread = kthread_run(m1pps_timer_thread, drv, "%s timers thread", m1pps_name);

  dev_dbg(&drv->usb_dev->dev, "opening transport");
  if ((err=m1pps_trs_open( __trs(&drv->trs_usb), &trs_data ))) {
    dev_err(&drv->usb_dev->dev, "m1pps_trs_open (%i)", err);
    goto error;
  }

	dev_info(&drv->usb_dev->dev, "attached to %s driver", m1pps_name);

  m1pps_inst_process_msg(&drv->inst, M1PPS_SUBSYS_SYNC, M1PPS_MSG_START, 0, 0);


  return 0;

error:

  __m1pps_stop(drv);

	return err;
}


static struct usb_device_id m1pps_dev_table[] =
{
    { USB_DEVICE(M1PPS_USB_VENDORID, M1PPS_USB_PRODUCTID) },
    { 0 } /* Terminating entry */
};
MODULE_DEVICE_TABLE(usb, m1pps_dev_table);

static struct usb_driver m1pps_driver =
{
	.name = m1pps_name,
	.id_table = m1pps_dev_table,
	.probe = m1pps_probe,
	.disconnect = m1pps_disconnect,
};

static int __init m1pps_init(void)
{
 	int err = 0;

  err = class_register(&m1pps_class);
  if (err < 0) {
    pr_err("%s: Can't register device class (%i)", m1pps_name, err);
    goto error;
  }

  err = class_interface_register(&m1pps_interface);
  if (err) {
    pr_err("%s: Can't register class interface (%i)\n", m1pps_name, err);
    return -EINVAL;
  }
	err = usb_register(&m1pps_driver);
  if (err) {
    pr_err("%s: usb_register failed (%d)", m1pps_name, err);
    goto error;
  }

  return 0;

error:

	return err;
}

static void __exit m1pps_exit(void)
{
   class_unregister(&m1pps_class);
   usb_deregister(&m1pps_driver);
}

module_init(m1pps_init);
module_exit(m1pps_exit);


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexander V. Buev <san@zzz.spb.ru>");
MODULE_DESCRIPTION("Time sync module for usb 1pps device");
